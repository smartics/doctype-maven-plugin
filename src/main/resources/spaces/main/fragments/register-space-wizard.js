// AJS.bind("blueprint.wizard-register.ready", function () {
  Confluence.Blueprint.setWizard('${groupId}.${artifactId}:${package}-space-blueprint-${spaceId}',
    function (wizard) {
      wizard.on("pre-render.spaceBasicDetailsId", Confluence.SpaceBlueprint.CommonWizardBindings.preRender);
      wizard.on("post-render.spaceBasicDetailsId", Confluence.SpaceBlueprint.CommonWizardBindings.postRender);
      wizard.on("submit.spaceBasicDetailsId", Confluence.SpaceBlueprint.CommonWizardBindings.submit);
    });
// });
