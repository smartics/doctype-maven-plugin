require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
	"use strict";

	AJS.bind("blueprint.wizard-register.ready", function () {
		Confluence.Blueprint.setWizard(
			'${project.groupId}.${project.artifactId}:create-doctype-template-${normalizedDoctypeId}',
			PROJECTDOC.standardWizard);
	});
});
