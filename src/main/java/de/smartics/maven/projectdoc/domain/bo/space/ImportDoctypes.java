/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information on doctypes to be imported.
 */
@XmlRootElement(name = "import-doctypes")
@XmlAccessorType(XmlAccessType.FIELD)
public class ImportDoctypes {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  public static final ImportDoctypes EMPTY = new ImportDoctypes();

  // --- members --------------------------------------------------------------

  /**
   * Provides localized information.
   *
   * @since 1.0
   */
  @XmlElement(name = "set")
  private List<RemoteDoctypeSet> doctypeSets;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ImportDoctypes() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public List<RemoteDoctypeSet> getDoctypeSets() {
    if (doctypeSets != null) {
      return doctypeSets;
    }

    return Collections.emptyList();
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "  Set        : " + doctypeSets;
  }
}
