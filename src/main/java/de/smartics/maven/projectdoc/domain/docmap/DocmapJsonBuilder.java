/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.docmap;

import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.settings.Reference;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Creates the map that allows the projectdoc Toolbox to reference documentation
 * for doctypes on the smartics server.
 */
public class DocmapJsonBuilder {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final StringBuilder buffer = new StringBuilder(1024 * 8);

  private final ProjectConfiguration config;

  private final String urlPrefix;

  private final String locale;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DocmapJsonBuilder(final ProjectConfiguration config) {
    this.config = config;
    final Reference ref = config.getReference("addon-documentation");
    this.urlPrefix = ref != null ? ref.getLocator() + '/' : StringUtils.EMPTY;
    buffer.append("  \"____________________________")
        .append(config.getShortId())
        .append("______________________________________\":{}");
    this.locale = config.getLocale();
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public StringBuilder appendStandard(final Doctype doctype) {
    final String doctypeId = doctype.getId();
    final String doctypeName = doctype.isType() ? doctype.getTypeName(locale)
        : doctype.getName(locale);
    appendStandard(doctypeId, doctypeName);

//    if (doctype.hasType()) {
//      final String doctypeTypeId = doctype.getSecondaryId();
//      final String doctypeTypeName = doctype.getTypeName(locale);
//      appendStandard(doctypeTypeId, doctypeTypeName);
//    }

    return buffer;
  }

  private void appendStandard(final String doctypeId,
      final String doctypeName) {
    if (StringUtils.isBlank(doctypeName)) {
      config.info("Cannot append docmap info for doctype '" + doctypeId
          + "' since doctype name is blank.");
      return;
    }
    buffer.append(",\n");
    final String url = urlPrefix + urlEncode(doctypeName);
    final String anchorName = StringUtils.remove(doctypeName, ' ');
    buffer.append("  \"").append(doctypeId)
        .append("\":{\n" + "   \"_default_\":\"").append(url)
        .append("\",\n" + "   \"Description\":\"").append(url).append('#')
        .append(anchorName)
        .append("-Description.1\",\n" + "   \"Notes\":      \"").append(url)
        .append("#DocumentSections-notes-explained\",\n"
            + "   \"References\": \"")
        .append(url)
        .append("#DocumentSections-references-explained\",\n"
            + "   \"Resources\":  \"")
        .append(url)
        .append("#DocumentSections-resources-explained\"\n" + "  }");
  }

  private static String urlEncode(final String input) {
    try {
      return URLEncoder.encode(input, "UTF-8");
    } catch (final UnsupportedEncodingException e) {
      throw new IllegalStateException("UTF-8 not supported.");
    }
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "{\n" + buffer + "\n}\n";
  }
}
