/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.metadata;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.Metadata;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

/**
 * Creates a RDF representation based on a doctype.
 */
public class DoctypeMetadataBuilder {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final String shortId;
  private final File metadataRootFolder;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DoctypeMetadataBuilder(
      final ProjectConfiguration projectConfiguration) {
    this.shortId = projectConfiguration.getShortId();
    this.metadataRootFolder = projectConfiguration.getDoctypeMetadataFolder();
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void writeMetadata(final Doctype doctype) {
    final String doctypeId = doctype.getId();

    writeMetadataXml(doctype);
    writePropertiesFile(doctypeId, doctype.getDoctypeMetadata());
  }

  private void writeMetadataXml(final Doctype doctype) {
    final DoctypeXmlExporter exporter =
        new DoctypeXmlExporter(doctype, shortId, metadataRootFolder);
    exporter.export();
  }

  private void writePropertiesFile(final String fileName,
      final List<Metadata> metadata) {
    final Properties properties = new Properties();
    properties.setProperty("projectdoc.doctype.common.in-context", shortId);
    for (final Metadata data : metadata) {
      properties.setProperty(data.getKey(), data.getValue());
    }

    final File file = new File(metadataRootFolder, fileName + ".properties");
    try (final OutputStream out = FileUtils.openOutputStream(file)) {
      properties.store(out, "projectdoc doctype metadata");
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot write properties doctype metadata file '"
              + file.getAbsolutePath() + "': " + e.getMessage());
    }
  }

  // --- object basics --------------------------------------------------------

}
