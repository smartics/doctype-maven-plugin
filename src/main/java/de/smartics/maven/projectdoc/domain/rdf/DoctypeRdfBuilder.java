/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.rdf;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.TypeReference;
import de.smartics.maven.projectdoc.domain.bo.doctype.TypeReferences;
import de.smartics.maven.projectdoc.io.StringFunction;

import org.apache.commons.io.FileUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Creates a RDF representation based on a doctype.
 */
public class DoctypeRdfBuilder {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String BASE_URI =
      "https://smartics.de/projectdoc/doctype";

  // --- members --------------------------------------------------------------

  private final File rdfRootFolder;

  private final Model addonModel;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DoctypeRdfBuilder(final File rdfRootFolder) {
    this.rdfRootFolder = rdfRootFolder;
    this.addonModel = ModelFactory.createDefaultModel();
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void createRdf(final Doctype doctype) {
    final String id = doctype.getId();
    final Model model = ModelFactory.createDefaultModel();
    final String uri = BASE_URI + '/' + id;
    addResource(doctype, model, uri);
    addResource(doctype, addonModel, uri);

    writeRdfFile(id, model);
  }

  public void writeRdfFile(final String shortId) {
    writeRdfFile("doctypes-addon-" + shortId, addonModel);
  }

  private void writeRdfFile(final String fileName, final Model model) {
    final File file = new File(rdfRootFolder, fileName + ".rdf");
    try (final OutputStream out = FileUtils.openOutputStream(file)) {
      model.write(out);
    } catch (IOException e) {
      throw new SystemException("Cannot write RDF file '"
          + file.getAbsolutePath() + "': " + e.getMessage());
    }
  }

  private void addResource(final Doctype doctype, final Model model,
      final String uri) {
    model.setNsPrefix(doctype.getId(), uri + '#');
    final Resource resource = model.createResource(uri);

    final TypeReferences references = doctype.getTypeReferences();
    for (final TypeReference reference : references) {
      final String predicate = reference.getReferencedByPropertyKey();

      for (final String referencedDoctypeId : StringFunction
          .splitByComma(reference.getForDoctypeId())) {
        final String object = BASE_URI + '/' + referencedDoctypeId;
        final Property rdfProperty =
            model.createProperty(uri + '#' + predicate);
        resource.addProperty(rdfProperty, object);
      }
    }
  }

  // --- object basics --------------------------------------------------------

}
