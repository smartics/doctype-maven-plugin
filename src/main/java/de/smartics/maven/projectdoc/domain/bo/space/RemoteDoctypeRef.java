/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information on a single doctype referenced for import.
 */
@XmlRootElement(name = "doctype-ref")
@XmlAccessorType(XmlAccessType.FIELD)
public class RemoteDoctypeRef {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String DEFAULT_LOCATION = "default";

  public static final String HOME_LOCATION = "home";

  public static final String TYPE_LOCATION = "type";

  // --- members --------------------------------------------------------------

  /**
   * The identifier of the referenced doctype.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String id;

  /**
   * The location for the homepage.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String location;

  /**
   * Marker to indicate whether (<code>true</code>) or not (<code>false</code>)
   * to import a homepage. This allows to promote imported doctypes without
   * adding a homepage to the space.
   *
   * @since 1.0
   */
  @XmlAttribute
  private Boolean homepage;

  /**
   * Marker to indicate whether (<code>true</code>) or not (<code>false</code>)
   * the doctype's blueprint should be promoted with this space. This allows to
   * demote imported doctypes.
   *
   * @since 1.0
   */
  @XmlAttribute
  private Boolean promote;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public RemoteDoctypeRef() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getId() {
    return id;
  }


  public String getLocation() {
    return location;
  }

  public String resolveLocation() {
    final String resolvedLocation;

    if (StringUtils.isBlank(location) || DEFAULT_LOCATION.equals(location)) {
      resolvedLocation =
          StringUtils.isNotBlank(id) && id.endsWith("-type") ? TYPE_LOCATION
              : HOME_LOCATION;
    } else {
      resolvedLocation = location;
    }

    return resolvedLocation;
  }

  public Boolean getHomepage() {
    return homepage == null || homepage;
  }

  public boolean hasHomepage() {
    return homepage == null || homepage.booleanValue();
  }

  public Boolean getPromote() {
    return promote == null || promote;
  }

  public boolean isPromoted() {
    return promote == null || promote.booleanValue();
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "  ID        : " + id;
  }
}
