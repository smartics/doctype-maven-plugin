/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.patch;

import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import org.apache.commons.io.FilenameUtils;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Replaces files in the target folder, which are backuped.
 */
public class FileReplacer {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ProjectConfiguration config;

  private final File targetFolder;

  private final File backupFolder;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public FileReplacer(final ProjectConfiguration config) {
    this.config = config;
    this.targetFolder = config.getWorkingFolder();
    this.backupFolder = config.getBackupFolder();
  }

  // ****************************** Inner Classes *****************************

  private static final class Marker {
    private final String start;
    private final String end;
    private final String replacementComment;

    private Marker(final String start, final String end,
        final String replacementComment) {
      this.start = start;
      this.end = end;
      this.replacementComment = replacementComment;
    }
  }

  private final class Traverser {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final File root;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private Traverser(final File root) {
      this.root = root;
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    private void start() throws IOException {
      traverse(root);
    }

    private void traverse(final File root) throws IOException {
      final File[] files = root.listFiles();
      if (files != null) {
        for (final File child : files) {
          if (child.canRead()) {
            if (child.isFile()) {
              handleFile(child);
            } else if (child.isDirectory()) {
              final boolean isFragmentsFolder = handleFragmentsFolder(child);
              if (!isFragmentsFolder) {
                traverse(child);
              }
            }
          }
        }
      }
    }

    private boolean handleFragmentsFolder(final File folder)
        throws IOException, IllegalArgumentException {
      final String path = calcPath(folder);

      final File targetFile = new File(targetFolder, path);
      if (targetFile.isFile() && targetFile.canWrite()) {
        final File[] fragmentFiles = folder.listFiles();
        if (fragmentFiles != null && fragmentFiles.length > 0) {
          final File backupFile = new File(backupFolder, path);
          if (!backupFile.exists()) {
            FileUtils.copyFile(targetFile, backupFile);
          }

          String targetContent = FileUtils.fileRead(targetFile, "UTF-8");

          for (final File fragmentFile : fragmentFiles) {
            final String originalFragmentContent =
                FileUtils.fileRead(fragmentFile, "UTF-8");

            final PlaceholderReplacer replacer = config.getReplacer();
            final String fragmentContent =
                replacer.replace(originalFragmentContent);

            final String fileName = fragmentFile.getName();
            final String id = FilenameUtils.getBaseName(fileName);
            final String extension = FilenameUtils.getExtension(fileName);
            final Marker marker = createStartEndMarker(id, extension);
            final Marker simpleMarker = createStartOnlyMarker(id, extension);
            targetContent = replace(targetFile, fragmentFile, targetContent,
                marker, simpleMarker, fragmentContent);
          }

          FileUtils.fileWrite(targetFile.getAbsolutePath(), "UTF-8",
              targetContent);

          return true;
        }
      }

      return false;
    }

    private String replace(final File file, final File fragmentFile,
        final String targetContent, final Marker marker,
        final Marker simpleMarker, final String fragmentContent) {
      int startIndex = targetContent.indexOf(marker.start, 0);
      if (startIndex != -1) {
        startIndex += marker.start.length();
        final int endIndex = targetContent.indexOf(marker.end, startIndex);
        if (endIndex != -1) {
          final String replaced = targetContent.substring(0, startIndex) + '\n'
              + marker.replacementComment + '\n' + fragmentContent + '\n'
              + targetContent.substring(endIndex);
          return replaced;
        } else {
          config.info("Cannot find end marker '" + marker.end + "' in file '"
              + file.getAbsolutePath()
              + "' -  therefore skipping fragment file '" + fragmentFile
              + "'.");
        }
      } else {
        if (simpleMarker != null) {
          int currentStart = 0;
          String replaced = targetContent;
          do {
            int simpleMarkerStartIndex =
                replaced.indexOf(simpleMarker.start, currentStart);
            if (simpleMarkerStartIndex != -1) {
              simpleMarkerStartIndex += simpleMarker.start.length();
              replaced = replaced.substring(0, simpleMarkerStartIndex) + '\n'
                  + fragmentContent + '\n'
                  + replaced.substring(simpleMarkerStartIndex);
              currentStart =
                  simpleMarkerStartIndex + fragmentContent.length() + 2;
            } else {
              if (currentStart == 0) {
                config.info("Cannot find start marker '" + marker.start
                    + "' nor start marker '" + simpleMarker.start
                    + "' in file '" + file.getAbsolutePath()
                    + "' -  therefore skipping fragment file '" + fragmentFile
                    + "'.");
              }
              currentStart = -1;
            }
          } while (currentStart != -1);
          return replaced;
        } else {
          config.info("Cannot find start marker '" + marker.start
              + "' in file '" + file.getAbsolutePath()
              + "' -  therefore skipping fragment file '" + fragmentFile
              + "'.");
        }
      }
      return targetContent;
    }

    private Marker createStartEndMarker(final String id, final String extension)
        throws IllegalArgumentException {
      if ("xml".equals(extension)) {
        return new Marker("<!-- insert-mark " + id + "# -->",
            "<!-- /insert-mark " + id + "# -->", "<!-- PATCHED! -->");
      } else if ("js".equals(extension)) {
        return new Marker("/* insert-mark " + id + "# */",
            "/* /insert-mark " + id + "# */", "/* PATCHED! */");
      }

      throw new IllegalArgumentException(
          "Cannot replace marker in files with extension '" + extension + "'.");
    }

    private Marker createStartOnlyMarker(final String id,
        final String extension) throws IllegalArgumentException {
      if ("xml".equals(extension)) {
        return new Marker("<!-- === projectdoc INSERT " + id + " === -->", null,
            "<!-- PATCHED! -->");
      } else if ("js".equals(extension)) {
        return new Marker("/* === projectdoc INSERT " + id + " === */", null,
            "/* PATCHED! */");
      }

      throw new IllegalArgumentException(
          "Cannot replace marker in files with extension '" + extension + "'.");
    }

    private void handleFile(final File child) throws IOException {
      final String path = calcPath(child);

      final File targetFile = new File(targetFolder, path);
      if (targetFile.exists()) {
        final File backupFile = new File(backupFolder, path);
        if (!backupFile.exists()) {
          FileUtils.copyFile(targetFile, backupFile);
        }
      }

      FileUtils.copyFile(child, targetFile);
    }

    private String calcPath(final File file) {
      final String rootPath = root.getAbsolutePath();
      final String filePath = file.getAbsolutePath();
      final String path = filePath.substring(rootPath.length());
      return path;
    }

    // --- object basics ------------------------------------------------------

  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void execute(final File root) throws IOException {
    if (root.canRead()) {
      new Traverser(root).start();
    }
  }

  // --- object basics --------------------------------------------------------

}
