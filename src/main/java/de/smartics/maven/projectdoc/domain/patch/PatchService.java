/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.patch;

import de.smartics.maven.projectdoc.domain.ConfigurationException;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import java.io.IOException;

/**
 * Patches generated files. Useful if model or generation process does not
 * support a use case.
 */
public class PatchService {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ProjectConfiguration config;

  private final Patcher patcher;

  private final FileReplacer replacer;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public PatchService(final AddonRepository repository) {
    this.config = repository.getConfig();
    this.patcher = new Patcher(repository);
    this.replacer = new FileReplacer(config);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void patch() {
    patcher.properties();

    try {
      replacer.execute(config.getPatchFilesFolder());
    } catch (final IOException e) {
      throw new ConfigurationException(
          "Cannot replace generated files with patch files: " + e.getMessage(),
          e);
    }
  }

  // --- object basics --------------------------------------------------------

}
