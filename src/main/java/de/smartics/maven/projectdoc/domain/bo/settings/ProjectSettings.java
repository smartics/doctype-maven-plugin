/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.settings;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Defaults for creating projects.
 */
@XmlRootElement(name = "project-settings")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProjectSettings {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The artifact coordinates within the project settings providing defaults.
   *
   * @since 1.0
   */
  private Coordinates coordinates;

  /**
   * The project directory layout within the project settings providing
   * defaults.
   *
   * @since 1.0
   */
  private Layout layout;

  /**
   * The default keys used in the project files.
   *
   * @since 1.0
   */
  private Keys keys;

  /**
   * The list of references to remote resources relevant to the project.
   *
   * @since 1.0
   */
  @XmlElementWrapper(name = "references")
  @XmlElement(name = "reference")
  private List<Reference> references;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the artifact coordinates within the project settings providing
   * defaults.
   *
   * @return the artifact coordinates within the project settings providing
   *         defaults.
   */
  public Coordinates getCoordinates() {
    if (coordinates != null) {
      return coordinates;
    }

    return new Coordinates();
  }

  /**
   * Returns the project directory layout within the project settings providing
   * defaults.
   *
   * @return the project directory layout within the project settings providing
   *         defaults.
   */
  public Layout getLayout() {
    if (layout != null) {
      return layout;
    }

    return new Layout();
  }

  /**
   * Returns the default keys used in the project files.
   *
   * @return the default keys used in the project files.
   */
  public Keys getKeys() {
    if (keys != null) {
      return keys;
    }

    return new Keys();
  }

  /**
   * Returns the list of references to remote resources relevant to the project.
   *
   * @return the list of references to remote resources relevant to the project.
   */
  public List<Reference> getReferences() {
    if (references != null) {
      return references;
    }

    return Collections.emptyList();
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return coordinates + "\n" + layout + "\n" + keys;
  }
}
