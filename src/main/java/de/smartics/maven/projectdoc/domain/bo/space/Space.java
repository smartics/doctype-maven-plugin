/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import de.smartics.maven.projectdoc.domain.bo.tool.ToolRef;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information on a single projectdoc space based on information from an XML
 * file.
 */
@XmlRootElement(name = "space")
@XmlAccessorType(XmlAccessType.FIELD)
public class Space {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The identifier of the space.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String id;

  /**
   * The category of the space.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String category;

  /**
   * Provides localized information.
   *
   * @since 1.0
   */
  @XmlElementWrapper(name = "resource-bundle")
  @XmlElement(name = "l10n")
  private List<L10n> resourceBundle;

  @XmlElement(name = "doctype-bundle")
  private DoctypeBundle doctypes;

  /**
   * The list of properties placed on the homepage.
   *
   * @since 1.0
   */
  @XmlElementWrapper(name = "properties")
  @XmlElement(name = "property")
  private List<SpaceProperty> properties;

  /**
   * The list of tools to add to the space.
   *
   * @since 1.0
   */
  @XmlElementWrapper(name = "tools")
  @XmlElement(name = "tool")
  private List<ToolRef> tools;

  @XmlElement(name = "import-doctypes")
  private ImportDoctypes importDoctypes;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public Space() {}

  // ****************************** Inner Classes *****************************

  public static final class Bundle {
    private final L10n l10n;

    private Bundle(final L10n l10n) {
      this.l10n = l10n;
    }

    public String getLocale() {
      return l10n.getLocale();
    }

    public String getName() {
      return l10n.getName();
    }

    public String getDescription() {
      return l10n.getDescription();
    }

    public String getAbout() {
      return l10n.getAbout();
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getId() {
    return id;
  }

  public String getCategory() {
    return StringUtils.isNotBlank(category) ? category : id;
  }

  public List<Bundle> getBundles() {
    if (resourceBundle != null) {
      final List<Bundle> bundles = new ArrayList<Bundle>(resourceBundle.size());

      for (final L10n l10n : resourceBundle) {
        final Bundle bundle = new Bundle(l10n);
        bundles.add(bundle);
      }

      return bundles;
    }

    return Collections.emptyList();
  }

  public boolean isIncluded(final String id) {
    if (doctypes != null) {
      return doctypes.isIncluded(id);
    }

    return true;
  }

  public List<SpaceProperty> getProperties() {
    if (properties != null) {
      return properties;
    }

    return Collections.emptyList();
  }

  public List<ToolRef> getTools() {
    if (tools != null) {
      return tools;
    }

    return Collections.emptyList();
  }

  public ImportDoctypes getImportDoctypes() {
    return importDoctypes != null ? importDoctypes : ImportDoctypes.EMPTY;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "  ID        : " + id;
  }
}
