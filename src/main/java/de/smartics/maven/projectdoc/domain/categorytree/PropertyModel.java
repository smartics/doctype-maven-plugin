/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.categorytree;

import de.smartics.maven.projectdoc.domain.categorytree.PropertyLocator.Position;
import de.smartics.maven.projectdoc.domain.categorytree.PropertyLocator.PropertyRef;

import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Provides a model of a projectdoc document property to be transfered to a
 * representation.
 *
 * @since 1.8
 */
@XmlRootElement(name = "property")
@XmlAccessorType(XmlAccessType.FIELD)
public class PropertyModel {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The name of the property.
   */
  @XmlElement
  private String name;

  /**
   * The string representation of the property value.
   */
  @XmlElement
  private String value;

  /**
   * The string representation of the controls value.
   */
  @XmlElement
  private String controls;

  /**
   * The position to apply to the property.
   */
  @XmlElement
  private String position;

  /**
   * The property to which the position applies.
   */
  @XmlElement(name = "ref")
  private String ref;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * JAXB constructor.
   */
  public PropertyModel() {}

  /**
   * In-document property model.
   */
  public PropertyModel(final String propertyName, final String propertyValue,
      final String controls) {
    this(propertyName, propertyValue, controls, null);
  }

  /**
   * In-document property model.
   */
  public PropertyModel(final String propertyName, final String propertyValue,
      final String controls, final Position position) {
    this.name = propertyName;
    this.value = propertyValue;
    this.controls = controls;
    this.position = position != null ? position.toString() : null;
  }

  /**
   * Plain value constructor.
   */
  public PropertyModel(final String propertyName, final String propertyValue,
      final String controls, final String position,
      final String referenceProperty) {
    this.name = propertyName;
    this.value = propertyValue;
    this.controls = controls;
    this.position = position;
    this.ref = referenceProperty;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getName() {
    return name;
  }

  public String getValue() {
    return value;
  }

  public String getControls() {
    return controls;
  }

  public String getPositionString() {
    return position;
  }

  public Position getPosition() throws IllegalArgumentException {
    return Position.toPosition(position);
  }

  // --- business -------------------------------------------------------------

  public PropertyLocator getLocator() {
    return getLocator(ref);
  }

  public PropertyLocator getLocator(final String defaultReferenceProperty) {
    final Position position = getPosition();
    final PropertyRef propertyRef = new PropertyRef(
        StringUtils.isBlank(ref) ? defaultReferenceProperty : ref);
    final PropertyLocator locator = new PropertyLocator(position, propertyRef);
    return locator;
  }

  // --- object basics --------------------------------------------------------

  public String toString() {
    return name + '=' + value
        + (StringUtils.isNotBlank(controls) ? " (" + controls + ")" : "") + ": "
        + (StringUtils.isNotBlank(position) ? position : "")
        + (StringUtils.isNotBlank(ref) ? "/" + ref : "");
  }
}
