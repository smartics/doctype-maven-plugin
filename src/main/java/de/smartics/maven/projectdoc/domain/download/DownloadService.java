/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.download;

import de.smartics.maven.projectdoc.domain.ConfigurationException;
import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.addon.ImportDoctypes;
import de.smartics.maven.projectdoc.domain.bo.addon.RemoteDoctypeRef;
import de.smartics.maven.projectdoc.domain.bo.addon.RemoteDoctypeSet;
import de.smartics.maven.projectdoc.domain.exporter.DoctypeExportService;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 *
 */
public class DownloadService {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final AddonRepository repository;

  private final ArtifactRepositoryService artifactRepositoryService;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DownloadService(final AddonRepository repository) {
    this.repository = repository;
    this.artifactRepositoryService =
        new ArtifactRepositoryService(repository.getConfig().getMavenSession());
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void downloadHomepageXmlFiles() throws SystemException {
    try {
      final ImportDoctypes importDoctypes =
          repository.getAddon().getImportDoctypes();
      final List<RemoteDoctypeSet> doctypeSets =
          importDoctypes.getDoctypeSets();

      final String pathTemplate = repository.getConfig().getPathTemplate();
      for (final RemoteDoctypeSet doctypeSet : doctypeSets) {
        final String groupId = doctypeSet.getGroupId();
        final String artifactId = doctypeSet.getArtifactId();
        final String version = calcVersion(groupId, artifactId);

        if (StringUtils.isBlank(version)) {
          final String message = "Cannot find resources for '" + groupId + ':'
              + artifactId
              + "'. Check if this artifact should be registed as a plugin"
              + " artifact in the plugin's configuration section of your POM!"
              + " Otherwise remove the import for doctypes from this artifact!";
          throw new ConfigurationException(message);
        }

        final ArchiveExtractor extractor = artifactRepositoryService
            .resolveFrom(groupId, artifactId, version, "jar", null);
        final HomepageAccessorHelper helper =
            new HomepageAccessorHelper(extractor, pathTemplate);

        final String addonId = doctypeSet.getShortId();
        for (final RemoteDoctypeRef doctypeRef : doctypeSet.getDoctypeRefs()) {
          if (!doctypeRef.hasHomepage()) {
            continue;
          }
          final String doctypeId = doctypeRef.getId();
          final String homepageXml = helper.load(addonId, doctypeId);

          final DoctypeExportService exportService =
              new DoctypeExportService(repository);
          exportService.writeToHome(doctypeId, homepageXml);
        }
      }
    } catch (final Exception e) {
      throw new SystemException("Cannot download remote homepages.", e);
    }
  }

  private String calcVersion(final String groupId, final String artifactId) {
    final String version =
        repository.getConfig().getPluginArtifactVersion(groupId, artifactId);
    return version;
  }

  // --- object basics --------------------------------------------------------

}
