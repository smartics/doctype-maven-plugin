/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Renders a Confluence placeholder element.
 */
@XmlRootElement(name = "placeholder")
@XmlAccessorType(XmlAccessType.FIELD)
public class Placeholder {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The key for the placeholder text.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String key;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public Placeholder() {}

  public Placeholder(final String key) {
    this.key = key;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public String toI18n() {
    final StringBuilder buffer = new StringBuilder(64);
    buffer.append("<at:i18n at:key=\"").append(key).append("\"/>");
    return buffer.toString();
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    final StringBuilder buffer = new StringBuilder(512);
    buffer.append("<ac:placeholder>").append(toI18n())
        .append("</ac:placeholder>");
    return buffer.toString();
  }
}
