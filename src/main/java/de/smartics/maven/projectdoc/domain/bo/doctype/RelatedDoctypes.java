/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information on related doctypes to a doctype.
 */
@XmlRootElement(name = "related-doctypes")
@XmlAccessorType(XmlAccessType.FIELD)
public class RelatedDoctypes implements Iterable<DoctypeRef> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @XmlAttribute(name = "type-ref-property")
  private String typeRefProperty;

  @XmlElement(name = "doctype-ref")
  private List<DoctypeRef> doctypeRefs;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getTypeRefPropertyKey() {
    return typeRefProperty;
  }

  public List<DoctypeRef> getDoctypeRefs() {
    if (doctypeRefs != null) {
      return doctypeRefs;
    }

    return Collections.emptyList();
  }

  @Override
  public Iterator<DoctypeRef> iterator() {
    if (doctypeRefs != null) {
      return doctypeRefs.iterator();
    }

    return Collections.<DoctypeRef> emptyList().iterator();
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
