/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.project.ExportLocation;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import de.smartics.maven.projectdoc.io.Resource;

import java.io.File;

public abstract class ExportLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  protected static final String PACKAGE = "${packagePath}";

  protected static final String SOURCES = "src/main/java/" + PACKAGE;

  protected static final String RESOURCES_ROOT = "src/main/resources";

  protected static final String RESOURCES = RESOURCES_ROOT + '/' + PACKAGE;

  protected static final String CONTENT = RESOURCES + "/content";

  // --- members --------------------------------------------------------------

  protected final File projectBaseFolder;

  protected final String shortId;

  protected final String packagePath;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  protected ExportLocations(final ProjectConfiguration projectConfig) {
    this.projectBaseFolder = projectConfig.getWorkingFolder();
    this.shortId = projectConfig.getShortId();
    this.packagePath = projectConfig.getPackagePath();
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  protected Resource getResource(final ExportLocation location) {
    return getResource(location, null);
  }

  public Resource getResource(final ExportLocation location,
      final String locale) {
    return getResource(projectBaseFolder, location, locale);
  }

  protected Resource getResource(final File baseFolder,
      final ExportLocation location, final String locale) {
    final String localeString = (locale != null ? "_" + locale : "");
    final String path = location.toString(createPathArgs(localeString));
    final File file = new File(baseFolder, path);
    return new Resource(file, path);
  }

  protected abstract String[] createPathArgs(String locale);

  public static File getContentFolder(final File baseFolder,
      final String packagePath) {
    final String path = CONTENT.replace("${packagePath}", packagePath);
    return new File(baseFolder, path);
  }

  // --- object basics --------------------------------------------------------

}
