/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.addon.Addon;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.TypeReference;
import de.smartics.maven.projectdoc.domain.bo.doctype.TypeReferences;
import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.bo.tool.Tool;
import de.smartics.maven.projectdoc.domain.exporter.ExportLocations;

import org.apache.commons.lang3.ObjectUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.CheckForNull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Provides static information about the add-on and its environment.
 */
public class AddonRepository {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ProjectConfiguration config;

  private final Addon addon;

  private final Map<String, Space> spaces = new HashMap<String, Space>();

  private final Map<String, Tool> tools = new HashMap<String, Tool>();

  /**
   * Mapping a doctype ID to the doctype.
   */
  private final Map<String, Doctype> doctypes =
      new TreeMap<String, Doctype>(new Comparator<String>() {
        @Override
        public int compare(final String o1, final String o2) {
          return -ObjectUtils.compare(o1, o2);
        }
      });

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public AddonRepository(final ProjectConfiguration config, final Addon addon) {
    this.config = config;
    this.addon = addon;
  }

  // ****************************** Inner Classes *****************************

  private static final class Accessor {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final JAXBContext xmlContext;

    private final Unmarshaller unmarshaller;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private Accessor() throws JAXBException {
      this.xmlContext = JAXBContext.newInstance(Addon.class);
      this.unmarshaller = xmlContext.createUnmarshaller();
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    private Addon unmarshal(final File file) throws JAXBException {
      final Addon addon = (Addon) unmarshaller.unmarshal(file);
      return addon;
    }

    // --- object basics ------------------------------------------------------
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- create ---------------------------------------------------------------

  public static AddonRepository create(final ProjectConfiguration config) {
    try {
      final Accessor accessor = new Accessor();
      final File file = new File(config.getModelsFolder(), "add-on.xml");
      final Addon addon =
          file.canRead() ? accessor.unmarshal(file) : new Addon();
      final AddonRepository repository = new AddonRepository(config, addon);
      return repository;
    } catch (final JAXBException e) {
      throw new SystemException(
          "Cannot read add-on descriptor: " + e.getMessage(), e);
    }
  }

  // --- get&set --------------------------------------------------------------

  public ProjectConfiguration getConfig() {
    return config;
  }

  public Addon getAddon() {
    return addon;
  }

  // --- business -------------------------------------------------------------

  public String createCategoryId(final String id, final String spaceId) {
    final String packageId = config.getPackage();
    final String shortId = config.getShortId();
    return packageId + ".content." + spaceId + '.' + id + ".section";
  }

  public boolean hasDoctypeModels() {
    final File folder = config.getDoctypesFolder();
    final String[] folderList = folder.list();
    return folder.exists() && folderList != null && folderList.length > 0;
  }

  public void addSpace(final Space space) {
    spaces.put(space.getId(), space);
  }

  public void addTool(final Tool tool) {
    tools.put(tool.getId(), tool);
  }

  @CheckForNull
  public Tool getTool(final String toolId) {
    return tools.get(toolId);
  }

  @CheckForNull
  public Space getSpace(final String spaceId) {
    return spaces.get(spaceId);
  }

  public Collection<Space> getSpaces() {
    return spaces.values();
  }

  public void addDoctype(final Doctype doctype) {
    doctypes.put(doctype.getId(), doctype);
  }

  public Doctype getDoctype(final String doctypeId) {
    return doctypes.get(doctypeId);
  }

  public Collection<Doctype> getDoctypes() {
    return doctypes.values();
  }

  public Collection<Doctype> getDoctypeWithType(final String id) {
    final List<Doctype> result = new ArrayList<Doctype>();
    for (final Doctype doctype : getDoctypes()) {
      if (id.equals(doctype.getSecondaryId())) {
        result.add(doctype);
      }
    }
    return result;
  }

  public boolean hasGeneratedTemplates() {
    final File contentFolder = getContentFolder();
    final File folder = new File(contentFolder, "doctypes");
    final String[] folderList = folder.list();
    return folder.exists() && folderList != null && folderList.length > 0;
  }

  public File getContentFolder() {
    final File contentFolder = ExportLocations
        .getContentFolder(config.getWorkingFolder(), config.getPackagePath());
    return contentFolder;
  }

  public File getTargetToolsFolder() {
    final File targetToolsFolder = new File(getContentFolder(), "tools");
    if (!targetToolsFolder.exists()) {
      targetToolsFolder.mkdirs();
    }
    return targetToolsFolder;
  }

  public List<String> getSupportedLocales() {
    return addon.getSupportedLocales();
  }

  public TypeReferences getTypeInfoFor(final Doctype doctype) {
    final String doctypeId = doctype.getId();
    final String anyProperty =
        doctype.getRelatedDoctypes().getTypeRefPropertyKey();
    final TypeReferences typeReferences = new TypeReferences(anyProperty);
    for (final Doctype current : doctypes.values()) {
      final TypeReferences references = current.getTypeReferences();
      for (final TypeReference reference : references) {
        if (doctypeId.equals(reference.getForDoctypeId())) {
          typeReferences.add(reference);
        }
      }
    }

    return typeReferences;
  }

  // --- object basics --------------------------------------------------------

}
