/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import de.smartics.maven.projectdoc.doc.WizardFormPlaceholderDefaultValue;

import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A wizard's field.
 */
@XmlRootElement(name = "field")
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressFBWarnings("UWF_UNWRITTEN_FIELD")
public class WizardField {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  @WizardFormPlaceholderDefaultValue(name = "height")
  private static final Integer DEFAULT_TEXTAREA_HEIGHT = 6;

  @WizardFormPlaceholderDefaultValue(name = "template")
  private static final String DEFAULT_TEMPLATE = "standard";

  // --- members --------------------------------------------------------------

  /**
   * The name of the field.
   */
  @XmlAttribute
  private String name;

  /**
   * The entire field pre-configured as XML.
   */
  private String xml;

  /**
   * The value to the key to fetch the label from the resource bundle.
   */
  @XmlAttribute
  private String key;

  /**
   * The value to the template the field is based on.
   */
  @XmlAttribute(required = true)
  private String template;

  /**
   * Optional attribute to specify the height of a field. Not every field
   * supports a height.
   */
  @XmlAttribute
  private Integer height;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getName() {
    return name;
  }

  public String getKey() {
    return key;
  }

  public Integer getHeight() {
    if (height == null) {
      return DEFAULT_TEXTAREA_HEIGHT;
    }
    return height;
  }

  public String getTemplate() {
    if (StringUtils.isBlank(template)) {
      return DEFAULT_TEMPLATE;
    }
    return template;
  }

  public String getXml() {
    return xml;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
