/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.categorytree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Provides a model of a list of projectdoc document documents.
 *
 * @since 1.8
 */
@XmlRootElement(name = "document")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentsModel implements Iterable<DocumentModel> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The list of documents to create.
   */
  @XmlElement(name = "documents")
  private final List<DocumentModel> documents = new ArrayList<>();

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * JAXB constructor.
   */
  public DocumentsModel() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void addDocument(final DocumentModel model) {
    documents.add(model);
  }

  @Nullable
  public DocumentModel getDocument(final String name, final String nameName) {
    for (final DocumentModel model : documents) {
      if (name.equals(model.getPropertyAsString(nameName))) {
        return model;
      }
      final DocumentModel child = getDocument(name, model, nameName);
      if (child != null) {
        return child;
      }
    }
    return null;
  }

  @Nullable
  private DocumentModel getDocument(final String name, final DocumentModel root,
      final String nameName) {
    for (final DocumentModel child : root) {
      if (name.equals(child.getPropertyAsString(nameName))) {
        return child;
      }
      final DocumentModel model = getDocument(name, child, nameName);
      if (model != null) {
        return model;
      }

    }

    return null;
  }

  @Override
  public Iterator<DocumentModel> iterator() {
    if (documents != null) {
      return documents.iterator();
    }
    return Collections.emptyIterator();
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    final StringBuilder buffer = new StringBuilder(8192);
    for (final DocumentModel document : documents) {
      buffer.append("\n===\n").append(document);
    }
    return buffer.toString();
  }
}
