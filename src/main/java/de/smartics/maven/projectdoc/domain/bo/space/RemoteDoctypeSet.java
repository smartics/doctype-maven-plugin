/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information on imported doctypes from a set.
 */
@XmlRootElement(name = "set")
@XmlAccessorType(XmlAccessType.FIELD)
public class RemoteDoctypeSet {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The identifier of the set is the blueprint ID prefix (excluding the name of
   * the doctype).
   *
   * @since 1.0
   */
  @XmlAttribute
  private String id;

  /**
   * Provides localized information.
   *
   * @since 1.0
   */
  @XmlElement(name = "doctype-ref")
  private List<RemoteDoctypeRef> doctypeRefs;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public RemoteDoctypeSet() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getId() {
    return id;
  }

  public String getGroupId() {
    if (StringUtils.isNotBlank(id)) {
      final int componentIndex = id.indexOf(':');
      if (componentIndex > 0) {
        final String addonId = id.substring(0, componentIndex);
        final int artifactIndex = addonId.lastIndexOf('.');
        if (artifactIndex > 0) {
          return addonId.substring(0, artifactIndex);
        }
      }
    }

    return null;
  }

  public String getArtifactId() {
    if (StringUtils.isNotBlank(id)) {
      final int componentIndex = id.indexOf(':');
      if (componentIndex > 0) {
        final String addonId = id.substring(0, componentIndex);
        final int artifactIndex = addonId.lastIndexOf('.');
        if (artifactIndex > 0) {
          return addonId.substring(artifactIndex + 1);
        }
      }
    }

    return null;
  }

  public List<RemoteDoctypeRef> getDoctypeRefs() {
    if (doctypeRefs != null) {
      return doctypeRefs;
    }

    return Collections.emptyList();
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "  ID        : " + id + " refs=" + doctypeRefs;
  }
}
