/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * A list of doctypes included in or excluded from this space.
 */
@XmlRootElement(name = "doctype-bundle")
@XmlAccessorType(XmlAccessType.FIELD)
public class DoctypeBundle {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String INCLUDE = "include";

  // --- members --------------------------------------------------------------

  /**
   * Either 'exclude' or 'include' to exclude or include all of the enclosed
   * doctypes.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String directive;

  @XmlElement(name = "doctype-ref")
  private List<DoctypeRefPattern> doctypes;

  @XmlTransient
  private Boolean doInclude;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------


  public boolean isIncluded(final String id) {
    if (doInclude == null) {
      doInclude = INCLUDE.equals(directive);
    }

    if (doInclude) {
      for (final DoctypeRefPattern doctype : doctypes) {
        if (doctype.matches(id)) {
          return true;
        }
      }

      return false;
    } else {
      for (final DoctypeRefPattern doctype : doctypes) {
        if (doctype.matches(id)) {
          return false;
        }
      }

      return true;
    }
  }


  // --- object basics --------------------------------------------------------

}
