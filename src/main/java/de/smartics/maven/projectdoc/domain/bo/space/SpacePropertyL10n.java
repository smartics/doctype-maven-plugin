/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import de.smartics.maven.projectdoc.io.StringFunction;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Provides a localized text resource.
 */
@XmlRootElement(name = "l10n")
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressFBWarnings("UWF_UNWRITTEN_FIELD")
public class SpacePropertyL10n {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The locale of the text resource. If this value is <code>null</code> it is
   * the default representation.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String locale;

  /**
   * The name resource.
   *
   * @since 1.0
   */
  private String name;

  /**
   * The description resource.
   *
   * @since 1.0
   */
  private String description;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the locale of the text resource. If this value is <code>null</code>
   * it is the default representation.
   *
   * @return the locale of the text resource.
   */
  public String getLocale() {
    return locale;
  }

  /**
   * Returns the name resource.
   *
   * @return the name resource.
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the description resource.
   *
   * @return the description resource.
   */
  public String getDescription() {
    return normalize(description);
  }

  // --- business -------------------------------------------------------------

  private static String normalize(final String input) {
    if (input == null) {
      return null;
    }
    return StringFunction.strip(input); // StringUtils.replace(, "\\n", "\\");
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this,
        ToStringStyle.SHORT_PREFIX_STYLE);
  }
}
