/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.addon;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Provides a localized text resource.
 */
@XmlRootElement(name = "l10n")
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressFBWarnings("UWF_UNWRITTEN_FIELD")
public class L10n {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The locale of the text resource. If this value is <code>null</code> it is
   * the default representation.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String locale;

  /**
   * The name resource.
   *
   * @since 1.0
   */
  private Term name;

  /**
   * The description to the resource.
   *
   * @since 1.8
   */
  @XmlElement
  private String description;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the locale of the text resource. If this value is <code>null</code>
   * it is the default representation.
   *
   * @return the locale of the text resource.
   */
  public String getLocale() {
    return locale;
  }

  /**
   * Returns the name resource.
   *
   * @return the name resource.
   */
  public String getName() {
    if (name != null) {
      return name.getSingular();
    }
    return null;
  }

  /**
   * Returns the name resource in its plural form.
   *
   * @return the name resource in its plural form.
   */
  public String getPlural() {
    if (name != null) {
      final String plural = name.getPlural();
      if (StringUtils.isNotBlank(plural)) {
        return plural;
      }
      return getName() + 's';
    }
    return null;
  }

  /**
   * Returns the description to the resource.
   *
   * @return the description to the resource.
   */
  public String getDescription() {
    return description;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this,
        ToStringStyle.SHORT_PREFIX_STYLE);
  }
}
