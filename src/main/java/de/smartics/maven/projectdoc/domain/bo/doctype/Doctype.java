/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import java.util.List;


/**
 * Information on a single projectdoc doctype.
 */
public interface Doctype {

  String getId();

  String getCategory();

  String getName(String locale);

  String getNamePlural(String locale);

  String getSecondaryId();

  String getTypeName(String locale);

  String getTypeNamePlural(String locale);

  String getBaseTemplateName();

  boolean hasHomepage();

  String getContextProvider(String packagePrefix);

  String getCreateResult();

  String getProvideType();

  boolean hasType();

  boolean isType();

  boolean isStandardType();

  DoctypeResourceBundle getResourceBundle();

  List<Property> getProperties();

  List<Section> getSections();

  Section getSection(String string);

  void setResourceBundle(DoctypeResourceBundle resourceBundle);

  void setBaseTemplateName(String baseTemplateName);

  RelatedDoctypes getRelatedDoctypes();

  void setRelatedDoctypes(RelatedDoctypes relatedDoctypes);

  void setSecondaryId(String secondaryId);

  boolean hasSection(String key);

  Wizard getWizard();

  TypeReferences getTypeReferences();

  List<Metadata> getDoctypeMetadata();
}
