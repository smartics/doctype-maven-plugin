/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.download;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.annotation.CheckForNull;

/**
 * Wraps an archive file.
 */
class ArchiveExtractor {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final JarFile archiveFile;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   *
   * @throws IOException if the archive file cannot be opened as a JAR.
   */
  ArchiveExtractor(final File archiveFile) throws IOException {
    this.archiveFile = new JarFile(archiveFile);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * Opens a stream to the given resource.
   *
   * @param name the name of the resource within the archive.
   * @return an input stream to the resource, <code>null</code> if the name is
   *         not found in the JAR.
   * @throws IOException on any problem opening the stream.
   */
  @CheckForNull
  public InputStream open(final String name) throws IOException {
    final JarEntry entry = archiveFile.getJarEntry(name);
    if (entry != null) {
      final InputStream input = archiveFile.getInputStream(entry);
      return input;
    }

    return null;
  }

  // --- object basics --------------------------------------------------------

}
