/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * A macro parameter.
 */
@XmlRootElement(name = "param")
@XmlAccessorType(XmlAccessType.FIELD)
public class MacroParam {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The name of the parameter.
   */
  @XmlAttribute
  private String name;

  /**
   * The key to the l10n resources.
   */
  @XmlAttribute
  private String key;

  /**
   * The value to the parameter.
   */
  @XmlValue
  private String value;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getName() {
    return name;
  }

  public String getKey() {
    return key;
  }

  public String getValue() {
    if (StringUtils.isBlank(value)) {
      if (StringUtils.isNotBlank(key)) {
        return "<at:i18n at:key=\"" + key + "\"/>";
      }
    }
    return value;
  }

  // --- business -------------------------------------------------------------

  public void filter(final PlaceholderReplacer replacer) {
    if (StringUtils.isNotBlank(value)) {
      value = replacer.replace(value);
    }
  }

  // --- object basics --------------------------------------------------------

}
