/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Contains information on constructing doctype wizards.
 *
 * @since 1.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Wizard {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String STANDARD_TEMPLATE_ID = "standard";

  private static final String STANDARD_CODE_ID = "standard";

  public static final Wizard DEFAULT =
      new Wizard(true, STANDARD_TEMPLATE_ID, STANDARD_CODE_ID);

  // --- members --------------------------------------------------------------

  /**
   * Selects a base Soy template.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String template;

  /**
   * Selects the JavaScript code for the wizard.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String code;

  /**
   * Allows to specify the identifier for the wizard form.
   *
   * @since 1.0
   */
  @XmlAttribute(name = "form-id")
  private String formId;

  /**
   * Flag to show if this wizard is configured or created by the system as
   * default.
   * <p>
   * The flag is needed so that we can ensure to return a non-null value for the
   * wizard. With this flag the client can check whether this is the default
   * provided by the system or not.
   * </p>
   *
   * @since 1.0
   */
  @XmlTransient
  private boolean isSystemDefault;

  /**
   * List of form parameters for the wizard to add to the Soy template.
   *
   * @since 1.1
   */
  @XmlElement(name = "form-param")
  private List<WizardFormParam> formParams;

  /**
   * List of fields for the wizard to add to the Soy template.
   *
   * @since 1.1
   */
  @XmlElement(name = "field")
  private List<WizardField> fields;

  /**
   * List of parameters for the wizard to add to the Soy template.
   *
   * @since 1.1
   */
  @XmlElement(name = "param")
  private List<WizardParam> params;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public Wizard() {
  }

  private Wizard(final boolean isSystemDefault, final String template,
      final String code) {
    this.isSystemDefault = isSystemDefault;
    this.template = template;
    this.code = code;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the value for template.
   * <p>
   * Selects a base Soy template.
   * </p>
   *
   * @return the value for template.
   */
  public String getTemplate() {
    if (StringUtils.isBlank(template)) {
      return STANDARD_TEMPLATE_ID;
    }
    return template;
  }

  /**
   * Returns the value for code.
   * <p>
   * Selects the JavaScript code for the wizard.
   * </p>
   *
   * @return the value for code.
   */
  public String getCode() {
    if (StringUtils.isBlank(code)) {
      return STANDARD_CODE_ID;
    }
    return code;
  }

  public String getFormId() {
    if (StringUtils.isBlank(formId)) {
      return "default-form";
    }
    return formId;
  }

  /**
   * Returns the value for systemDefault.
   * <p>
   * Flag to show if this wizard is configured or created by the system as
   * default.
   * </p>
   * <p>
   * The flag is needed so that we can ensure to return a non-null value for the
   * wizard. With this flag the client can check whether this is the default
   * provided by the system or not.
   * </p>
   *
   * @return the value for systemDefault.
   */
  public boolean isSystemDefault() {
    return isSystemDefault;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder
        .reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
  }

  /**
   * Returns the list of fields for the wizard to add to the Soy template.
   *
   * @return the ist of fields for the wizard.
   */
  public List<WizardField> getFields() {
    if (fields == null) {
      return Collections.emptyList();
    }
    return fields;
  }

  public String getAdditionalFormParametersXml() {
    if (formParams != null) {
      final StringBuilder buffer = new StringBuilder(128);

      buffer.append(" * \n");
      for (final WizardFormParam param : formParams) {
        buffer.append('\n');
        param.addAsXml(buffer);
      }

      return buffer.toString();
    }

    return StringUtils.EMPTY;
  }

  public List<WizardParam> getAdditionalParameters() {
    return params;
  }

  public String getAdditionalParametersXml() {
    if (params != null) {
      final StringBuilder buffer = new StringBuilder(128);

      for (final WizardParam param : params) {
        buffer.append('\n');
        param.addAsXml(buffer);
      }

      return buffer.toString();
    }

    return StringUtils.EMPTY;
  }
}
