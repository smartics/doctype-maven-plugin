/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import static de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.calcKey;

import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;

import org.apache.commons.lang.ObjectUtils;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Information on a property that is part of a projectdoc document.
 *
 * @since 1.0
 */
@XmlRootElement(name = "property")
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressFBWarnings("UWF_UNWRITTEN_FIELD")
public class SpaceProperty {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The key as the unique identifier for the property.
   *
   * @since 1.0
   */
  @XmlAttribute(required = true)
  private String key;

//  /**
//   * The content for the name of the property.
//   *
//   * @since 1.0
//   */
//  private PropertyPart name;

  /**
   * The content for the value of the property.
   *
   * @since 1.0
   */
  private PropertySpacePart value;

  /**
   * The content for the controls of the property.
   *
   * @since 1.0
   */
  private String controls;

  /**
   * Provides localized information.
   *
   * @since 1.0
   */
  @XmlElementWrapper(name = "resource-bundle")
  @XmlElement(name = "l10n")
  private List<SpacePropertyL10n> resourceBundle;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getKey() {
//    if (name != null) {
//      final String keyProvidedByName = name.getKey();
//      if (StringUtils.isNotBlank(keyProvidedByName)) {
//        return keyProvidedByName;
//      }
//    }

    return key;
  }

  /**
   * Returns the content for the name of the property.
   *
   * @return the content for the name of the property.
   */
  public String getName() {
//    if (name != null) {
//      return name.toString();
//    }

    return "<at:i18n at:key=\"" + key + "\"/>";
  }

  /**
   * Returns the content for the value of the property.
   *
   * @return the content for the value of the property.
   */
  public String getValue(final String packageId, final String doctypeId) {
    if (value != null) {
      return ObjectUtils.toString(value, null);
    }

    final String placeholderKey = calcKey(doctypeId, key + ".placeholder");
    final Placeholder placeholder = new Placeholder(placeholderKey);
    return placeholder.toString();
  }

  /**
   * Returns the content for the controls of the property.
   *
   * @return the content for the controls of the property.
   */
  public String getControls() {
    return ObjectUtils.toString(controls, null);
  }

  public List<SpacePropertyL10n> getResourceBundle() {
    if (resourceBundle != null) {
      return resourceBundle;
    }

    return Collections.emptyList();
  }

  /**
   * Checks whether or not the property value is a placeholder.
   *
   * @return <code>true</code> if the value of the property is a placeholder
   *         element, <code>false</code> otherwise.
   */
  public boolean hasPlaceholderValue() {
    return PropertySpacePart.hasPlaceholderValue(value);
  }

  // --- business -------------------------------------------------------------

  public void filter(final PlaceholderReplacer replacer) {
    if (value != null) {
      value.filter(replacer);
    }
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "  Key: " + key + "\n  Value: " + value + "\n  Controls: " + controls
        + "\n  Resource Bundle:\n" + resourceBundle;
  }
}
