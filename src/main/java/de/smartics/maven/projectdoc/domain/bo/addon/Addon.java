/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.addon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information global for all domain objects of the add-on.
 */
@XmlRootElement(name = "add-on")
@XmlAccessorType(XmlAccessType.FIELD)
public class Addon {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Provides localized information.
   *
   * @since 1.0
   */
  @XmlElementWrapper(name = "resource-bundle")
  @XmlElement(name = "l10n")
  private List<L10n> resourceBundle;

  @XmlAttribute(name = "default-locale")
  private String defaultLocale;

  @XmlElementWrapper(name = "categories")
  @XmlElement(name = "category")
  private List<Category> categories;

  @XmlElement(name = "import-doctypes")
  private ImportDoctypes importDoctypes;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public Addon() {}

  // ****************************** Inner Classes *****************************

  public static final class Bundle {
    private final L10n l10n;

    private Bundle(final L10n l10n) {
      this.l10n = l10n;
    }

    public String getLocale() {
      return l10n.getLocale();
    }

    public String getName() {
      return l10n.getName();
    }

    public String getDescription() {
      return l10n.getDescription();
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public List<Bundle> getBundles() {
    if (resourceBundle != null) {
      final List<Bundle> bundles = new ArrayList<Bundle>(resourceBundle.size());

      for (final L10n l10n : resourceBundle) {
        final Bundle bundle = new Bundle(l10n);
        bundles.add(bundle);
      }

      return bundles;
    }

    return Collections.emptyList();
  }

  @Nullable
  public Bundle getBundle(final String locale) {
    if (resourceBundle != null) {
      for (final L10n l10n : resourceBundle) {
        if (Objects.equals(locale, l10n.getLocale())) {
          final Bundle bundle = new Bundle(l10n);
          return bundle;
        }
      }
    }
    return null;
  }

  public String getDefaultLocale() {
    return defaultLocale;
  }

  public List<Category> getCategories() {
    if (categories != null) {
      return categories;
    }

    return Collections.emptyList();
  }

  public ImportDoctypes getImportDoctypes() {
    return importDoctypes != null ? importDoctypes : ImportDoctypes.EMPTY;
  }

  // --- business -------------------------------------------------------------

  public List<String> getSupportedLocales() {
    final List<Category> categories = getCategories();
    if (!categories.isEmpty()) {
      final Category arbitraryCategory = categories.get(0);
      final List<String> locales = arbitraryCategory.getSupportedLocales();
      if (!locales.isEmpty()) {
        return locales;
      }
    }
    return Arrays.asList(null, "de");
  }

  @Nullable
  public Category getCategory(final String categoryId) {
    for (final Category category : getCategories()) {
      if (categoryId.equals(category.getId())) {
        return category;
      }
    }
    return null;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    if (categories != null) {
      return categories.toString();
    }
    return "";
  }
}
