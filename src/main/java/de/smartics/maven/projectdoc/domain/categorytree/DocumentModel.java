/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.categorytree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Provides a model of a projectdoc document entity to be transfered to a
 * representation.
 *
 * @since 1.8
 */
@XmlRootElement(name = "document")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentModel implements Iterable<DocumentModel> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The list of properties of this document.
   */
  @XmlElement(name = "property")
  private final List<PropertyModel> property = new ArrayList<>();

  /**
   * The list of properties of this document.
   */
  @XmlElement(name = "section")
  private final List<SectionModel> section = new ArrayList<>();

  /**
   * The list of properties of this document.
   */
  @XmlElement(name = "children")
  private final List<DocumentModel> children = new ArrayList<>();

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * JAXB constructor.
   */
  public DocumentModel() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public List<PropertyModel> getProperties() {
    return property;
  }

  public Map<String, String> getPropertiesMap() {
    final Map<String, String> map = new HashMap<>();
    for (final PropertyModel model : getProperties()) {
      map.put(model.getName(), model.getValue());
    }
    return map;
  }

  public List<SectionModel> getSections() {
    return section;
  }

  @Override
  public Iterator<DocumentModel> iterator() {
    if (children != null) {
      return children.iterator();
    }
    return Collections.emptyIterator();
  }

  // --- business -------------------------------------------------------------

  public void addDocumentSection(final SectionModel sectionModel) {
    this.section.add(sectionModel);
  }

  public void addDocumentProperty(final String name, final String value,
      final String controls, final PropertyLocator.Position position) {
    final PropertyModel propertyModel =
        new PropertyModel(name, value, controls, position);
    this.property.add(propertyModel);
  }

  public void addDocumentProperty(final String name, final String value,
      final String controls, final String position, final String ref) {
    final PropertyModel propertyModel =
        new PropertyModel(name, value, controls, position, ref);
    this.property.add(propertyModel);
  }

  public void addDocumentProperty(final String name, final String value,
      final String controls, final String position) {
    addDocumentProperty(name, value, controls, position, null);
  }

  public void addChild(final DocumentModel model) {
    this.children.add(model);
  }

  @Nullable
  public String getPropertyAsString(final String propertyName) {
    for (final PropertyModel property : property) {
      if (propertyName.equals(property.getName())) {
        return property.getValue();
      }
    }

    return null;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    final StringBuilder buffer = new StringBuilder(8192);
    buffer.append("properties: ")
        .append(property)
        .append("\n sections: ")
        .append(section);
    for (final DocumentModel child : children) {
      buffer.append("\n===\n").append(child);
    }
    return buffer.toString();
  }
}
