/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.tool;

import java.io.File;

/**
 * Tool information for spaces.
 */
public class Tool {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The identifier of the tool.
   */
  private final String id;

  /**
   * The root directory providing resources for a tool.
   */
  private final File toolDir;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   *
   * @param toolDir the root directory providing resources for a tool.
   */
  public Tool(final File toolDir) {
    this.toolDir = toolDir;
    this.id = toolDir.getName();
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the identifier of the tool.
   *
   * @return the identifier of the tool.
   */
  public String getId() {
    return id;
  }

  /**
   * Returns the root directory providing resources for a tool.
   *
   * @return the root directory providing resources for a tool.
   */
  public File getToolDir() {
    return toolDir;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "  ID        : " + id;
  }
}
