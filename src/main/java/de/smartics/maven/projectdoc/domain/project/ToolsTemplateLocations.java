/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import static de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple.a;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class ToolsTemplateLocations extends AbstractTemplateLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ToolsTemplateLocations(final File toolsResourcesFolder) {
    super(toolsResourcesFolder);
  }

  // ****************************** Inner Classes *****************************

  public static enum Location implements ImportLocation {
    TEMPLATE("tools/specialpages-option.soy",
        "<!-- === projectdoc INSERT option === -->");

    private final String pathPattern;

    private final String marker;

    Location(final String pathPattern) {
      this(pathPattern, null);
    }

    Location(final String pathPattern, final String marker) {
      this.pathPattern = pathPattern;
      this.marker = marker;
    }

    @Override
    public String getMarker() {
      return marker;
    }

    @Override
    public String getPathPattern() {
      return pathPattern;
    }

    @Override
    public boolean isTextResource() {
      return false;
    }

    @Override
    public String truncatePath(final String path) {
      return path;
    }

    @Override
    public String toString(final String... args) {
      final PlaceholderReplacer replacer =
          new PlaceholderReplacer(a("${toolId}", args[0]));
      return replacer.replace(pathPattern);
    }
  }

  private static final class Resource {
    private final InputStream input;
    private final String encoding;

    private Resource(final ImportLocation location, final InputStream input) {
      this.input = input;
      this.encoding = "UTF-8";
    }

    private void close() {
      IOUtils.closeQuietly(input);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  private Resource getResource(final String toolId,
      final ImportLocation location) {
    // final String toolIdSoyFix = StringEscapeUtils.escapeHtml4(toolId);
    final String path = location.toString(toolId, null);

    final File providedFile;
    if (location.getPathPattern().contains("${toolId}")) {
      providedFile = new File(rootFolder, path);
    } else {
      final String resourceFileName = FilenameUtils.getName(path);
      providedFile = new File(new File(rootFolder, toolId), resourceFileName);
    }
    InputStream in;
    if (providedFile.canRead()) {
      try {
        in = FileUtils.openInputStream(providedFile);
      } catch (final IOException e) {
        in = this.getClass().getClassLoader().getResourceAsStream(path);
      }
    } else {
      in = this.getClass().getClassLoader().getResourceAsStream(path);
    }

    return in != null ? new Resource(location, new BufferedInputStream(in))
        : null;
  }

  public String getResourceAsString(final String toolId,
      final ImportLocation location) throws IOException {
    return getResourceAsString(toolId, location, (PlaceholderReplacer) null);
  }

  public String getResourceAsString(final String toolId,
      final ImportLocation location, final PlaceholderReplacer replacer)
      throws IOException {
    final Resource resource = getResource(toolId, location);
    if (resource != null) {
      try {
        final String template =
            IOUtils.toString(resource.input, resource.encoding);
        final String replaced = replacePlaceholders(template, replacer);
        return replaced;
      } finally {
        resource.close();
      }
    } else {
      return StringUtils.EMPTY;
    }
  }

  private String replacePlaceholders(final String template,
      final PlaceholderReplacer replacer) {
    if (replacer != null) {
      final String replaced = replacer.replace(template);
      return replaced;
    }
    return template;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
