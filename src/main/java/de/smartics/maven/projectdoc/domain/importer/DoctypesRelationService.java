/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.importer;

import de.smartics.maven.projectdoc.domain.ConfigurationException;
import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.Property;
import de.smartics.maven.projectdoc.domain.bo.doctype.Section;
import de.smartics.maven.projectdoc.domain.bo.doctype.TypeReferences;
import de.smartics.maven.projectdoc.domain.bo.doctype.TypeReferences.TypeInfo;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import java.io.File;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Responsible for applying relationship information between doctype.
 */
public class DoctypesRelationService implements Iterable<Doctype> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The naming convention requires every placeholder to have this prefix in its
   * name. This way the project service may determine if there are problems
   * resolving placeholders in a given phase.
   */
  private static final String RELATION_PLACEHOLDER_NAME_PREFIX = "relation#";

  // --- members --------------------------------------------------------------

  private final AddonRepository repository;

  private final DoctypeIterator iterator;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private DoctypesRelationService(final AddonRepository repository)
      throws SystemException {
    this.repository = repository;
    this.iterator = new DoctypeIterator();
  }

  // ****************************** Inner Classes *****************************

  private final class DoctypeIterator implements Iterator<Doctype> {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    private final Iterator<Doctype> iterator;

    private Doctype nextDoctype;

    // --- members ------------------------------------------------------------

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private DoctypeIterator() throws SystemException {
      iterator = repository.getDoctypes().iterator();
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    @Override
    public boolean hasNext() {
      if (nextDoctype != null) {
        return true;
      }

      while (iterator.hasNext()) {
        final Doctype doctype = iterator.next();
        if ("type".equals(doctype.getBaseTemplateName())) {
          nextDoctype = doctype;
          return true;
        }
      }

      return false;
    }

    @Override
    public Doctype next() throws ConfigurationException {
      if (nextDoctype != null) {
        final Doctype doctype = nextDoctype;
        nextDoctype = null;
        return resolve(doctype);
      }

      while (iterator.hasNext()) {
        final Doctype doctype = iterator.next();
        if ("type".equals(doctype.getBaseTemplateName())) {
          return resolve(doctype);
        }
      }

      throw new NoSuchElementException("No more doctypes available!");
    }

    private Doctype resolve(final Doctype doctype) {
      final PlaceholderReplacer replacer = createBaseTemplateReplacer(doctype);

      for (final Property property : doctype.getProperties()) {
        property.filter(replacer);
      }

      for (final Section section : doctype.getSections()) {
        section.filter(replacer);
      }

      return doctype;
    }

    private PlaceholderReplacer createBaseTemplateReplacer(
        final Doctype doctype) {
      final PlaceholderReplacer replacer = new PlaceholderReplacer();
      final TypeReferences types = repository.getTypeInfoFor(doctype);
      if (!types.isEmpty()) {
        final TypeInfo type = types.calcType();
        final String typeForDoctypeId = type.getForDoctypeIds();
        final String refByPropertyKey = type.getReferencedByPropertyKeys();
        replacer.add(rph("typeForDoctypeId"), typeForDoctypeId);
        replacer.add(rph("refByPropertyKey"), refByPropertyKey);
        if (type.isAnyDoctype()) {
          replacer.add(rph("instancesOfType_Select"),
              "<at:i18n at:key=\"projectdoc.doctype.common.name\"/>,"
                  + " <at:i18n at:key=\"projectdoc.doctype.common.doctype\"/>,"
                  + " <at:i18n at:key=\"projectdoc.doctype.common.shortDescription\"/>");
          replacer.add(rph("instancesOfType_Translations"),
              "<at:i18n at:key=\"projectdoc.doctype.common.doctype\"/>="
                  + "<at:i18n at:key=\"projectdoc.doctype.common.type\"/>");
        } else {
          replacer.add(rph("instancesOfType_Select"),
              "<at:i18n at:key=\"projectdoc.doctype.common.name\"/>,"
                  + " <at:i18n at:key=\"projectdoc.doctype.common.shortDescription\"/>");
          replacer.add(rph("instancesOfType_Translations"), " ");
        }
      }
      return replacer;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException(
          "Removing doctypes not supported.");
    }

    // --- object basics ------------------------------------------------------
  }

  private static final String rph(final String name) {
    return "${" + RELATION_PLACEHOLDER_NAME_PREFIX + name + '}';
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static DoctypesRelationService create(final AddonRepository repository)
      throws ConfigurationException, SystemException {
    final ProjectConfiguration config = repository.getConfig();
    final File folder = config.getDoctypesFolder();
    if (!folder.exists()) {
      throw new ConfigurationException("Folder '" + folder.getAbsolutePath()
          + "', which should contain doctype definition files, does not exist.");
    }
    if (!folder.isDirectory()) {
      throw new ConfigurationException("Folder '" + folder.getAbsolutePath()
          + "', which should contain doctype definition files, exists, but is not a folder.");
    }
    if (!folder.canRead()) {
      throw new ConfigurationException("Folder '" + folder.getAbsolutePath()
          + "', which should contain doctype definition files, exists, but cannot be read.");
    }

    return new DoctypesRelationService(repository);
  }

  @Override
  public Iterator<Doctype> iterator() {
    return iterator;
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
