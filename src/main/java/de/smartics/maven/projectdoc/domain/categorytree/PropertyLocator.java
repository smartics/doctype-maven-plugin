/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.categorytree;

import com.google.common.base.Objects;

import org.apache.commons.lang3.StringUtils;

/**
 * Defines the location of a property.
 *
 * @since 1.8
 */
public class PropertyLocator {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Defines the relative position to which the property is located.
   */
  private final Position position;

  private final PropertyRef propertyRef;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public PropertyLocator(final String refPropertyName) {
    this(Position.REPLACE_OR_ADD, new PropertyRef(refPropertyName));
  }

  /**
   * Default constructor.
   */
  public PropertyLocator(final PropertyRef propertyRef) {
    this(Position.REPLACE_OR_ADD, null);
  }

  public PropertyLocator(final String position,
      final String referenceProperty) {
    this(PropertyLocator.Position.toPosition(position),
        new PropertyLocator.PropertyRef(referenceProperty));
  }

  /**
   * Default constructor.
   */
  public PropertyLocator(final Position position,
      final PropertyRef propertyRef) {
    this.position = position;
    this.propertyRef = propertyRef;
  }

  // ****************************** Inner Classes *****************************

  public static enum Position {
    /**
     * Insert the new property before the reference property.
     */
    BEFORE("before"),

    /**
     * Insert the new property after the reference property.
     */
    AFTER("after"),

    /**
     * Replaces the current property with a new one.
     */
    REPLACE("replace"),

    /**
     * Only empty values in the existing property are overridden.
     */
    MERGE("merge"),

    /**
     * Deletes and inserts property values.
     */
    REPLACE_VALUES("replace-values"),

    /**
     * Merges property values into the existing list of property values.
     */
    MERGE_VALUES("merge-values"),

    /**
     * Inserts the new property as the first property in the list of properties.
     */
    FIRST("first"),

    /**
     * Inserts the new property as the last property in the list of properties.
     */
    LAST("last"),

    /**
     * Replaces the current property with a new one and if that property does
     * not exist, adds it as the last property to the list of properties.
     */
    REPLACE_OR_ADD("replace-or-add"),

    /**
     * Deletes the property.
     */
    DELETE("delete"),

    /**
     * Deletes values from the existing list of property values.
     */
    DELETE_VALUES("delete-values");

    private final String id;

    private Position(final String id) {
      this.id = id;
    }

    public static Position from(final String string)
        throws IllegalArgumentException {
      for (final Position position : values()) {
        if (string.equals(position.id)) {
          return position;
        }
      }

      throw new IllegalArgumentException("Unknown position: " + string);
    }

    public static Position toPosition(final String position)
        throws IllegalArgumentException {
      return StringUtils.isBlank(position) ? Position.REPLACE_OR_ADD
          : Position.from(position);
    }

    @Override
    public String toString() {
      return id;
    }

    public boolean isAdding() {
      return this == BEFORE || this == AFTER || this == FIRST || this == LAST;
    }
  }

  public static final class PropertyRef {
    /**
     * The name is used to identify the property.
     */
    private final String name;

    public PropertyRef(final String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public boolean hasName() {
      return StringUtils.isNotBlank(name);
    }

    public boolean hasName(final String name) {
      return Objects.equal(this.name, name);
    }

    @Override
    public String toString() {
      return name;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public Position getPosition() {
    return position;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return position + " '" + propertyRef + "\'";
  }
}
