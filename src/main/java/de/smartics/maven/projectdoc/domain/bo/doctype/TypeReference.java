/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Objects;

/**
 * Information on type information for a doctype.
 *
 * @since 1.0
 */
public class TypeReference {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The identifier of the doctype the property is part of. If <code>null</code>
   * any doctype with the given property matches.
   *
   * @since 1.0
   */
  private String forDoctypeId;

  /**
   * The reference to the property that references instances of the type.
   *
   * @since 1.0
   */
  private String referencedByPropertyKey;

  /**
   * The name of the property that identifies the referenced document.
   *
   * @since 2.3
   */
  private String uniqueIdentifierPropertyKey;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   *
   * @param forDoctypeId                the identifier of the doctype the
   *                                    property is part of.
   * @param referencedByPropertyKey     the reference to the property that
   *                                    references instances of the type.
   * @param uniqueIdentifierPropertyKey the name of the property that identifies
   *                                    the referenced document.
   */
  public TypeReference(final String forDoctypeId,
      final String referencedByPropertyKey,
      final String uniqueIdentifierPropertyKey) {
    this.forDoctypeId = forDoctypeId;
    this.referencedByPropertyKey = referencedByPropertyKey;
    this.uniqueIdentifierPropertyKey = uniqueIdentifierPropertyKey;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the identifier of the doctype the property is part of. If
   * <code>null</code> any doctype with the given property matches.
   *
   * @return the identifier of the doctype the property is part of.
   */
  public String getForDoctypeId() {
    return isAnyDoctype() ? " " : forDoctypeId;
  }

  /**
   * Returns the reference to the property that references instances of the
   * type.
   *
   * @return the reference to the property that references instances of the
   * type.
   */
  public String getReferencedByPropertyKey() {
    return referencedByPropertyKey;
  }

  /**
   * Returns the name of the property that identifies the referenced document.
   *
   * @return the name of the property that identifies the referenced document.
   *
   * @since 2.3
   */
  public String getUniqueIdentifierPropertyKey() {
    return uniqueIdentifierPropertyKey;
  }

  public boolean isAnyDoctype() {
    return StringUtils.isBlank(forDoctypeId);
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public int hashCode() {
    return Objects.hash(forDoctypeId, referencedByPropertyKey,
        uniqueIdentifierPropertyKey);
  }

  /**
   * Returns <code>true</code> if the given object is semantically equal to the
   * given object, <code>false</code> otherwise.
   *
   * @param object the instance to compare to.
   * @return <code>true</code> if the given object is semantically equal to the
   * given object, <code>false</code> otherwise.
   */
  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    } else if (object == null || getClass() != object.getClass()) {
      return false;
    }

    final TypeReference other = (TypeReference) object;

    return (Objects.equals(forDoctypeId, other.forDoctypeId) &&
            Objects.equals(referencedByPropertyKey,
                other.referencedByPropertyKey) &&
            Objects.equals(uniqueIdentifierPropertyKey,
                other.uniqueIdentifierPropertyKey));
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this,
        ToStringStyle.SHORT_PREFIX_STYLE);
  }
}