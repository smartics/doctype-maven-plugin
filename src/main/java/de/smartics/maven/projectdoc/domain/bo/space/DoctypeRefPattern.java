/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * References one or more doctypes.
 */
@XmlRootElement(name = "doctype-ref")
@XmlAccessorType(XmlAccessType.FIELD)
public class DoctypeRefPattern {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The identifier of the doctype.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String id;

  /**
   * The regular expression to match for doctype IDs.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String pattern;

  @XmlTransient
  private Pattern compiledPattern;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the identifier of the doctype. May be a pattern.
   *
   * @return the identifier of the doctype.
   */
  public String getPattern() {
    return pattern;
  }

  // --- business -------------------------------------------------------------

  public boolean matches(final String id) {
    if (this.id != null) {
      return this.id.equals(id);
    } else if(this.pattern == null) {
      return false;
    }

    if (compiledPattern == null) {
      compiledPattern = Pattern.compile(this.pattern);
    }

    final Matcher matcher = compiledPattern.matcher(id);
    final boolean match = matcher.matches();
    return match;
  }

  // --- object basics --------------------------------------------------------

}
