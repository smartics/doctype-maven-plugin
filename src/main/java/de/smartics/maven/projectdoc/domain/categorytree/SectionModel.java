/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.categorytree;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Provides a model of a projectdoc document section to be transfered to a
 * representation.
 *
 * @since 1.8
 */
@XmlRootElement(name = "section")
@XmlAccessorType(XmlAccessType.FIELD)
public class SectionModel {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The unique identifier of the document the section belongs to.
   */
  @XmlAttribute(name = "document-id")
  private Long id;

  /**
   * The URL to the document.
   */
  @XmlElement(name = "document-url")
  private String documentUrl;

  /**
   * The title or ID of the section.
   */
  @XmlElement
  private String title;

  /**
   * The string representation of the section content.
   */
  @XmlElement
  private String content;

  /**
   * The position to apply to the section.
   */
  @XmlElement
  private String position;

  /**
   * The reference section for the position.
   * <p>
   * In case of renaming the section this property holds the title of the
   * section before it is renamed. This is only relevant for batch updates.
   * </p>
   */
  @XmlElement(name = "ref")
  private String referenceSection;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * JAXB constructor.
   */
  public SectionModel() {}

  /**
   * Default constructor.
   */
  private SectionModel(final Long id, final String baseUrl, final String title,
      final String content, final String position,
      final String referenceSection) {
    this.id = id;
    this.documentUrl = baseUrl;
    this.title = title;
    this.content = content;
    this.position = position;
    this.referenceSection = referenceSection;
  }
  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  public static SectionModel create(final String documentUrl,
      final String title, final String content) {
    final SectionModel model =
        new SectionModel(null, documentUrl, title, content, null, null);
    return model;
  }

  public static SectionModel create(final String documentUrl,
      final String title, final String content, final String position) {
    final SectionModel model =
        new SectionModel(null, documentUrl, title, content, position, null);
    return model;
  }

  public static SectionModel create(final String documentUrl,
      final String title, final String content, final String position,
      final String referenceSection) {
    final SectionModel model = new SectionModel(null, documentUrl, title,
        content, position, referenceSection);
    return model;
  }

  // --- get&set --------------------------------------------------------------

  public Long getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getText() {
    return content;
  }

  public String getPositionString() {
    return position;
  }

  public SectionLocator.Position getPosition() throws IllegalArgumentException {
    return SectionLocator.Position.toPosition(position);
  }

  public String getReferenceSection() {
    return referenceSection;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return title;
  }
}
