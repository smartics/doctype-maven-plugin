/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Provides localized resource bundles.
 *
 * @since 1.0
 */
@XmlRootElement(name = "resource-bundle")
@XmlAccessorType(XmlAccessType.FIELD)
public class DoctypeResourceBundle implements
    Iterable<de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.Labels> {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String PROJECTDOC_DOCTYPE_COMMON_KEY_PREFIX =
      "projectdoc.doctype.common.";
  private static final int PROJECTDOC_DOCTYPE_COMMON_KEY_PREFIX_BEGIN_INDEX =
      PROJECTDOC_DOCTYPE_COMMON_KEY_PREFIX.length() - 1;

  // --- members --------------------------------------------------------------

  /**
   * The prefix for resource keys.
   * <p>
   * This is currently only relevant for label resources.
   * </p>
   *
   * @since 1.0
   */
  @XmlAttribute
  private String prefix;

  /**
   * List of localized bundles.
   *
   * @since 1.0
   */
  @XmlElement(name = "l10n")
  private List<L10n> resourceBundle;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DoctypeResourceBundle() {}

  public DoctypeResourceBundle(final List<L10n> resourceBundle) {
    this.resourceBundle = resourceBundle;
  }

  // ****************************** Inner Classes *****************************

  public final class Label {
    private final XmlLabel xml;

    private Label(final XmlLabel xml) {
      this.xml = xml;
    }

    public String getKey() {
      return (StringUtils.isNotBlank(prefix) ? prefix + '.' : "")
          + xml.getKey();
    }

    public String getValue() {
      return xml.getValue();
    }
  }

  public final class Labels implements Iterator<Label>, Iterable<Label> {
    private final L10n l10n;

    private final Iterator<XmlLabel> i;

    private Labels(final L10n l10n) {
      this.l10n = l10n;
      i = l10n.iterator();
    }

    public String getLocale() {
      return l10n.getLocale();
    }

    public Term getName() {
      return l10n.getName();
    }

    public String getDescription() {
      return l10n.getDescription();
    }

    public Term getType() {
      return l10n.getType();
    }

    public String getIntro() {
      return l10n.getIntro();
    }

    public String getExtro() {
      return l10n.getExtro();
    }

    public String getAbout() {
      return l10n.getAbout();
    }

    @Override
    public Iterator<Label> iterator() {
      return this;
    }

    @Override
    public String toString() {
      return l10n.toString();
    }

    @Override
    public boolean hasNext() {
      return i.hasNext();
    }

    @Override
    public Label next() {
      return new Label(i.next());
    }

    @Override
    public void remove() throws UnsupportedOperationException {
      throw new UnsupportedOperationException(
          "Iterator does not support remove.");
    }
  }


  public class LabelsIterator implements Iterator<Labels> {

    private final Iterator<L10n> i;

    private LabelsIterator(final List<L10n> list) {
      i = list.iterator();
    }

    @Override
    public boolean hasNext() {
      return i.hasNext();
    }

    @Override
    public Labels next() {
      return new Labels(i.next());
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException(
          "Iterator does not support remove.");
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public Iterator<Labels> iterator() {
    if (resourceBundle != null) {
      return new LabelsIterator(resourceBundle);
    }

    return new LabelsIterator(Collections.<L10n> emptyList());
  }

  @Nullable
  public Labels getLabels(String locale) {
    for (final Labels labels : this) {
      if (Objects.equals(locale, labels.getLocale())) {
        return labels;
      }
    }

    return null;
  }

  public static String calcKey(final String doctypeId, final String key) {
    if (key != null && key.startsWith(PROJECTDOC_DOCTYPE_COMMON_KEY_PREFIX)) {
      final String suffix =
          key.substring(PROJECTDOC_DOCTYPE_COMMON_KEY_PREFIX_BEGIN_INDEX);
      return "projectdoc.doctype." + doctypeId + suffix;
    } else {
      return key;
    }
  }

  // --- object basics --------------------------------------------------------

}
