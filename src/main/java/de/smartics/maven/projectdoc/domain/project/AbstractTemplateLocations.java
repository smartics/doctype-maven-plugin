/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public abstract class AbstractTemplateLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  protected final File rootFolder;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  protected AbstractTemplateLocations(final File rootFolder) {
    this.rootFolder = rootFolder;
  }

  // ****************************** Inner Classes *****************************

  protected static final class Resource {
    private final InputStream input;
    private final String encoding;

    private Resource(final ImportLocation location, final InputStream input) {
      this.input = input;
      this.encoding = "properties".equals(
          FilenameUtils.getExtension(location.getPathPattern())) ? "ISO-8859-1"
              : "UTF-8";
    }

    private void close() {
      IOUtils.closeQuietly(input);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  private Resource getResource(final ImportLocation location,
      final String... locationParams) {
    final String path = location.toString(locationParams);

    final String resourceFileName = location.truncatePath(path);
    final File providedFile = new File(rootFolder, resourceFileName);
    InputStream in;
    if (providedFile.canRead()) {
      try {
        in = FileUtils.openInputStream(providedFile);
      } catch (final IOException e) {
        in = this.getClass().getClassLoader().getResourceAsStream(path);
      }
    } else {
      in = this.getClass().getClassLoader().getResourceAsStream(path);
    }

    return in != null ? new Resource(location, new BufferedInputStream(in))
        : null;
  }

  public String getResourceAsString(final String spaceId,
      final ImportLocation location) throws IOException {
    return getResourceAsString(spaceId, location, null, null,
        new String[] {spaceId});
  }

  public String getResourceAsString(final String spaceId,
      final ImportLocation location, final String locale) throws IOException {
    return getResourceAsString(spaceId, location, locale, null,
        new String[] {spaceId});
  }

  public String getResourceAsString(final String spaceId,
      final ImportLocation location, final PlaceholderReplacer replacer)
      throws IOException {
    return getResourceAsString(spaceId, location, null, replacer,
        new String[] {spaceId});
  }

  public String getResourceAsString(final String spaceId,
      final ImportLocation location, final String locale,
      final PlaceholderReplacer replacer, final String[] locationParams)
      throws IOException {
    final Resource resource = getResource(location, locationParams);
    if (resource != null) {
      try {
        final String template =
            IOUtils.toString(resource.input, resource.encoding);
        final String replaced = replacePlaceholders(template, locale, replacer);
        return replaced;
      } finally {
        resource.close();
      }
    } else {
      if (!location.isTextResource()) {
        throw new IOException("Cannot find template '" + location + "' in set '"
            + spaceId + "'.");
      } else {
        if (locale != null) {
          return getResourceAsString(spaceId, location, null, replacer,
              locationParams);
        } else {
          return StringUtils.EMPTY;
        }
      }
    }
  }

  private String replacePlaceholders(final String template, final String locale,
      final PlaceholderReplacer replacer) {
    if (replacer != null) {
      final String replaced = replacer.replace(template);
      return replaced;
    }
    return template;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
