/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import static de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.calcKey;

import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information on a section that is part of a projectdoc document.
 *
 * @since 1.0
 */
@XmlRootElement(name = "section")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlSection implements Section {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @XmlAttribute
  private String key;

  /**
   * The name of the XML container to insert this section to.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String target;

  /**
   * The content of the section title.
   */
  private String xml;

  /**
   * Provides parameter configuration for the section macro.
   *
   * @since 1.0
   */
  @XmlElement(name = "config")
  private MacroConfig macroConfig;

  /**
   * Provides localized information.
   *
   * @since 1.0
   */
  @XmlElement(name = "resource-bundle")
  private DoctypeResourceBundle resourceBundle;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public String getKey() {
    return key;
  }

  @Override
  public boolean hasXml() {
    return StringUtils.isNotBlank(xml);
  }

  @Override
  public String getXml(final String packageId, final String doctypeId) {
    if (hasXml()) {
      return xml;
    }

    // FIXME: This should be extracted from the base doctype model
    final String placeholderKey = calcPlaceholderKey(doctypeId);
    final StringBuilder buffer = new StringBuilder(512);
    buffer.append("  <ac:structured-macro ac:name=\"projectdoc-section\">\n")
        .append("    <ac:parameter ac:name=\"title\">")
        .append(new Placeholder(key).toI18n()).append("</ac:parameter>\n");
    if (macroConfig != null) {
      buffer.append(macroConfig);
    }
    buffer.append("    <ac:rich-text-body>\n")
        .append(new Placeholder(placeholderKey))
        .append("    </ac:rich-text-body>\n  </ac:structured-macro>");
    return buffer.toString();
  }

  private String calcPlaceholderKey(final String doctypeId) {
    return calcKey(doctypeId, key + ".placeholder");
  }

  @Override
  public void initializeWithDefaults(final String doctypeId) {
    if (!hasXml()) {
      this.xml = getXml(null, doctypeId);
    }
  }

  @Override
  public DoctypeResourceBundle getResourceBundle() {
    if (resourceBundle != null) {
      return resourceBundle;
    }

    return new DoctypeResourceBundle(Collections.<L10n>emptyList());
  }

  @Override
  public void filter(final PlaceholderReplacer replacer) {
    if (StringUtils.isNotBlank(xml)) {
      xml = replacer.replace(xml);
    }
  }

  @Override
  public String getTarget() {
    return target;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "  XML: " + getXml("<packageId>", "<doctypeId>");
  }
}
