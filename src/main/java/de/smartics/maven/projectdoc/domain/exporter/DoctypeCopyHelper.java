/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.Wizard;
import de.smartics.maven.projectdoc.domain.bo.doctype.WizardField;
import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.DoctypeTemplateLocations;
import de.smartics.maven.projectdoc.domain.project.DoctypeTemplateLocations.Location;
import de.smartics.maven.projectdoc.domain.project.ImportLocation;
import de.smartics.maven.projectdoc.domain.project.ImportLocationManager;
import de.smartics.maven.projectdoc.domain.project.Mode;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import de.smartics.maven.projectdoc.domain.project.TemplateLocations;
import de.smartics.maven.projectdoc.domain.project.TemplateLocations.Context;
import de.smartics.maven.projectdoc.domain.project.WizardTemplateLocations;
import de.smartics.maven.projectdoc.io.FileFunction;
import de.smartics.maven.projectdoc.io.PropertiesHelper;
import de.smartics.maven.projectdoc.io.StringFunction;

import org.apache.commons.lang.StringUtils;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import javax.annotation.CheckForNull;

class DoctypeCopyHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final AddonRepository repository;

  private final ProjectConfiguration projectConfig;

  private final Doctype doctype;

  private final TemplateLocations importLocations;

  private final DoctypeExportLocations exportLocations;

  private final Mode mode;

  private final boolean skip;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  DoctypeCopyHelper(final AddonRepository repository, final Doctype doctype)
      throws IOException {
    this.repository = repository;
    this.projectConfig = repository.getConfig();
    this.doctype = doctype;

    this.importLocations = new ImportLocationManager(projectConfig, doctype);
    this.exportLocations =
        DoctypeExportLocations.create(projectConfig, doctype.getId());
    this.mode = projectConfig.getMode();
    this.skip =
        exportLocations.hasResource(DoctypeExportLocations.Location.TEMPLATE);
  }

  // ****************************** Inner Classes *****************************

  static final class ExportMarker {
    public static final ExportMarker PLAIN = new ExportMarker(null, null);
    private final String startMarker;
    private final String endMarker;

    ExportMarker(final String startMarker, final String endMarker) {
      this.startMarker = startMarker;
      this.endMarker = endMarker;
    }

    String mark(final String string) {
      if (startMarker != null) {
        return startMarker + string + endMarker;
      }
      return string;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public boolean skip() {
    return skip;
  }

  // --- business -------------------------------------------------------------

  void wizardSoy() throws SystemException {
    final Wizard wizard = doctype.getWizard();
    final String additionalWizardFormParameters =
        wizard.getAdditionalFormParametersXml();
    final String additionalWizardFields = createAdditionalFieldsXml(wizard);
    final String additionalWizardParameters =
        wizard.getAdditionalParametersXml();
    final PlaceholderReplacer replacer = new PlaceholderReplacer(
        Tuple.a("${formId}", wizard.getFormId()),
        Tuple.a(
            WizardTemplateLocations.Location.WIZARD_SOY_FORM_PARAM.getMarker(),
            additionalWizardFormParameters),
        Tuple.a(WizardTemplateLocations.Location.WIZARD_SOY_FIELD.getMarker(),
            additionalWizardFields),
        Tuple.a(WizardTemplateLocations.Location.WIZARD_SOY.getMarker(),
            additionalWizardParameters));
    copy(DoctypeExportLocations.Location.WIZARD_SOY,
        WizardTemplateLocations.Location.WIZARD_SOY, replacer);
  }

  private String createAdditionalFieldsXml(final Wizard wizard) {
    final StringBuilder buffer = new StringBuilder(1024);
    for (final WizardField field : wizard.getFields()) {
      final String xml = field.getXml();
      if (StringUtils.isNotBlank(xml)) {
        buffer.append(xml).append('\n');
      } else {
        final String template = field.getTemplate();

        final Context context = createWizardFieldContext(field, template);
        try {
          final String fragment = importLocations.getResourceAsString(context);
          buffer.append(fragment).append('\n');
        } catch (final IOException e) {
          throw new SystemException("Cannot add field '" + template
              + "' to wizard : " + e.getMessage(), e);
        }
      }
    }
    return buffer.toString();
  }

  private Context createWizardFieldContext(final WizardField field,
      final String template) {
    final PlaceholderReplacer replacer =
        new PlaceholderReplacer(Tuple.a("${id}", template));
    final String name = field.getName();
    if (StringUtils.isNotBlank(name)) {
      replacer.add("${fieldId}", name);
    }
    final String key = field.getKey();
    if (StringUtils.isBlank(key)) {
      switch (template) {
        case "short-description":
          replacer.add("${labelKey}",
              "projectdoc.blueprint.form.label.shortDescription");
          break;
        case "name":
          replacer.add("${labelKey}", "projectdoc.blueprint.form.label.name");
          break;
      }
    } else {
      replacer.add("${labelKey}", key);
    }
    final Integer height = field.getHeight();
    replacer.add("${height}", String.valueOf(height));
    final Context context = Context.Builder.a()
        .withLocation(WizardTemplateLocations.Location.WIZARD_SOY_FIELD)
        .withReplacer(replacer).build();
    return context;
  }

  void subspaceOptionSoy() throws SystemException {
    replaceContent(DoctypeExportLocations.Location.SUBSPACE_OPTION_SOY,
        DoctypeTemplateLocations.Location.SUBSPACE_OPTION_SOY, "UTF-8");
  }

  void subspaceListenerJava() {
    replaceContent(DoctypeExportLocations.Location.PARTITION_CONTEXT_PROVIDER_JAVA,
        DoctypeTemplateLocations.Location.PARTITION_CONTEXT_PROVIDER_JAVA, "UTF-8");
  }

  void wizardJs() throws SystemException {
    append(DoctypeExportLocations.Location.WIZARD_JS,
        WizardTemplateLocations.Location.WIZARD_JS,
        new ExportMarker("/* insert-mark " + doctype.getId() + "# */\n",
            "\n/* /insert-mark " + doctype.getId() + "# */"));
  }

  public void template() {
    final String packageId = projectConfig.getPackage();
    final String templateId = doctype.getBaseTemplateName();
    try {
      final Context context =
          Context.Builder.build(DoctypeTemplateLocations.Location.TEMPLATE);
      final String xml = importLocations.getResourceAsString(context);

      final TemplateWriter writer = new TemplateWriter(packageId, xml, doctype);

      final File file = exportLocations
          .getResource(DoctypeExportLocations.Location.TEMPLATE).getFile();
      handleFileIssues(file);
      FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", writer.toString());
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot create template " + templateId + ": " + e.getMessage(), e);
    }
  }

  private void handleFileIssues(final File file) throws IOException {
    if (file.exists()) {
      if (Mode.UPDATE == mode) {
        FileUtils.forceDelete(file);
      }
    } else {
      FileFunction.ensureContainingFolder(file);
    }
  }

  public void home(final String relatedDoctypeItems) {
    final PlaceholderReplacer replacer = new PlaceholderReplacer(
        Tuple.a(DoctypeTemplateLocations.Location.HOME.getMarker(),
            relatedDoctypeItems));
    copy(DoctypeExportLocations.Location.HOME,
        DoctypeTemplateLocations.Location.HOME, replacer);
  }

  public void writeToHome(final String homepageXml) {
    writeToFile(DoctypeExportLocations.Location.HOME, homepageXml);
  }

  public void index(final String relatedDoctypeItems) {
    final PlaceholderReplacer replacer = new PlaceholderReplacer(
        Tuple.a(DoctypeTemplateLocations.Location.INDEX.getMarker(),
            relatedDoctypeItems));
    if (doctype.hasHomepage()) {
      final String indexLinksPanel =
          readFragment(DoctypeTemplateLocations.Location.INDEX_LINKS_PANEL);
      replacer.add(
          DoctypeTemplateLocations.Location.INDEX_LINKS_PANEL.getMarker(),
          indexLinksPanel);
    }

    copy(DoctypeExportLocations.Location.INDEX,
        DoctypeTemplateLocations.Location.INDEX, replacer);
  }

  private String readFragment(final Location location) throws SystemException {
    try {
      final Context context =
          Context.Builder.a().withLocation(location).build();
      final String content = importLocations.getResourceAsString(context);
      return content;
    } catch (final IOException e) {
      throw new SystemException("Fetch fragment for "
          + location.getPathPattern() + ": " + e.getMessage(), e);
    }
  }

//  private void copy(final DoctypeExportLocations.Location export,
//      final ImportLocation location) {
//    copy(export, location, null);
//  }

  private void copy(final DoctypeExportLocations.Location export,
      final ImportLocation location, final PlaceholderReplacer replacer) {
    copy(export, location, "UTF-8", projectConfig.getLocale(), replacer);
  }

  private void copy(final DoctypeExportLocations.Location export,
      final ImportLocation location, final String encoding, final String locale,
      final PlaceholderReplacer replacer) {
    final File file = exportLocations.getResource(export).getFile();
    try {
      final Context context = Context.Builder.a().withLocation(location)
          .withLocale(locale).withReplacer(replacer).build();
      final String data = importLocations.getResourceAsString(context);
      handleFileIssues(file);
      FileUtils.fileWrite(file.getAbsolutePath(), encoding, data);
    } catch (final IOException e) {
      throw new SystemException("Cannot apply changes to "
          + file.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  private void writeToFile(final DoctypeExportLocations.Location export,
      final String xmlContent) {
    final File file = exportLocations.getResource(export).getFile();
    try {
      handleFileIssues(file);
      FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", xmlContent);
    } catch (final IOException e) {
      throw new SystemException("Cannot apply changes to "
          + file.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  public void properties() {
    final L10nHelper helper = L10nHelper.create(projectConfig, doctype);

    final PropertiesHelper propertiesHelper = new PropertiesHelper();
    for (final Map.Entry<String, Properties> entry : helper.getResourceBundles()
        .entrySet()) {
      final String locale = entry.getKey();

      if (Mode.UPDATE == mode) {
        final File file = exportLocations
            .getResource(DoctypeExportLocations.Location.TEXT_RESOURCE, locale)
            .getFile();
        propertiesHelper.mergeAndWriteProperties(entry.getValue(), file);
      } else {
        final String moreData = StringFunction.toData(entry.getValue());
        append(DoctypeExportLocations.Location.TEXT_RESOURCE,
            DoctypeTemplateLocations.Location.TEXT_RESOURCE, "ISO-8859-1",
            locale, moreData, ExportMarker.PLAIN);
//        final Properties additionalProperties = entry.getValue();
//        appendProperties(DoctypeExportLocations.Location.TEXT_RESOURCE,
//            DoctypeTemplateLocations.Location.TEXT_RESOURCE, "ISO-8859-1",
//            locale, additionalProperties, ExportMarker.PLAIN);
      }
    }

    final String defaultLocale = repository.getAddon().getDefaultLocale();
    if (StringUtils.isNotBlank(defaultLocale)) {
      final File defaultLocaleFile = exportLocations
          .getResource(DoctypeExportLocations.Location.TEXT_RESOURCE,
              defaultLocale)
          .getFile();
      if (defaultLocaleFile != null && defaultLocaleFile.canRead()) {
        final File currentDefaultLocaleFile = exportLocations
            .getResource(DoctypeExportLocations.Location.TEXT_RESOURCE, null)
            .getFile();
        try {
          FileUtils.copyFile(defaultLocaleFile, currentDefaultLocaleFile);
        } catch (final IOException e) {
          throw new SystemException("Cannot copy default locale properties "
              + defaultLocaleFile.getAbsolutePath() + " to "
              + currentDefaultLocaleFile.getAbsolutePath() + ": "
              + e.getMessage(), e);
        }
      } else {
        repository.getConfig()
            .info("Cannot find resource file for specified default locale "
                + defaultLocale + ". Skipping ...");
      }
    }
  }

  private void append(final DoctypeExportLocations.Location export,
      final ImportLocation location, final ExportMarker exportMarker) {
    append(export, location, "UTF-8", null, null, exportMarker);
  }

// Won't fix the encoding problem. Also does not preserve comments (yet).
//  private void appendProperties(final DoctypeExportLocations.Location export,
//      final ImportLocation location, final String encoding, final String locale,
//      final Properties moreData, final ExportMarker exportMarker) {
//    final File file = exportLocations.getResource(export, locale).getFile();
//    try {
//      final PropertiesConfiguration propertiesConfig =
//          new PropertiesConfiguration();
//      PropertiesHelper.normalizeLayout(propertiesConfig);
//      if (!file.exists()) {
//        FileFunction.ensureContainingFolder(file);
//      } else {
//        PropertiesHelper.readTo(propertiesConfig, file);
//      }
//
//      final PropertiesConfiguration morePropertiesConfig =
//          new PropertiesConfiguration();
//      final Context context =
//          Context.Builder.a().withLocation(location).withLocale(locale).build();
//      final String fileContent = importLocations.getResourceAsString(context);
//      morePropertiesConfig.read(new StringReader(fileContent));
//      if (!moreData.isEmpty()) {
//        final MapConfiguration mapConfig = new MapConfiguration(moreData);
//        morePropertiesConfig.append(mapConfig);
//      }
//      propertiesConfig.append(morePropertiesConfig);
//      PropertiesHelper.writeTo(propertiesConfig, file);
//    } catch (final IOException e) {
//      throw new SystemException("Cannot apply changes to "
//          + file.getAbsolutePath() + ": " + e.getMessage(), e);
//    } catch (final ConfigurationException e) {
//      throw new SystemException("Cannot apply changes to "
//          + file.getAbsolutePath() + ": " + e.getMessage(), e);
//    }
//  }

  private void append(final DoctypeExportLocations.Location export,
      final ImportLocation location, final String encoding, final String locale,
      final String moreData, final ExportMarker exportMarker) {
    final Context context =
        Context.Builder.a().withLocation(location).withLocale(locale).build();
    append(context, export, encoding, moreData, exportMarker);
  }

  private void append(final Context context,
      final DoctypeExportLocations.Location export, final String encoding,
      final String moreData, final ExportMarker exportMarker) {
    final File file =
        exportLocations.getResource(export, context.getLocale()).getFile();
    try {
      final String fileContent = importLocations.getResourceAsString(context);
      final String data = exportMarker
          .mark(fileContent + (moreData != null ? "\n\n" + moreData : ""));
      if (!file.exists()) {
        FileFunction.ensureContainingFolder(file);
        FileUtils.fileWrite(file.getAbsolutePath(), encoding, data);
      } else {
        FileUtils.fileAppend(file.getAbsolutePath(), encoding, "\n\n" + data);
      }
    } catch (final IOException e) {
      throw new SystemException("Cannot apply changes to "
          + file.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  public void pluginXml() {
    final String blueprint =
        fragment(DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_BLUEPRINT);
    final String webResource = optionalFragment(
        DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_WEB_RESOURCE);
    final String resource = webResource != null ? null
        : fragment(DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_RESOURCE);
    final String templateRef = fragment(
        DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_REF);
    final String templateHome = fragment(
        DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_HOME);
    final String template =
        fragment(DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE);

    final File file = exportLocations
        .getResource(DoctypeExportLocations.Location.ATLASSIAN_PLUGIN)
        .getFile();
    if (!file.exists()) {
      try {
        FileUtils.fileWrite(file.getParentFile().getAbsolutePath() + "/"
            + doctype.getId() + "-blueprint.xml", "UTF-8", blueprint);
        if (resource != null) {
          FileUtils.fileWrite(file.getParentFile().getAbsolutePath() + "/"
              + doctype.getId() + "-resource.xml", "UTF-8", resource);
        }
        if (webResource != null) {
          FileUtils.fileWrite(file.getParentFile().getAbsolutePath() + "/"
              + doctype.getId() + "-web-resource.xml", "UTF-8", webResource);
        }
        if (doctype.hasHomepage()) {
          FileUtils.fileWrite(file.getParentFile().getAbsolutePath() + "/"
              + doctype.getId() + "-template-ref.xml", "UTF-8", templateRef);
          FileUtils.fileWrite(file.getParentFile().getAbsolutePath() + "/"
              + doctype.getId() + "-template-home.xml", "UTF-8", templateHome);
        }
        FileUtils.fileWrite(file.getParentFile().getAbsolutePath() + "/"
            + doctype.getId() + "-template.xml", "UTF-8", template);
      } catch (final IOException e) {
        throw new SystemException("Cannot write fragments to "
            + file.getParentFile().getAbsolutePath() + ": " + e.getMessage(),
            e);
      }
    } else {
      try {
        String xml = FileUtils.fileRead(file, "UTF-8");

        if (resource != null) {
          xml = replaceXml(xml,
              DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_RESOURCE,
              resource);
        }
        if (webResource != null) {
          xml = replaceXml(xml,
              DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_WEB_RESOURCE,
              webResource);
        }

        for (final Space space : repository.getSpaces()) {
          if (space.isIncluded(doctype.getId())) {
            final PlaceholderReplacer replacer =
                new PlaceholderReplacer(Tuple.a("${spaceId}", space.getId()));

            xml = replaceXml(xml,
                DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_BLUEPRINT,
                blueprint, replacer);

            if (doctype.hasHomepage()) {
              xml = replaceXml(xml,
                  DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_REF,
                  templateRef, replacer);
            }
          }
        }
        if (doctype.hasHomepage()) {
          xml = replaceXml(xml,
              DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_HOME,
              templateHome, null);
        }

        xml = replaceXml(xml,
            DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE,
            template);
        FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", xml);
      } catch (final IOException e) {
        throw new SystemException("Cannot write atlassian-plugin.xml to "
            + file.getParentFile().getAbsolutePath() + ": " + e.getMessage(),
            e);
      }
    }
  }

  private void replaceContent(final DoctypeExportLocations.Location export,
      final DoctypeTemplateLocations.Location template, final String encoding) {
    final File file = exportLocations.getResource(export).getFile();
    if (file.exists()) {
      try {
        final String fragment = fragment(template);
        String xml = FileUtils.fileRead(file, encoding);
        xml = replace(xml, template, fragment, "\n", ExportMarker.PLAIN, null);
        FileUtils.fileWrite(file.getAbsolutePath(), encoding, xml);
      } catch (final IOException e) {
        throw new SystemException("Cannot apply changes to "
            + file.getAbsolutePath() + ": " + e.getMessage(), e);
      }
    }
  }

  private String replaceXml(final String content, final Location location,
      final String fragment) {
    return replaceXml(content, location, fragment, null);
  }

  private String replaceXml(final String content, final Location location,
      final String fragment, final PlaceholderReplacer replacer) {
    return replace(content, location, fragment, "\n\n",
        new ExportMarker(
            "<!-- insert-mark " + doctype.getId() + "-" + location.name()
                + "# -->\n",
            "\n<!-- /insert-mark " + doctype.getId() + "-" + location.name()
                + "# -->"),
        replacer);
  }

  private String replace(final String content, final Location location,
      final String fragment, final String separator,
      final ExportMarker exportMarker, final PlaceholderReplacer replacer) {
    String marker;
    if (this.doctype.isType()
        && location == DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_REF) {
      // FIXME: Should not be a special case ...
      marker =
          DoctypeTemplateLocations.ATLASSIAN_PLUGIN_TEMPLATE_REF_TYPE_MARKER;
    } else {
      marker = location.getMarker();
    }

    if (replacer != null) {
      marker = replacer.replace(marker);
    }

    final String replaced =
        StringUtils.replaceEach(content, new String[] {marker},
            new String[] {marker + separator + exportMarker.mark(fragment)});
    return replaced;
  }

  @CheckForNull
  private String optionalFragment(final ImportLocation template) {
    try {
      return fragment(template, null);
    } catch (final Exception e) {
      return null;
    }
  }

  private String fragment(final ImportLocation template) {
    return fragment(template, null);
  }

  private String fragment(final ImportLocation location, final String locale) {
    try {
      final Context context =
          Context.Builder.a().withLocation(location).withLocale(locale).build();
      final String data = importLocations.getResourceAsString(context);
      return data;
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot fetch fragment for atlassian-plugin.xml "
              + location.toString() + ": " + e.getMessage(),
          e);
    }
  }

  public void spaceHomepage() {
    if (doctype.isType()) {
      // Types are listed on the separate Types page.
      return;
    }
    final File file = exportLocations
        .getResource(DoctypeExportLocations.Location.SPACE_TEMPLATE).getFile();
    runOnPage(file);
  }

//  public void contentManagementDashboard() {
//    if (doctype.isType()) {
//      // Types are listed on the separate Types page.
//      return;
//    }
//    final File file = exportLocations
//        .getResource(
//            DoctypeExportLocations.Location.CONTENT_MANAGEMENT_TEMPLATE)
//        .getFile();
//    runOnPage(file);
//  }

  private void runOnPage(final File file) {
    if (!file.canRead()) {
      // Nothing adjust
      return;
    }

    try {
      final String fragment =
          fragment(DoctypeTemplateLocations.Location.HOMEPAGE_LINK);
      final String pageKey = "projectdoc.content." + doctype.getId() + '.'
          + (doctype.hasHomepage() ? "home" : "index.all") + ".title";
      final String pageLink =
          StringUtils.replace(fragment, "${pageKey}", pageKey);

      final String xml = FileUtils.fileRead(file, "UTF-8");
      String marker =
          DoctypeTemplateLocations.Location.HOMEPAGE_LINK.getMarker();

      if (marker.contains("${")) {
        marker = StringUtils.replaceEach(marker, new String[] {"${category}"},
            new String[] {doctype.getCategory()});
      }

      final String result =
          StringUtils.replace(xml, marker, marker + "\n\n" + pageLink);
      FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", result);
    } catch (final IOException e) {
      throw new SystemException("Cannot apply changes to space homepage "
          + file.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  // --- object basics --------------------------------------------------------

}
