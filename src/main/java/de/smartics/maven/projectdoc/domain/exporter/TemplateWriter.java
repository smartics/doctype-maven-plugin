/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.Property;
import de.smartics.maven.projectdoc.domain.bo.doctype.Section;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Document.OutputSettings.Syntax;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.nio.charset.Charset;

class TemplateWriter {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String PROPERTIES = "projectdoc-properties-container";

  private static final String SECTIONS = "projectdoc-sections-container";

  // --- members --------------------------------------------------------------

  private final String packageId;

  private final Document document;

  private final Doctype doctype;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  TemplateWriter(final String packageId, final String templateContents,
      final Doctype doctype) {
    this.packageId = packageId;
    this.document = Jsoup.parse(templateContents);
    this.doctype = doctype;

    configureXmlParsing(document);
    parse();
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  private void parse() {
    handleProperties();
    handleSections();
  }

  private void handleProperties() {
    final Element standardPropertiesContainer =
        locateContainer(PROPERTIES, "properties");

    final String doctypeId = doctype.getId();
    for (final Property property : doctype.getProperties()) {
      final Element tr = new Element("tr");
      tr.appendText(" \n                ");

      final Element th = new Element("th");
      th.attr("class", "confluenceTh");
      final String name = property.getName();
      if (name != null) {
        th.html(name);
      }
      tr.appendChild(th);
      tr.appendText("\n                ");

      final Element tdValue = new Element("td");
      tdValue.attr("class", "confluenceTd");
      final String value = property.getValue(packageId, doctypeId);
      if (value != null) {
        tdValue.html(value);
      }
      tr.appendChild(tdValue);
      tr.appendText("\n                ");

      final Element tdControls = new Element("td");
      tdControls.attr("class", "confluenceTd");
      final String controls = property.getControls();
      if (controls != null) {
        tdControls.html(controls);
      }
      tr.appendChild(tdControls);
      tr.appendText("\n              ");

      final String target = property.getTarget();
      final Element propertiesContainer =
          (target == null || PROPERTIES.equals(target))
              ? standardPropertiesContainer
              : locateContainer(target, "properties");

      propertiesContainer.appendChild(tr);
      propertiesContainer.appendText("\n              ");
    }
    standardPropertiesContainer.appendText("\n\n");
  }

  private void handleSections() {
    final Element container = locateContainer(SECTIONS, "sections");

    final String doctypeId = doctype.getId();
    for (final Section section : doctype.getSections()) {
      final String xml = section.getXml(packageId, doctypeId);
      if (xml != null) {
        final String target = section.getTarget();
        final Element sectionContainer =
            (target == null || SECTIONS.equals(target)) ? container
                : locateContainer(target, "sections");
        sectionContainer.append(xml + "\n\n      ");
      }
    }
  }

  private Element locateContainer(final String name, final String contextId) {
    final Elements containers =
        document.getElementsByAttributeValue("name", name);
    final Element container = containers.first();
    if (container == null) {
      throw new SystemException("Cannot find " + contextId
          + ", searching for element named '" + name + "'.");
    }
    return container;
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static void configureXmlParsing(final Document document)
      throws NullPointerException {
    // We cannot pretty print since Confluence is sensitive to line breaks (e.g.
    // in macro params)
    document.outputSettings(document.outputSettings().prettyPrint(false));
    document.outputSettings(document.outputSettings().syntax(Syntax.xml));
    document.charset(Charset.forName("UTF-8"));
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    final String xml =
        /* "<?xml version='1.0'?>\n\n" + */document.body().html();
    return xml;
  }

}
