/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.download;

import de.smartics.maven.projectdoc.domain.SystemException;

import org.apache.commons.io.IOUtils;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Helps to access homepage XML files within a JAR artifact.
 */
public class HomepageAccessorHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ArchiveExtractor extractor;

  private final String pathTemplate;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public HomepageAccessorHelper(final ArchiveExtractor extractor,
      final String pathTemplate) {
    this.extractor = extractor;
    this.pathTemplate = pathTemplate;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * Loads the homepage XML file from the embedded JAR.
   *
   * @return the homepage XML file.
   * @throws ReportException on any problem fetching the homepage.
   */
  public String load(final String addonId, final String doctypeId)
      throws SystemException {
    InputStream input = null;
    try {
      final String path =
          StringUtils.replace(pathTemplate, "${addonId}", addonId) + '/'
              + doctypeId + "/home.xml";
      input = extractor.open(path);
      if (input == null) {
        throw new SystemException(
            "Cannot find homepage XML for doctype '" + doctypeId + "'.");
      }
      final String xml = IOUtils.toString(input, "UTF-8");
      return xml;

    } catch (final IOException e) {
      throw new SystemException(
          "Cannot load homepage XML for doctype '" + doctypeId + "'.", e);
    } finally {
      IOUtil.close(input);
    }
  }

  // --- object basics --------------------------------------------------------

}
