/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.addon.RemoteDoctypeRef;
import de.smartics.maven.projectdoc.domain.bo.addon.RemoteDoctypeSet;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.AddonTemplateLocations;
import de.smartics.maven.projectdoc.domain.project.DoctypeTemplateLocations;
import de.smartics.maven.projectdoc.domain.project.DoctypeTemplateLocations.Location;
import de.smartics.maven.projectdoc.domain.project.Mode;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple;
import de.smartics.maven.projectdoc.domain.project.SpaceTemplateLocations;
import org.apache.commons.lang.StringUtils;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

class AddonCopyHelper extends CopyHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final boolean dataCenterApproved;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  AddonCopyHelper(final AddonRepository repository, final Mode mode)
      throws IOException {
    super(repository, "add-on", mode,
        AddonExportLocations.createForExport(repository.getConfig()),
        new SpaceTemplateLocations(repository.getConfig().getAddonFolder()));
    this.dataCenterApproved = repository.getConfig().isDataCenterApproved();
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void applyConfiguration() {
    final File atlassianPluginXmlFile = exportLocations.getResource(
        AddonExportLocations.Location.ATLASSIAN_PLUGIN).getFile();
    if (!atlassianPluginXmlFile.exists()) {
      return;
    }

    try {
      String atlassianPluginXml =
          FileUtils.fileRead(atlassianPluginXmlFile, "UTF-8");
      atlassianPluginXml = handleDataCenter(atlassianPluginXml);
      atlassianPluginXml = importedDoctypes(atlassianPluginXml);

      FileUtils.fileWrite(atlassianPluginXmlFile.getAbsolutePath(), "UTF-8",
          atlassianPluginXml);
    } catch (final IOException e) {
      throw new SystemException("Cannot write atlassian-plugin.xml to " +
                                atlassianPluginXmlFile.getParentFile()
                                    .getAbsolutePath() + ": " + e.getMessage(),
          e);
    }
  }

  private String handleDataCenter(String atlassianPluginXml) {
    if (dataCenterApproved) {
      final String fragment =
          fragment(AddonTemplateLocations.Location.DATA_CENTER_COMPATIBILITY);
      atlassianPluginXml =
          replace(PlaceholderReplacer.NO_OP, atlassianPluginXml,
              AddonTemplateLocations.Location.DATA_CENTER_COMPATIBILITY,
              fragment, "\n\n");
    }
    return atlassianPluginXml;
  }


  private String importedDoctypes(final String atlassianPluginXml) {
    String xml = atlassianPluginXml;
    final List<RemoteDoctypeSet> sets =
        repository.getAddon().getImportDoctypes().getDoctypeSets();
    if (sets.isEmpty()) {
      return atlassianPluginXml;
    }

    final String templateHome = fragment(
        AddonTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_HOME);

    for (final RemoteDoctypeSet set : sets) {
      repository.getConfig().info("Importing homepages from " + set.getId());

      final Tuple packageIdTuple = Tuple.a("${package}", packageId);
      final Tuple packagePathTuple =
          Tuple.a("${packagePath}", packageInPathFormat);

      for (final RemoteDoctypeRef doctypeRef : set.getDoctypeRefs()) {
        if (!doctypeRef.hasHomepage()) {
          continue;
        }
        final PlaceholderReplacer replacer3 =
            new PlaceholderReplacer(Tuple.a("${doctypeId}", doctypeRef.getId()),
                packageIdTuple, packagePathTuple);
        // standard-template.xml
        xml = replaceXml(xml,
            DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_HOME,
            templateHome, replacer3);
      }
    }
    return xml;
  }

  private String replaceXml(final String content, final Location location,
      final String fragment, final PlaceholderReplacer replacer) {
    return replace(content, location, replacer.replace(fragment), "\n\n",
        new de.smartics.maven.projectdoc.domain.exporter.DoctypeCopyHelper.ExportMarker(
            "<!-- insert-mark standard-templates-" + location.name() +
            "# -->\n",
            "\n<!-- /insert-mark standard-templates-" + location.name() +
            "# -->"), replacer);
  }

  private String replace(final String content, final Location location,
      final String fragment, final String separator,
      final de.smartics.maven.projectdoc.domain.exporter.DoctypeCopyHelper.ExportMarker exportMarker,
      final PlaceholderReplacer replacer) {
    final String marker = replacer.replace(location.getMarker());
    final String replaced =
        StringUtils.replaceEach(content, new String[] {marker},
            new String[] {marker + separator + exportMarker.mark(fragment)});
    return replaced;
  }

  // --- object basics --------------------------------------------------------

}
