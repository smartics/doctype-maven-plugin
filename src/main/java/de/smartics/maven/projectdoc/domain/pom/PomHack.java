/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.pom;

import de.smartics.maven.projectdoc.mojo.Dependency;
import de.smartics.maven.projectdoc.mojo.PluginArtifact;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Organization;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * A quick hack to supply a POM with needed information.
 * <p>
 * The PomHandler copies information from a local POM file provided in the
 * user's .m2 folder. This copies information from the project context. We
 * cannot use information from outside the project context since the project
 * (which is pulled from a repository) must be able to create everything.
 * </p>
 * <p>
 * We should handle the information using the Maven Model (as PomHandler does).
 * </p>
 */
public class PomHack {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final MavenProject mavenProject;

  private final List<Dependency> dependencies;

  private final List<PluginArtifact> pluginArtifacts;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public PomHack(final MavenProject mavenProject,
      final List<Dependency> dependencies,
      final List<PluginArtifact> pluginArtifacts) {
    this.mavenProject = mavenProject;
    this.dependencies = dependencies;
    this.pluginArtifacts = pluginArtifacts;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void applyToPom(final File projectFolder)
      throws MojoExecutionException {
    StringBuilder dependenciesBuffer = null;
    StringBuilder pluginArtifactsBuffer = null;
    final InstructionsHandler handler = new InstructionsHandler();
    if (dependencies != null && !dependencies.isEmpty()) {
      dependenciesBuffer = new StringBuilder(1024);
      for (final Dependency dependency : dependencies) {
        dependency.toXml(dependenciesBuffer);
        dependency.toInstructions(handler);
      }
    }
    if (pluginArtifacts != null && !pluginArtifacts.isEmpty()) {
      pluginArtifactsBuffer = new StringBuilder(1024);
      for (final PluginArtifact pluginArtifact : pluginArtifacts) {
        pluginArtifact.toXml(pluginArtifactsBuffer);
      }
    }

    final File pomFile = new File(projectFolder, "pom.xml");
    try {
      String content = FileUtils.readFileToString(pomFile, "UTF-8");

      final StringBuilder buffer = new StringBuilder(512);
      // TODO: License, Developers, ...
      final Organization organization = mavenProject.getOrganization();
      if (organization != null) {
        buffer.append("  <organization>\n");
        buffer.append("    <name>").append(organization.getName())
            .append("</name>\n");
        final String url = organization.getUrl();
        if (StringUtils.isNotBlank(url)) {
          buffer.append("    <url>").append(url).append("</url>\n");
        }
        buffer.append("  </organization>\n");
      }

      if (buffer.length() > 0) {
        content = content.replace("</inceptionYear>",
            "</inceptionYear>\n\n" + buffer);
      }

      if (dependenciesBuffer != null || pluginArtifactsBuffer != null) {

        if (dependenciesBuffer != null) {
          content = content.replace("<!-- projectdoc: dependencies -->",
              "<!-- projectdoc: dependencies -->" + dependenciesBuffer);
          content = content.replace("<!-- projectdoc: instructions -->",
              "<!-- projectdoc: instructions -->" + handler);
        }
        if (pluginArtifactsBuffer != null) {
          content = content.replace("<!-- projectdoc: plugin artifacts -->",
              "<!-- projectdoc: plugin artifacts -->" + pluginArtifactsBuffer);
        }
      }

      FileUtils.write(pomFile, content, "UTF-8");
    } catch (final IOException e) {
      throw new MojoExecutionException(
          "Cannot add plugin artifacts to POM file '"
              + pomFile.getAbsolutePath() + "': " + e.getMessage(),
          e);
    }
  }

  // --- object basics --------------------------------------------------------

}
