/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.settings;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * Project directory layout within the project settings providing defaults.
 */
@XmlRootElement(name = "reference")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reference {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The name of the reference.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String name;

  /**
   * The type of the reference indicating the type of system found at the end of
   * the reference.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String type;

  /**
   * The resource locator (URL) or identifier (URI).
   *
   * @since 1.0
   */
  @XmlValue
  private String locator;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public Reference() {

  }

  public Reference(final String name, final String type, final String locator) {
    this.name = name;
    this.type = type;
    this.locator = locator;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the name of the reference.
   *
   * @return the name of the reference.
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the type of the reference indicating the type of system found at
   * the end of the reference.
   *
   * @return the type of the reference indicating the type of system found at
   *         the end of the reference.
   */
  public String getType() {
    return type;
  }

  /**
   * Returns the resource locator (URL) or identifier (URI).
   *
   * @return the resource locator (URL) or identifier (URI).
   */
  public String getLocator() {
    return locator;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return locator;
  }
}
