/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;

/**
 * Provides a sections with defaults.
 */
public class CompoundSection implements Section {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Section primarySection;

  private final Section defaultSection;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public CompoundSection(final Section primarySection,
      final Section defaultSection) {
    this.primarySection = primarySection;
    this.defaultSection = defaultSection;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public String getKey() {
    String value = primarySection.getKey();
    if (value == null) {
      value = defaultSection.getKey();
    }
    return value;
  }

  @Override
  public boolean hasXml() {
    return primarySection.hasXml() || defaultSection.hasXml();
  }

  @Override
  public String getXml(final String packageId, final String doctypeId) {
    final String value;
    if (!primarySection.hasXml() && defaultSection.hasXml()) {
      value = defaultSection.getXml(packageId, doctypeId);
    } else {
      value = primarySection.getXml(packageId, doctypeId);
    }
    return value;
  }

  @Override
  public DoctypeResourceBundle getResourceBundle() {
    DoctypeResourceBundle value = primarySection.getResourceBundle();
    if (value == null) {
      value = defaultSection.getResourceBundle();
    }
    return value;
  }

  @Override
  public void filter(final PlaceholderReplacer replacer) {
    primarySection.filter(replacer);
    defaultSection.filter(replacer);
  }

  @Override
  public String getTarget() {
    String value = primarySection.getTarget();
    if (value == null) {
      value = defaultSection.getTarget();
    }
    return value;
  }

  @Override
  public void initializeWithDefaults(final String doctypeId) {
    if (!primarySection.hasXml()) {
      primarySection.initializeWithDefaults(doctypeId);
    }
  }

  // --- object basics --------------------------------------------------------

}
