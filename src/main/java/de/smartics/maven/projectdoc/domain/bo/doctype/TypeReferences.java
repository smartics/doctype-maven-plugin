/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Information on doctype references.
 */
public class TypeReferences implements Iterable<TypeReference> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final String anyDoctypePropertyKey;

  private final List<TypeReference> references = new ArrayList<TypeReference>();

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public TypeReferences() {
    this(null);
  }

  public TypeReferences(final String anyDoctypePropertyKey) {
    this.anyDoctypePropertyKey = anyDoctypePropertyKey;
  }

  // ****************************** Inner Classes *****************************

  public static final class TypeInfo {

    private final boolean anyDoctypes;

    private final String forDoctypeIds;

    private final String referencedByPropertyKeys;

    private TypeInfo(final boolean anyDoctypes, final String forDoctypeIds,
        final String referencedByPropertyKeys) {
      this.anyDoctypes = anyDoctypes;
      this.forDoctypeIds = forDoctypeIds;
      this.referencedByPropertyKeys = referencedByPropertyKeys;
    }

    public String getForDoctypeIds() {
      return forDoctypeIds;
    }

    public String getReferencedByPropertyKeys() {
      return referencedByPropertyKeys;
    }

    public boolean isAnyDoctype() {
      return anyDoctypes;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public Iterator<TypeReference> iterator() {
    return references.iterator();
  }

  public boolean isEmpty() {
    return references.isEmpty();
  }

  // --- business -------------------------------------------------------------

  public void add(final TypeReference reference) {
    if (!references.contains(reference)) {
      references.add(reference);
    }
  }

  public TypeInfo calcType() {
    final StringBuilder forDoctypeIds = new StringBuilder();
    final StringBuilder referencedByPropertyKeys = new StringBuilder();
    final boolean anyDoctypes = anyDoctypePropertyKey != null;

    if (anyDoctypes) {
      return new TypeInfo(anyDoctypes, " ",
          createWhereElement(anyDoctypePropertyKey));
    }

    for (final TypeReference reference : references) {
      final String doctypeId = reference.getForDoctypeId();
      if (forDoctypeIds.length() != 0) {
        forDoctypeIds.append(", ");
      }
      forDoctypeIds.append(doctypeId);

      final String propertyKey = reference.getReferencedByPropertyKey();
      if (referencedByPropertyKeys.length() != 0) {
        referencedByPropertyKeys.append(", ");
      }
      referencedByPropertyKeys.append(createWhereElement(propertyKey));
    }

    return new TypeInfo(anyDoctypes, forDoctypeIds.toString(),
        referencedByPropertyKeys.toString());
  }

  private String createWhereElement(final String propertyKey) {
    return "$&lt;<at:i18n at:key=\"" + propertyKey
        + "\"/>&gt; = [${<at:i18n at:key=\"projectdoc.doctype.common.name\"/>}]";
  }

  // --- object basics --------------------------------------------------------

}
