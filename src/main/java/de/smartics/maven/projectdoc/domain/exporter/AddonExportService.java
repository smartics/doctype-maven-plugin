/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.addon.Addon;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.categorytree.CategoryTreeJsonBuilder;
import de.smartics.maven.projectdoc.domain.docmap.DocmapJsonBuilder;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Writes add-on information to the add-on.
 */
public class AddonExportService {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final AddonRepository repository;

  private final DocmapJsonBuilder docmapBuilder;

  private final List<CategoryTreeJsonBuilder> categoryTreeBuilders;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public AddonExportService(final AddonRepository repository) {
    this.repository = repository;
    final ProjectConfiguration config = repository.getConfig();
    this.docmapBuilder = new DocmapJsonBuilder(config);
    categoryTreeBuilders =
        initCategoryTreeBuilders(config, repository.getAddon());
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  private static List<CategoryTreeJsonBuilder> initCategoryTreeBuilders(
      ProjectConfiguration config, Addon addon) {
    final List<CategoryTreeJsonBuilder> map = new ArrayList<>();

    for (final String locale : addon.getSupportedLocales()) {
      final CategoryTreeJsonBuilder builder =
          new CategoryTreeJsonBuilder(config, addon, locale);
      map.add(builder);
    }

    return map;
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public boolean export() throws SystemException {
    try {
      final ProjectConfiguration config = repository.getConfig();
      final AddonCopyHelper copy =
          new AddonCopyHelper(repository, config.getMode());
      copy.applyConfiguration();
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot create backup folder: " + e.getMessage(), e);
    }

    return true;
  }

  public void registerForDocmap(final Doctype doctype) {
    docmapBuilder.appendStandard(doctype);
  }

  public void registerForCategoryTree(final Doctype doctype) {
    for (final CategoryTreeJsonBuilder builder : categoryTreeBuilders) {
      builder.addDoctype(doctype);
    }
  }

  public void writeDocmap() throws SystemException {
    final File file =
        AddonExportLocations.createForExport(repository.getConfig())
            .getResource(AddonExportLocations.Location.DOCMAP).getFile();
    try {
      FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8",
          docmapBuilder.toString());
    } catch (final IOException e) {
      throw new SystemException("Cannot write docmap.json: " + e.getMessage(),
          e);
    }
  }

  public void writeCategoryTree() {
    final File folder =
        AddonExportLocations.createForExport(repository.getConfig())
            .getResource(AddonExportLocations.Location.CATEGORY_TREE).getFile();
    folder.mkdirs();

    for (final CategoryTreeJsonBuilder builder : categoryTreeBuilders) {
      final String fileName = builder.createFileName();
      final File file = new File(folder, fileName);
      try {
        FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8",
            builder.createContents());
      } catch (final IOException e) {
        throw new SystemException(
            "Cannot write " + fileName + ": " + e.getMessage(), e);
      }
    }
  }

  // --- object basics --------------------------------------------------------

}
