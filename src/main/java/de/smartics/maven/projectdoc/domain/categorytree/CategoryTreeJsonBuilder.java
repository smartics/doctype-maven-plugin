/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.categorytree;

import de.smartics.maven.projectdoc.domain.bo.addon.Addon;
import de.smartics.maven.projectdoc.domain.bo.addon.Addon.Bundle;
import de.smartics.maven.projectdoc.domain.bo.addon.Category;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle;
import de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.Labels;
import de.smartics.maven.projectdoc.domain.categorytree.PropertyLocator.Position;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import de.smartics.maven.projectdoc.io.StringFunction;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.lang3.StringUtils;

import java.util.ResourceBundle;

import javax.annotation.Nullable;

/**
 * Creates a JSON document to create a category tree in a Confluence space based
 * on the projectdoc Toolbox.
 *
 * @since 1.8
 */
public class CategoryTreeJsonBuilder {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ProjectConfiguration config;
  private final Addon addon;
  private final String locale;

  private final DocumentsModel documents = new DocumentsModel();

  private final String shortDescriptionName;
  private final String doctypeName;
  private final String nameName;
  private final String iterationName;
  private final String sortKeyName;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public CategoryTreeJsonBuilder(final ProjectConfiguration config,
      final Addon addon, final String locale) {
    this.config = config;
    this.locale = locale;
    this.addon = addon;

    final ResourceBundle bundle = config.getResourceBundle(locale);
    this.shortDescriptionName =
        bundle.getString("projectdoc.doctype.common.shortDescription");
    this.doctypeName = bundle.getString("projectdoc.doctype.common.doctype");
    this.nameName = bundle.getString("projectdoc.doctype.common.name");
    this.iterationName =
        bundle.getString("projectdoc.doctype.common.iteration");
    this.sortKeyName = bundle.getString("projectdoc.doctype.common.sortKey");

    initCategories(addon);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  private DocumentModel initCategories(final Addon addon) {
    final Bundle bundle = addon.getBundle(locale);
    final String addonName;
    final String addonDescription;
    if (bundle != null) {
      addonName = StringFunction.strip(bundle.getName());
      addonDescription = StringFunction.strip(bundle.getDescription());
    } else {
      addonName = StringFunction.strip(config.getProjectName());
      addonDescription = addonName;
    }

    final DocumentModel rootModel =
        createCategoryDocumentModel(addonName, addonDescription);
    documents.addDocument(rootModel);

//    final DocumentModel defaultModel =
//        createCategoryDocumentModel("default", "DEFAULT");
//    rootModel.addChild(defaultModel);

    int counter = 10;
    for (final Category category : addon.getCategories()) {
      final String name = category.getName(locale);
      final String description = category.getDescription(locale);

      final String sortKey =
          StringUtils.leftPad(String.valueOf(counter), 4, "0");
      final DocumentModel categoryModel =
          createCategoryDocumentModel(name, description, sortKey);
      rootModel.addChild(categoryModel);
      counter += 10;
    }

    return rootModel;
  }

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void addDoctype(final Doctype doctype) {
    // final String doctypeId = doctype.getId();
    final String doctypeName = doctype.isType() ? doctype.getTypeName(locale)
        : doctype.getName(locale);

    final DoctypeResourceBundle bundle = doctype.getResourceBundle();

    final Labels l10n = bundle.getLabels(locale);
    if (l10n != null) {
      final String description = l10n.getAbout();
      final DocumentModel model =
          createCategoryDocumentModel(doctypeName, description);
      final String categoryName = calcCategoryName(doctype);
      if (StringUtils.isNotBlank(categoryName)) {
        final DocumentModel categoryModel =
            documents.getDocument(categoryName, nameName);
        if (categoryModel != null) {
          categoryModel.addChild(model);
        }
//      } else if (rootModel != null) {
//        rootModel.addChild(model);
//      } else {
//        documents.addDocument(model);
      }
    }
  }

  @Nullable
  private String calcCategoryName(final Doctype doctype) {
    final String categoryId = doctype.getCategory();
    if (StringUtils.isNotBlank(categoryId)) {
      final Category category = addon.getCategory(categoryId);
      if (category != null) {
        final String categoryName = category.getName(locale);
        return categoryName;
      }
    }

    return null;
  }

  private DocumentModel createCategoryDocumentModel(final String name,
      final String description) {
    return createCategoryDocumentModel(name, description, null);
  }

  private DocumentModel createCategoryDocumentModel(final String name,
      final String description, final String sortKey) {
    final DocumentModel model = new DocumentModel();

    if (StringUtils.isNotBlank(description)) {
      model.addDocumentProperty(shortDescriptionName,
          StringFunction.strip(description), null, Position.REPLACE);
    }
    model.addDocumentProperty(doctypeName, "category", null, Position.MERGE);
    model.addDocumentProperty(nameName, StringFunction.strip(name), null,
        Position.MERGE);
    model.addDocumentProperty(iterationName, "production", "hide",
        Position.REPLACE_VALUES);

    if (StringUtils.isNotBlank(sortKey)) {
      model.addDocumentProperty(sortKeyName, sortKey, "hide", Position.REPLACE);
    }

    return model;
  }

  public String createFileName() {
    return "category-tree" + (locale != null ? '-' + locale : StringUtils.EMPTY)
        + ".json";
  }

  public String createContents() {
    final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    final String content = gson.toJson(documents);
    return content;
  }

  // --- object basics --------------------------------------------------------

}
