/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * Represents a term with metadata.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Metadata {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The name that identifies the metadata.
   *
   * @since 1.1
   */
  @XmlAttribute
  private String key;

  /**
   * The value of the metadata
   *
   * @since 1.1
   */
  @XmlValue
  private String value;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static Metadata create(final String key, final String value) {
    final Metadata instance = new Metadata();
    instance.key = key;
    instance.value = value;
    return instance;
  }

  // --- get&set --------------------------------------------------------------

  public String getKey() {
    return StringUtils.trim(key);
  }

  public String getValue() {
    return StringUtils.trim(value);
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return key + '=' + value;
  }
}
