/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.importer;

import de.smartics.maven.projectdoc.domain.ConfigurationException;
import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import de.smartics.maven.projectdoc.io.XmlFileFilter;

import java.io.File;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Service to access to space definitions.
 */
public class SpacesImportService implements Iterable<Space> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ProjectConfiguration config;

  private final SpaceIterator iterator;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private SpacesImportService(final AddonRepository repository)
      throws SystemException {
    this.config = repository.getConfig();
    this.iterator = new SpaceIterator();
  }

  // ****************************** Inner Classes *****************************

  private final class SpaceAccessor {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final JAXBContext xmlContext;

    private final Unmarshaller unmarshaller;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private SpaceAccessor() throws JAXBException {
      this.xmlContext = JAXBContext.newInstance(Space.class);
      this.unmarshaller = xmlContext.createUnmarshaller();
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    private Space unmarshal(final File file) throws JAXBException {
      final Space space = (Space) unmarshaller.unmarshal(file);
      return space;
    }

    // --- object basics ------------------------------------------------------
  }

  private final class SpaceIterator implements Iterator<Space> {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final SpaceAccessor accessor;

    private final File[] spaceFiles;

    private int currentFileIndex;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private SpaceIterator() throws SystemException {
      try {
        this.accessor = new SpaceAccessor();
        this.spaceFiles = config.listSpaceFiles(XmlFileFilter.INSTANCE);
      } catch (final JAXBException e) {
        throw new SystemException(
            "Cannot access space files for XML parsing: " + e.getMessage(), e);
      }
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    @Override
    public boolean hasNext() {
      return currentFileIndex < spaceFiles.length;
    }

    @Override
    public Space next() throws ConfigurationException {
      if (hasNext()) {
        final File currentFile = spaceFiles[currentFileIndex];
        try {
          Space space = accessor.unmarshal(currentFile);

          currentFileIndex++;
          return space;
        } catch (final JAXBException e) {
          throw new ConfigurationException(
              "Cannot parse space definition from file '"
                  + currentFile.getAbsolutePath() + "' due to XML problems: "
                  + e.getMessage(),
              e);
        }
      }

      return null;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException("Removing spaces not supported.");
    }

    // --- object basics ------------------------------------------------------
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static SpacesImportService create(final AddonRepository repository)
      throws ConfigurationException, SystemException {
    final ProjectConfiguration config = repository.getConfig();
    final File folder = config.getSpacesFolder();
    if (!folder.exists()) {
      throw new ConfigurationException("Folder '" + folder.getAbsolutePath()
          + "', which should contain space definition files, does not exist.");
    }
    if (!folder.isDirectory()) {
      throw new ConfigurationException("Folder '" + folder.getAbsolutePath()
          + "', which should contain space definition files, exists, but is not a folder.");
    }
    if (!folder.canRead()) {
      throw new ConfigurationException("Folder '" + folder.getAbsolutePath()
          + "', which should contain space definition files, exists, but cannot be read.");
    }

    return new SpacesImportService(repository);
  }

  @Override
  public Iterator<Space> iterator() {
    return iterator;
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
