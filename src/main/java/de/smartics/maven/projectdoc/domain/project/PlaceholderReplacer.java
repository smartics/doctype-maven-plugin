/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * The replacer allows each template to define additional, template specific
 * replacements.
 */
public class PlaceholderReplacer {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  public static final PlaceholderReplacer NO_OP = new PlaceholderReplacer();

  // --- members --------------------------------------------------------------

  private final List<String> placeholders = new ArrayList<String>();
  private final List<String> replacements = new ArrayList<String>();

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public PlaceholderReplacer(final Tuple... tuples) {
    for (final Tuple tuple : tuples) {
      add(tuple.placeholder, tuple.replacement);
    }
  }

  public PlaceholderReplacer() {}

  // ****************************** Inner Classes *****************************

  public static final class Tuple {
    private final String placeholder;
    private final String replacement;

    private Tuple(final String placeholder, final String replacement) {
      this.placeholder = placeholder;
      this.replacement = replacement;
    }

    public static Tuple a(final String placeholder, final String replacement) {
      return new Tuple(placeholder, replacement);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public String replace(final String template) {
    final String[] placeholders =
        this.placeholders.toArray(new String[this.placeholders.size()]);
    final String[] replacements =
        this.replacements.toArray(new String[this.replacements.size()]);;

    final String value =
        StringUtils.replaceEach(template, placeholders, replacements);
    return value;
  }

  public final void add(final String placeholder, final String replacement) {
    placeholders.add(placeholder);
    replacements.add(replacement);
  }

  // --- object basics --------------------------------------------------------

}
