/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import static de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple.a;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class SpaceTemplateLocations extends AbstractTemplateLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private final static String FRAGMENTS = "spaces/main/fragments";

  private final static String ATLASSIAN_PLUGIN =
      FRAGMENTS + "/atlassian-plugin";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public SpaceTemplateLocations(final File spaceResourcesFolder) {
    super(spaceResourcesFolder);
  }

  // ****************************** Inner Classes *****************************

  public static enum Location implements ImportLocation {
    HOMEPAGE_LINKS(FRAGMENTS + "/homepage-links.xml",
        "<!-- projectdoc INSERT Homepage Links -->"),

    REGISTER_SPACE_WIZARD(FRAGMENTS + "/register-space-wizard.js",
        "/* === projectdoc INSERT register-space-wizard === */"),

    SPACE(FRAGMENTS + "/space.xml",
        "<!-- === projectdoc INSERT spaces === -->"),

    CREATE_SPACE("spaces/main/create-space.soy"),

    TEXT_RESOURCE("spaces/main/l10n${locale}.properties"),

    RESOURCE("spaces/main/resources.xml",
        "<!-- === projectdoc INSERT space-resources === -->"),

//    CONTENT_MANAGEMENT_DASHBOARD_TEMPLATE(
//        "spaces/main/pages/content-management-dashboard.xml"),

    TEMPLATE("spaces/main/template.xml"),

    ATLASSIAN_PLUGIN_BLUEPRINT(ATLASSIAN_PLUGIN + "-blueprint.xml",
        "<!-- === projectdoc INSERT standard-blueprint-ref ${spaceId} === -->"),

    ATLASSIAN_PLUGIN_RESOURCE(ATLASSIAN_PLUGIN + "-resource-js.xml",
        "<!-- === projectdoc INSERT standard-resource-js.xml === -->"),

    ATLASSIAN_PLUGIN_TEMPLATE_REF(ATLASSIAN_PLUGIN + "-template-ref.xml",
        "<!-- === projectdoc INSERT standard-template-home-ref ${spaceId} === -->"),

    ATLASSIAN_PLUGIN_TEMPLATE_HOME(ATLASSIAN_PLUGIN + "-template-home.xml",
        "<!-- === projectdoc INSERT standard-template === -->");

    private final String pathPattern;

    private final String marker;

    Location(final String pathPattern) {
      this(pathPattern, null);
    }

    Location(final String pathPattern, final String marker) {
      this.pathPattern = pathPattern;
      this.marker = marker;
    }

    @Override
    public String getMarker() {
      return marker;
    }

    @Override
    public String getPathPattern() {
      return pathPattern;
    }

    @Override
    public boolean isTextResource() {
      return this == TEXT_RESOURCE;
    }

    @Override
    public String truncatePath(final String path) {
      return FilenameUtils.getName(path);
    }

    @Override
    public String toString(final String... args) {
      final PlaceholderReplacer replacer;
      if (args != null && args.length > 0) {
        final String locale =
            args.length > 1 && args[1] != null ? '_' + args[1] : "";
        replacer = new PlaceholderReplacer(a("${spaceId}", args[0]),
            a("${locale}", locale));
      } else {
        replacer =
            new PlaceholderReplacer(a("${spaceId}", ""), a("${locale}", ""));
      }

      return replacer.replace(pathPattern);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
