/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information on a property part. The content may be plain XML or other
 * elements that are easier to write down.
 *
 * @since 1.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressFBWarnings("UWF_UNWRITTEN_FIELD")
public class PropertyPart {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @XmlAttribute
  private String key;

  /**
   * The content of the property part.
   */
  private String xml;

  private Macro macro;

  private Placeholder placeholder;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getKey() {
    return key;
  }

  // --- business -------------------------------------------------------------

  public static boolean hasPlaceholderValue(final PropertyPart value) {
    // If there is no value, the placeholder is implied.
    return value == null || value.placeholder != null;
  }

  @Nullable
  public TypeReference getTypeReference(final String owningDoctypeId) {
    if (macro != null) {
      final String macroName = macro.getName();
      if ("projectdoc-transclusion-parent-property".equals(macroName)) {
        return handleWithDefault("parent-doctype", owningDoctypeId, null,
            PropertyNameKey.NAME);
      } else if ("projectdoc-tag-list-macro".equals(macroName) ||
                 "projectdoc-tag-body-list-macro".equals(macroName)) {
        return handleWithDefault("doctype", "tag", null, PropertyNameKey.NAME);
      } else if ("projectdoc-role-list-macro".equals(macroName)) {
        return handleWithDefault("doctype", "role", null, PropertyNameKey.NAME);
      } else if ("projectdoc-transclusion-ancestor-property-macro".equals(
          macroName)) {
        return handleWithDefault("ancestor-doctype", owningDoctypeId,
            "property-name", PropertyNameKey.NAME);
      } else if ("projectdoc-name-list".equals(macroName) ||
                 "projectdoc-name-body-list".equals(macroName)) {
        return handleWithDefault("doctype", null, "property-name",
            PropertyNameKey.NAME);
      } else if (
          "projectdoc-transclusion-property-display-ref".equals(macroName) ||
          "projectdoc-transclusion-property-display-ref-concat".equals(
              macroName)) {
        // Only if the 'doctype' parameter is provided, the unique property
        // name is 'Name'
        return handleWithDefault("doctype", null, null, PropertyNameKey.NAME);
      }
    }
    // TODO: If the macro is provided as XML, we need to parse the storage XML.

    return null;
  }

  @Nullable
  private TypeReference handleWithDefault(final String paramName,
      @Nullable final String defaultDoctypeId,
      @Nullable final String refPropertyParamName,
      final String defaultRefPropertyName) {
    if (macro != null) {
      final MacroParam propertyNameParam = (null != refPropertyParamName ?
                                            macro.getParameter(
                                                refPropertyParamName) : null);
      final String uniquePropertyName =
          (null != propertyNameParam ? propertyNameParam.getKey() :
           defaultRefPropertyName);

      final MacroParam param = macro.getParameter(paramName);
      if (param != null) {
        final String doctypeId = param.getValue();
        return new TypeReference(doctypeId, key, uniquePropertyName);
      }
    }
    if (defaultDoctypeId != null) {
      return new TypeReference(defaultDoctypeId, key, defaultRefPropertyName);
    }
    return null;

  }

  public void filter(final PlaceholderReplacer replacer) {
    if (macro != null) {
      macro.filter(replacer);
    } else if (StringUtils.isNotBlank(xml)) {
      xml = replacer.replace(xml);
    }
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    if (StringUtils.isBlank(xml)) {
      if (null != macro) {
        return macro.toString("                  ");
      }

      if (null != placeholder) {
        return placeholder.toString();
      }

      if (StringUtils.isNotBlank(key)) {
        return "<at:i18n at:key=\"" + key + "\"/>";
      }
    }
    return xml;
  }
}
