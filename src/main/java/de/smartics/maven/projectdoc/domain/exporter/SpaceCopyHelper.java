/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import static de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple.a;
import static de.smartics.maven.projectdoc.io.StringFunction.escapePropertyValue;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.addon.Addon;
import de.smartics.maven.projectdoc.domain.bo.addon.Category;
import de.smartics.maven.projectdoc.domain.bo.space.ImportDoctypes;
import de.smartics.maven.projectdoc.domain.bo.space.RemoteDoctypeRef;
import de.smartics.maven.projectdoc.domain.bo.space.RemoteDoctypeSet;
import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.bo.space.Space.Bundle;
import de.smartics.maven.projectdoc.domain.bo.space.SpaceProperty;
import de.smartics.maven.projectdoc.domain.bo.space.SpacePropertyL10n;
import de.smartics.maven.projectdoc.domain.bo.tool.ToolRef;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.DoctypeTemplateLocations;
import de.smartics.maven.projectdoc.domain.project.DoctypeTemplateLocations.Location;
import de.smartics.maven.projectdoc.domain.project.Mode;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple;
import de.smartics.maven.projectdoc.domain.project.SpaceTemplateLocations;
import de.smartics.maven.projectdoc.io.PropertiesHelper;
import de.smartics.maven.projectdoc.io.StringFunction;

import org.apache.commons.lang.StringUtils;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Properties;

class SpaceCopyHelper extends CopyHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Space space;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  SpaceCopyHelper(final AddonRepository repository, final Space space,
      final Mode mode) throws IOException {
    super(repository, space.getId(), mode,
        SpaceExportLocations.createForExport(repository.getConfig(), space),
        new SpaceTemplateLocations(
            new File(repository.getConfig().getSpacesFolder(),
                space.getId() + "-space")));
    this.space = space;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  void provideTemplate() {
    final String spaceId = space.getId();

    final PlaceholderReplacer replacer =
        new PlaceholderReplacer(a("${shortId}", shortId),
            a("${package}", packageId));
    if (addonDocumentation != null) {
      replacer.add("${addon-documentation}", addonDocumentation.getLocator());
    }

    try {
      final String content = importLocations.getResourceAsString(spaceId,
          SpaceTemplateLocations.Location.TEMPLATE, replacer);

      final File file = exportLocations
          .getResource(SpaceExportLocations.Location.SPACE_TEMPLATE).getFile();
      file.getParentFile().mkdirs();
      FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", content);
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot copy space Template " + spaceId + ": " + e.getMessage(), e);
    }
  }

//  void providedPages() {
//    final String spaceId = space.getId();
//
//    final PlaceholderReplacer replacer =
//        new PlaceholderReplacer(a("${shortId}", shortId),
//            a("${package}", packageId));
//
//    try {
//      final String content = importLocations.getResourceAsString(spaceId,
//          SpaceTemplateLocations.Location.CONTENT_MANAGEMENT_DASHBOARD_TEMPLATE,
//          replacer);
//
//      final File file = exportLocations.getResource(
//          SpaceExportLocations.Location.CONTENT_MANAGEMENT_DASHBOARD_TEMPLATE)
//          .getFile();
//      file.getParentFile().mkdirs();
//      FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", content);
//    } catch (final IOException e) {
//      throw new SystemException(
//          "Cannot copy Content Management Dashboard Template " + spaceId +
//          ": " + e.getMessage(), e);
//    }
//  }

  void tools() {
    for (final ToolRef tool : space.getTools()) {
      final String toolId = tool.getId();
      try {
        final ToolCopyHelper helper =
            new ToolCopyHelper(repository, space, toolId, mode);
        helper.provide();
      } catch (final IOException e) {
        throw new SystemException(
            "Cannot copy tools for " + toolId + ": " + e.getMessage(), e);
      }
    }
  }

  void wizardSoy() {
    final String spaceId = space.getId();
    final String shortIdSoy = soyify(shortId);
    final String spaceIdSoy = soyify(spaceId);
    final PlaceholderReplacer replacer =
        new PlaceholderReplacer(a("${shortIdSoy}", shortIdSoy),
            a("${spaceIdSoy}", spaceIdSoy));
    try {
      final String content = importLocations.getResourceAsString(spaceId,
          SpaceTemplateLocations.Location.CREATE_SPACE, replacer);

      final File file =
          exportLocations.getResource(SpaceExportLocations.Location.WIZARD_SOY)
              .getFile();
      file.getParentFile().mkdirs();
      FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", content);
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot copy space wizard soy " + spaceId + ": " + e.getMessage(), e);
    }
  }

  public static String soyify(final String spaceId) {
    return StringUtils.remove(spaceId, '-');
  }

  void properties() {
    for (final Bundle bundle : space.getBundles()) {
      final String locale = bundle.getLocale();

      final PlaceholderReplacer replacer =
          new PlaceholderReplacer(a("${shortId}", shortId),
              a("${spaceId}", space.getId()),
              a("${space.name}", escapePropertyValue(bundle.getName())),
              a("${space.description}",
                  escapePropertyValue(bundle.getDescription())),
              a("${space.about}", escapePropertyValue(bundle.getAbout())),
              a("${package}", packageId));
      final String[] locationParams =
          locale != null ? new String[] {"locale", locale} : new String[0];
      append(SpaceExportLocations.Location.TEXT_RESOURCE,
          SpaceTemplateLocations.Location.TEXT_RESOURCE, "ISO-8859-1", locale,
          replacer, locationParams);
    }
  }

  void registerWizard() {
    final String spaceId = space.getId();
    final PlaceholderReplacer replacer =
        new PlaceholderReplacer(a("${shortId}", shortId),
            a("${spaceId}", spaceId), a("${groupId}", groupId),
            a("${artifactId}", artifactId), a("${package}", packageId));
    final ExportMarker marker =
        new ExportMarker("/* insert-mark " + spaceId + " */\n",
            "\n/* /insert-mark " + spaceId + " */");

    replace(SpaceExportLocations.Location.WIZARD_JS,
        SpaceTemplateLocations.Location.REGISTER_SPACE_WIZARD, "UTF-8",
        replacer, marker);
  }

  void addSpace() {
    final String spaceId = space.getId();
    final String spaceCategory = space.getCategory();
    final String shortIdSoy = soyify(shortId);
    final String spaceIdSoy = soyify(spaceId);
    final PlaceholderReplacer replacer =
        new PlaceholderReplacer(a("${shortId}", shortId),
            a("${spaceId}", spaceId), a("${spaceCategory}", spaceCategory),
            a("${packageInPathFormat}", packageInPathFormat),
            a("${package}", packageId), a("${shortIdSoy}", shortIdSoy),
            a("${spaceIdSoy}", spaceIdSoy));
    final ExportMarker marker =
        new ExportMarker("<!-- insert-mark space " + spaceId + " -->\n",
            "\n<!-- /insert-mark space " + spaceId + " -->");

    replace(SpaceExportLocations.Location.ATLASSIAN_PLUGIN,
        SpaceTemplateLocations.Location.SPACE, "UTF-8", replacer, marker);

    replace(SpaceExportLocations.Location.ATLASSIAN_PLUGIN,
        SpaceTemplateLocations.Location.RESOURCE, "UTF-8", replacer, marker);
  }

  void homepages() {
    final File file = exportLocations
        .getResource(SpaceExportLocations.Location.SPACE_TEMPLATE).getFile();
    if (!file.canRead()) {
      return;
    }
    spaceProperties(file);
    homepages(file, true);
  }

//  void contentManagementDashboard() {
//    final File file = exportLocations.getResource(
//        SpaceExportLocations.Location.CONTENT_MANAGEMENT_DASHBOARD_TEMPLATE)
//        .getFile();
//    if (!file.canRead()) {
//      return;
//    }
//    homepages(file, false);
//  }

  private void homepages(final File file, final boolean createCategories) {
    try {
      final String homepageLinks =
          fragment(SpaceTemplateLocations.Location.HOMEPAGE_LINKS);

      final PropertiesManager propertiesManager;
      if (createCategories) {
        final List<String> locales = repository.getSupportedLocales();
        propertiesManager = new PropertiesManager(locales);
      } else {
        propertiesManager = null;
      }

      final StringBuilder linkBuffer = new StringBuilder(8192);
      final Addon addon = repository.getAddon();
      for (final Category category : addon.getCategories()) {
        final String id = category.getId();
        final String key = repository.createCategoryId(id, space.getId());

        if (createCategories) {
          propertiesManager.setProperty(key, category);
        }

        final String fragment = StringUtils.replaceEach(homepageLinks,
            new String[] {"${shortId}", "${category}", "${category.title.key}",
                "${package}"}, new String[] {shortId, id, key, packageId});
        linkBuffer.append(fragment).append('\n');
      }

      if (createCategories) {
        for (final Entry<String, Properties> entry : propertiesManager.getMap()
            .entrySet()) {
          final String locale = entry.getKey();
          final String data = StringFunction.toData(entry.getValue());
          append(SpaceExportLocations.Location.TEXT_RESOURCE, data,
              "ISO-8859-1", locale);
        }
      }

      final String marker =
          SpaceTemplateLocations.Location.HOMEPAGE_LINKS.getMarker();
      writePage(file, marker, linkBuffer);
    } catch (final IOException e) {
      throw new SystemException("Cannot write space homepage to " +
                                file.getParentFile().getAbsolutePath() + ": " +
                                e.getMessage(), e);
    }
  }

  private void spaceProperties(final File file) throws SystemException {
    final StringBuilder buffer = new StringBuilder(512);
    final String doctypeId = space.getId();

    final PropertiesHelper propertiesHelper = new PropertiesHelper();
    for (final SpaceProperty property : space.getProperties()) {
      buffer.append("                <tr>\n                  <th " +
                    "class=\"confluenceTh\">");
      final String name = property.getName();
      if (name != null) {
        buffer.append(name);
      }
      buffer.append("</th>\n                  <td class=\"confluenceTd\">");
      final String value = property.getValue(packageId, doctypeId);
      if (value != null) {
        buffer.append(value);
      }
      buffer.append("</td>\n                  <td class=\"confluenceTd\">");
      final String controls = property.getControls();
      if (controls != null) {
        buffer.append(controls);
      }

      buffer.append("</td>\n                </tr>\n");
    }
    try {
      if (buffer.length() > 0) {
        writePage(file, "<!-- projectdoc INSERT Space Properties -->", buffer);
      }
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot write properties to space homepage at " +
          file.getParentFile().getAbsolutePath() + ": " + e.getMessage(), e);
    }

    for (final String locale : repository.getAddon().getSupportedLocales()) {
      final Properties l10nBuffer = new Properties();
      for (final SpaceProperty property : space.getProperties()) {
        for (final SpacePropertyL10n l10n : property.getResourceBundle()) {
          if (!Objects.equals(locale, l10n.getLocale())) {
            continue;
          }

          l10nBuffer.put(property.getKey(), l10n.getName());

          final String description = l10n.getDescription();
          if (StringUtils.isNotBlank(description) ||
              property.hasPlaceholderValue()) {
            l10nBuffer
                .put(property.getKey() + ".placeholder", l10n.getDescription());
          }
        }
      }
      final File propertiesFile = exportLocations
          .getResource(SpaceExportLocations.Location.TEXT_RESOURCE, locale)
          .getFile();
      propertiesHelper.mergeAndWriteProperties(l10nBuffer, propertiesFile,
          "# space properties");
    }
  }

  public void importedDoctypes() {
    final ImportDoctypes importDoctypes = space.getImportDoctypes();

    final List<RemoteDoctypeSet> sets = importDoctypes.getDoctypeSets();
    if (sets.isEmpty()) {
      return;
    }

    final File file = exportLocations
        .getResource(DoctypeExportLocations.Location.ATLASSIAN_PLUGIN)
        .getFile();
    if (!file.exists()) {
      return;
    }
    try {
      String xml = FileUtils.fileRead(file, "UTF-8");

      final String blueprint =
          fragment(SpaceTemplateLocations.Location.ATLASSIAN_PLUGIN_BLUEPRINT);
      final String templateRef = fragment(
          SpaceTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_REF);

      final Tuple spaceIdTupel = Tuple.a("${spaceId}", this.space.getId());

      for (final RemoteDoctypeSet set : sets) {
        repository.getConfig().info("Importing homepages from " + set.getId());

        final Tuple groupId = Tuple.a("${project.groupId}", set.getGroupId());
        final Tuple artifactId =
            Tuple.a("${project.artifactId}", set.getArtifactId());
        for (final RemoteDoctypeRef doctypeRef : set.getDoctypeRefs()) {
          final String doctypeId = doctypeRef.getId();
          // standard-blueprint-ref
          if (doctypeRef.isPromoted()) {
            final PlaceholderReplacer replacer1 =
                new PlaceholderReplacer(Tuple.a("${doctypeId}", doctypeId),
                    spaceIdTupel, groupId, artifactId);
            xml = replaceXml(xml,
                DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_BLUEPRINT_STANDARD,
                blueprint, replacer1, doctypeId);
          }

          if (doctypeRef.hasHomepage()) {
            // standard-template-home-ref
            final PlaceholderReplacer replacer2 =
                new PlaceholderReplacer(Tuple.a("${doctypeId}", doctypeId),
                    spaceIdTupel);
            xml = replaceXml(xml, RemoteDoctypeRef.TYPE_LOCATION
                                      .equals(doctypeRef.resolveLocation()) ?
                                  DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_REF_STANDARD_TYPE :
                                  DoctypeTemplateLocations.Location.ATLASSIAN_PLUGIN_TEMPLATE_REF_STANDARD,
                templateRef, replacer2, doctypeId);
          }
        }
      }

      FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", xml);
    } catch (final IOException e) {
      throw new SystemException("Cannot write atlassian-plugin.xml to " +
                                file.getParentFile().getAbsolutePath() + ": " +
                                e.getMessage(), e);
    }
  }

  private String replaceXml(final String content, final Location location,
      final String fragment, final PlaceholderReplacer replacer,
      final String id) {
    return replace(content, location, replacer.replace(fragment), "\n\n",
        new de.smartics.maven.projectdoc.domain.exporter.DoctypeCopyHelper.ExportMarker(
            "<!-- insert-mark " + space.getId() + '-' + id + '-' +
            location.name() + "# -->\n",
            "\n<!-- /insert-mark " + space.getId() + "-" + location.name() +
            "# -->"), replacer);
  }

  private String replace(final String content, final Location location,
      final String fragment, final String separator,
      final de.smartics.maven.projectdoc.domain.exporter.DoctypeCopyHelper.ExportMarker exportMarker,
      final PlaceholderReplacer replacer) {
    final String marker = replacer.replace(location.getMarker());
    final String replaced = StringUtils
        .replaceEach(content, new String[] {marker},
            new String[] {marker + separator + exportMarker.mark(fragment)});
    return replaced;
  }

  // --- object basics --------------------------------------------------------

}
