/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import static de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple.a;

import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.Wizard;
import de.smartics.maven.projectdoc.io.StringFunction;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.plexus.util.FileUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class WizardTemplateLocations implements TemplateLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Doctype doctype;

  // private final ProjectConfiguration projectConfiguration;

  private final PlaceholderReplacer replacer;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public WizardTemplateLocations(final Doctype doctype,
      final ProjectConfiguration projectConfiguration) {
    this.doctype = doctype;
    // this.projectConfiguration = projectConfiguration;
    this.replacer = createReplacer(doctype, projectConfiguration);
  }

  // ****************************** Inner Classes *****************************

  private static PlaceholderReplacer createReplacer(final Doctype doctype,
      final ProjectConfiguration projectConfiguration) {
    final String shortId = projectConfiguration.getShortId();
    final String doctypeId = doctype.getId();
    final String normalizedDoctypeId =
        StringFunction.normalizeUnicode(doctypeId);
    final String shortName = StringUtils.capitalize(shortId);
    final String doctypeName = StringFunction.toName(doctypeId);
    final String normalizedDoctypeName =
        StringFunction.normalizeUnicode(doctypeName);
    final String groupId = projectConfiguration.getGroupId();
    final String artifactId = projectConfiguration.getArtifactId();

    final PlaceholderReplacer replacer = new PlaceholderReplacer(
        a("${shortId}", shortId), a("${doctypeId}", doctypeId),
        a("${normalizedDoctypeId}", normalizedDoctypeId),
        a("${shortName}", shortName), a("${doctypeName}", doctypeName),
        a("${normalizedDoctypeName}", normalizedDoctypeName),
        a("${project.groupId}", groupId),
        a("${project.artifactId}", artifactId));
    return replacer;
  }

  public enum Location implements ImportLocation {
    WIZARD_SOY("wizards/template/${id}.soy",
        "<!-- === projectdoc INSERT wizard-params === -->"),

    WIZARD_SOY_FORM_PARAM("wizards/template/field/${id}.xml",
        "<!-- === projectdoc INSERT wizard-form-parameters === -->"),

    WIZARD_SOY_FIELD("wizards/template/field/${id}.xml",
        "<!-- === projectdoc INSERT wizard-fields === -->"),

    WIZARD_JS("wizards/code/${id}.js");

    private final String pathPattern;

    private final String marker;

    Location(final String pathPattern) {
      this(pathPattern, null);
    }

    Location(final String pathPattern, final String marker) {
      this.pathPattern = pathPattern;
      this.marker = marker;
    }

    @Override
    public String getPathPattern() {
      return pathPattern;
    }

    @Override
    public String getMarker() {
      return marker;
    }

    @Override
    public boolean isTextResource() {
      return false;
    }

    @Override
    public String truncatePath(final String path) {
      return FilenameUtils.getName(path);
    }

    private static String calcId(final ImportLocation location,
        final Wizard wizard, final PlaceholderReplacer replacer)
        throws IllegalArgumentException {
      switch ((Location) location) {
        case WIZARD_SOY:
          return wizard.getTemplate();
        case WIZARD_SOY_FIELD:
          final String fieldId = replacer.replace("${id}");
          return fieldId;
        case WIZARD_JS:
          return wizard.getCode();
        default:
          throw new IllegalArgumentException("Unknown location: " + location);
      }
    }

    @Override
    public String toString(final String... args) {
      return StringUtils.replace(pathPattern, "${id}", args[0]);
    }
  }

  private static final class Resource {
    private final InputStream input;
    private final String encoding;

    private Resource(final ImportLocation location, final InputStream input) {
      this.input = input;
      this.encoding =
          "properties".equals(FileUtils.extension(location.getPathPattern()))
              ? "ISO-8859-1"
              : "UTF-8";
    }

    private void close() {
      IOUtils.closeQuietly(input);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public String getResourceAsString(final Context context) throws IOException {
    final ImportLocation location = context.getLocation();
    final String templateSetId =
        Location.calcId(location, doctype.getWizard(), context.getReplacer());
    return getResourceAsString(templateSetId, context);
  }

  @Override
  public String getResourceAsString(final String templateSetId,
      final Context context) throws IOException {
    final ImportLocation location = context.getLocation();
    final Resource resource = getResource(location, templateSetId);
    if (resource != null) {
      try {
        final String template =
            IOUtils.toString(resource.input, resource.encoding);
        final String replaced = replacer.replace(context.replace(template));
        return replaced;
      } finally {
        resource.close();
      }
    } else {
      throw new IOException(
          "Cannot find template in context '" + context + "'.");
    }
  }

  private Resource getResource(final ImportLocation location, final String id) {
    final String path = location.toString(id);
    final InputStream in =
        this.getClass().getClassLoader().getResourceAsStream(path);
    return in != null ? new Resource(location, new BufferedInputStream(in))
        : null;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
