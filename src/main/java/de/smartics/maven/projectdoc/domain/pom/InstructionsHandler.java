/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.pom;

import org.apache.commons.lang3.StringUtils;

/**
 *
 */
public class InstructionsHandler {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final StringBuilder importPackage = new StringBuilder(128);

  private final StringBuilder dynamicImportPackage = new StringBuilder(128);

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public boolean hasInstructions() {
    return importPackage.length() > 0 || dynamicImportPackage.length() > 0;
  }

  // --- business -------------------------------------------------------------

  public void importInstruction(final String instruction) {
    importInstruction(importPackage, instruction);
  }

  public void dynamicImportInstruction(final String instruction) {
    importInstruction(dynamicImportPackage, instruction);
  }

  private void importInstruction(final StringBuilder buffer,
      final String instruction) {
    if (StringUtils.isNotBlank(instruction)) {
      if (buffer.length() > 0) {
        buffer.append(',');
      }
      buffer.append(instruction);
    }
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    if (importPackage.length() == 0) {
      importPackage.append(
          "de.smartics.projectdoc.atlassian.confluence;version=\"0.0.0\",*;resolution:=optional");
    }
    final StringBuilder buffer = new StringBuilder(
        importPackage.length() + dynamicImportPackage.length());

    buffer.append("\n            <Import-Package>").append(importPackage)
        .append("</Import-Package>");
    if (dynamicImportPackage.length() > 0) {
      buffer.append("\n            <DynamicImport-Package>")
          .append(dynamicImportPackage).append("</DynamicImport-Package>");
    }

    return buffer.toString();
  }
}
