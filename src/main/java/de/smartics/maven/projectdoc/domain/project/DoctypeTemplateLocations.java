/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.io.StringFunction;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

public class DoctypeTemplateLocations implements TemplateLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private final static String FRAGMENTS = "fragments";

  private final static String ATLASSIAN_PLUGIN =
      FRAGMENTS + "/atlassian-plugin";

  // --- members --------------------------------------------------------------

  private final Doctype doctype;

  private final String shortId;
  private final String projectName;
  private final String groupId;
  private final String artifactId;
  private final String thePackage;
  private final String thePackageAsPath;
  private final String contextProvider;
  private final String createResult;

  private final File doctypeResourcesFolder;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DoctypeTemplateLocations(final Doctype doctype,
      final ProjectConfiguration config) {
    this(doctype, doctype.getId(), doctype.getSecondaryId(), config);
  }

  private DoctypeTemplateLocations(final Doctype doctype,
      final String doctypeId, final String doctypeTypeId,
      final ProjectConfiguration config) {
    this.doctype = doctype;

    this.shortId = config.getShortId();
    this.projectName = config.getProjectName();
    this.groupId = config.getGroupId();
    this.artifactId = config.getArtifactId();
    this.thePackage = config.getPackage();
    this.thePackageAsPath = config.getPackagePath();
    this.contextProvider = doctype.getContextProvider(thePackage);
    this.createResult = doctype.getCreateResult();

    final File doctypesFolder = config.getDoctypesFolder();
    this.doctypeResourcesFolder = new File(doctypesFolder, doctype.getId());
  }


  // ****************************** Inner Classes *****************************

  // FIXME:
  public static final String ATLASSIAN_PLUGIN_TEMPLATE_REF_TYPE_MARKER =
      "<!-- === projectdoc INSERT template-type-home-ref ${spaceId} === -->";

  public static enum Location implements ImportLocation {
    DOCTYPE("doctype.xml"),

    TEMPLATE("template.xml"),

    ATLASSIAN_PLUGIN_BLUEPRINT(ATLASSIAN_PLUGIN + "-blueprint.xml",
        "<!-- === projectdoc INSERT blueprint-ref ${spaceId} === -->"),

    ATLASSIAN_PLUGIN_BLUEPRINT_STANDARD(ATLASSIAN_PLUGIN + "-blueprint.xml",
        "<!-- === projectdoc INSERT standard-blueprint-ref ${spaceId} === -->"),

    ATLASSIAN_PLUGIN_RESOURCE(ATLASSIAN_PLUGIN + "-resource-js.xml",
        "<!-- === projectdoc INSERT resource-js.xml === -->"),

    ATLASSIAN_PLUGIN_WEB_RESOURCE(ATLASSIAN_PLUGIN + "-web-resource-js.xml",
        "<!-- === projectdoc INSERT web-resource-js.xml === -->"),

    ATLASSIAN_PLUGIN_TEMPLATE_REF(ATLASSIAN_PLUGIN + "-template-ref.xml",
        "<!-- === projectdoc INSERT template-home-ref ${spaceId} === -->"),

    ATLASSIAN_PLUGIN_TEMPLATE_REF_STANDARD(
        ATLASSIAN_PLUGIN + "-template-ref.xml",
        "<!-- === projectdoc INSERT standard-template-home-ref ${spaceId} === -->"),

    ATLASSIAN_PLUGIN_TEMPLATE_REF_TYPE(ATLASSIAN_PLUGIN + "-template-ref.xml",
        ATLASSIAN_PLUGIN_TEMPLATE_REF_TYPE_MARKER),

    ATLASSIAN_PLUGIN_TEMPLATE_REF_STANDARD_TYPE(
        ATLASSIAN_PLUGIN + "-template-ref.xml",
        "<!-- === projectdoc INSERT standard-template-type-home-ref ${spaceId} === -->"),

    ATLASSIAN_PLUGIN_TEMPLATE_HOME(ATLASSIAN_PLUGIN + "-template-home.xml",
        "<!-- === projectdoc INSERT template === -->"),

    ATLASSIAN_PLUGIN_TEMPLATE(ATLASSIAN_PLUGIN + "-template.xml",
        "<!-- === projectdoc INSERT template === -->"),

    TEXT_RESOURCE(FRAGMENTS + "/l10n${locale}.properties"),

    HOME("home.xml", "<!-- Related Doctypes -->"),

    INDEX("index.xml", "<!-- Related Doctypes -->"),

    INDEX_LINKS_PANEL(FRAGMENTS + "/index-links-panel.xml",
        "<!-- Index Panel -->"),

    SUBSPACE_OPTION_SOY("subspace-option.soy",
        "<!-- === projectdoc INSERT option === -->"),

    PARTITION_CONTEXT_PROVIDER_JAVA("partition-context-provider.java",
        "// === projectdoc INSERT type-home-template ==="),

    HOMEPAGE_LINK(FRAGMENTS + "/homepage-link.xml",
        "<!-- projectdoc INSERT Homepage Link ${category} -->");

    private final String pathPattern;

    private final String marker;

    Location(final String pathPattern) {
      this(pathPattern, null);
    }

    Location(final String pathPattern, final String marker) {
      this.pathPattern = pathPattern;
      this.marker = marker;
    }

    @Override
    public String getPathPattern() {
      return pathPattern;
    }

    @Override
    public String getMarker() {
      return marker;
    }

    @Override
    public boolean isTextResource() {
      return this == TEXT_RESOURCE;
    }

    @Override
    public String toString(final String... args) {
      if (pathPattern.contains("${")) {
        if (args.length > 0 && args[0] != null) {
          return StringUtils.replace(pathPattern, "${locale}", '_' + args[0]);
        } else {
          return StringUtils.replace(pathPattern, "${locale}", "");
        }
      }

      return pathPattern;
    }

    @Override
    public String truncatePath(final String path) {
      return FilenameUtils.getName(path);
    }

    @Override
    public String toString() {
      return pathPattern;
    }
  }

  private static final class Resource {
    private final String path;
    private final InputStream input;
    private final String encoding;

    private Resource(final String path, final InputStream input) {
      this.path = path;
      this.input = input;
      this.encoding =
          "properties".equals(FilenameUtils.getExtension(path)) ? "ISO-8859-1"
              : "UTF-8";
    }

    private void close() {
      IOUtils.closeQuietly(input);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public String getResourceAsString(final Context context) throws IOException {
    final String templateId = doctype.getBaseTemplateName();
    final String xml = getResourceAsString(templateId, context);
    return xml;
  }

  @Override
  public String getResourceAsString(final String templateSetId,
      final Context context) throws IOException {
    final ImportLocation location = context.getLocation();
    final String locale = context.getLocale();
    final PlaceholderReplacer replacer = context.getReplacer();

    final Resource resource =
        getResource(templateSetId, location, context.getLocale());
    if (resource.input != null) {
      try {
        final String template =
            IOUtils.toString(resource.input, resource.encoding);
        final boolean unicodeEncoded = location == Location.TEXT_RESOURCE;
        final String replaced =
            replacePlaceholders(template, locale, replacer, unicodeEncoded);
        return replaced;
      } finally {
        resource.close();
      }
    } else {
      if (location != Location.TEXT_RESOURCE) {
        throw new IOException("Cannot find template for id '" + templateSetId
            + "' in context '" + context + "': " + resource.path);
      } else {
        if (locale != null) {
          return getResourceAsString(templateSetId,
              Context.defaultLocale(context));
        } else {
          return StringUtils.EMPTY;
        }
      }
    }
  }

  private Resource getResource(final String templateSetId,
      final ImportLocation location, final String locale) {
    final String path = location.toString(locale);
    final String streamPath = "doctypes/" + templateSetId + "-doctype/" + path;

    final String resourceFileName = FilenameUtils.getName(path);
    final File providedFile =
        new File(doctypeResourcesFolder, resourceFileName);
    InputStream in;
    if (providedFile.canRead()) {
      try {
        in = FileUtils.openInputStream(providedFile);
      } catch (final IOException e) {
        in = read(streamPath, path);
      }
    } else {
      in = read(streamPath, path);
    }

    return in != null ? new Resource(streamPath, new BufferedInputStream(in))
        : new Resource(streamPath, null);
  }

  private InputStream read(final String streamPath, final String path) {
    final ClassLoader loader = this.getClass().getClassLoader();
    InputStream in = loader.getResourceAsStream(streamPath);
    if (in == null) {
      final String defaultStreamPath = "doctypes/_DEFAULT_/" + path;
      in = loader.getResourceAsStream(defaultStreamPath);
    }
    return in;
  }

  // FIXME: Use replacer
  private String replacePlaceholders(final String template, final String locale,
      final PlaceholderReplacer replacer, final boolean unicodeEncoded) {
    final String doctypeId = doctype.getId();
    final String doctypeTypeId = doctype.getSecondaryId();

    final String shortName = StringUtils.capitalize(shortId);
    final String doctypeName = StringFunction.toName(doctypeId);
    final String doctypeIdSoyFix = StringEscapeUtils.escapeHtml4(doctypeId);
    final String normalizedDoctypeName =
        StringFunction.normalizeUnicode(doctypeName);
    final String normalizedDoctypeId =
        StringFunction.normalizeUnicode(doctypeId);
    final String doctypeNameLabel = doctype.getName(locale);
    final String doctypeNameLabelPlural = doctype.getNamePlural(locale);
    final String doctypeNameLabelPluralLower =
        StringFunction.toLower(doctypeNameLabelPlural);
    final String doctypeNameUrlPart = encodeUrl(doctypeNameLabel);
    final String projectNameUrlPart = encodeUrl(projectName);

    final String doctypeTypeNameLabel = doctype.getTypeName(locale);
    final String doctypeTypeNameLabelPlural = doctype.getTypeNamePlural(locale);
    final String doctypeTypeNameLabelPluralLower =
        doctypeTypeNameLabelPlural != null
            ? doctypeTypeNameLabelPlural.toLowerCase(Locale.ENGLISH)
            : null;
    final String doctypeTypeNameLabelUrlPart = encodeUrl(doctypeTypeNameLabel);

    final String replaced =
        replacer != null ? replacer.replace(template) : template;
    final String value = StringUtils.replaceEach(replaced,
        new String[] {"${shortId}", "${shortName}", "${doctypeId}",
            "${doctypeIdSoyFix}", "${normalizedDoctypeId}", "${doctypeName}",
            "${normalizedDoctypeName}", "${doctypeNameLabel}",
            "${doctypeNameLabel.plural}", "${doctypeNameLabel.plural.lower}",
            "${doctypeNameLabel.urlPart}", "${doctypeTypeId}",
            "${doctypeTypeNameLabel}", "${doctypeTypeNameLabel.plural}",
            "${doctypeTypeNameLabel.plural.lower}",
            "${doctypeTypeNameLabel.urlPart}", "${project.name}",
            "${project.name.url}", "${project.groupId}",
            "${project.artifactId}", "${package}", "${packagePath}",
            "${contextProvider}", "${blueprint.createResult}"},
        new String[] {shortId, shortName, doctypeId, doctypeIdSoyFix,
            normalizedDoctypeId, doctypeName, normalizedDoctypeName,
            doctypeNameLabel, doctypeNameLabelPlural,
            doctypeNameLabelPluralLower, doctypeNameUrlPart, doctypeTypeId,
            doctypeTypeNameLabel, doctypeTypeNameLabelPlural,
            doctypeTypeNameLabelPluralLower, doctypeTypeNameLabelUrlPart,
            projectName, projectNameUrlPart, groupId, artifactId, thePackage,
            thePackageAsPath, contextProvider, createResult});
    return value;
  }

  private static String encodeUrl(final String label) {
    if (label != null) {
      try {
        return URLEncoder.encode(label, "UTF-8");
      } catch (final UnsupportedEncodingException e) {
        // return the unencoded label
      }
    }
    return label;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
