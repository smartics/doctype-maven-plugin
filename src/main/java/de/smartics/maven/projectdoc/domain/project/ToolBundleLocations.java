/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import static de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple.a;

import java.io.File;

public class ToolBundleLocations extends AbstractTemplateLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ToolBundleLocations(final File toolBundleFolder) {
    super(toolBundleFolder);
  }

  // ****************************** Inner Classes *****************************

  public static enum Location implements ImportLocation {
    /**
     * Reference to pages to copy.
     */
    PAGES("${toolId}/tools/pages"),
    /**
     * Reference to localization bundles.
     */
    TEXT_RESOURCE("${toolId}/text-resources${locale}.properties"),

    /**
     * Reference to space information for the <tt>atlassian-plugin.xml</tt>.
     */
    SPACE("${toolId}/content-template-space.xml",
        "<!-- === projectdoc INSERT standard-template-home-ref ${spaceId} === -->"),

    /**
     * Reference to tool information for the <tt>atlassian-plugin.xml</tt>.
     */
    TOOL("${toolId}/content-template-tool.xml",
        "<!-- === projectdoc INSERT content-template-tool === -->"),

    /**
     * Reference to space information for the <tt>atlassian-plugin.xml</tt>.
     */
    SPECIAL("${toolId}/content-template-blueprint.xml",
        "<!-- === projectdoc INSERT specialpage === -->");

    private final String pathPattern;

    private final String marker;

    Location(final String pathPattern) {
      this(pathPattern, null);
    }

    Location(final String pathPattern, final String marker) {
      this.pathPattern = pathPattern;
      this.marker = marker;
    }

    @Override
    public String getMarker() {
      return marker;
    }

    @Override
    public String getPathPattern() {
      return pathPattern;
    }

    @Override
    public boolean isTextResource() {
      return this == TEXT_RESOURCE;
    }

    @Override
    public String truncatePath(final String path) {
      return path;
    }

    @Override
    public String toString(final String... args) {
      final String locale = args[1] != null ? '_' + args[1] : "";
      final PlaceholderReplacer replacer = new PlaceholderReplacer(
          a("${toolId}", args[0]), a("${locale}", locale));
      return replacer.replace(pathPattern);
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
