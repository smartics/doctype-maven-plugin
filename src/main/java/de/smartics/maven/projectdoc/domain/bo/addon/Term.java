/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.addon;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * Represents a term with metadata.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Term {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The singular form of the text resource.
   *
   * @since 1.0
   */
  @XmlValue
  private String singular;

  /**
   * The plural form of the text resource. May be <code>null</code>.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String plural;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the singluar form of the text resource.
   *
   * @return the singluar form of the text resource.
   */
  public String getSingular() {
    return StringUtils.trim(singular);
  }

  /**
   * Returns the plural form of the text resource.
   *
   * @return the plural form of the text resource.
   */
  public String getPlural() {
    return StringUtils.trim(plural);
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return singular + (StringUtils.isNotBlank(plural) ? "/" + plural : "");
  }
}
