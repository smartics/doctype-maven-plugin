/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;

/**
 * Interface to sections of a document.
 */
public interface Section {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- get&set --------------------------------------------------------------

  String getKey();

  /**
   * Returns the value for resourceBundle.
   * <p>
   * Provides localized information.
   * </p>
   *
   * @return the value for resourceBundle.
   */
  DoctypeResourceBundle getResourceBundle();

  /**
   * Returns the name of the XML container to insert this section to.
   *
   * @return the name of the XML container to insert this section to.
   */
  String getTarget();

  boolean hasXml();

  // --- business -------------------------------------------------------------

  /**
   * Returns the content of the section title.
   * <p>
   * If there is no content, a default section with a placeholder element will
   * be returned.
   * </p>
   *
   * @return the content of the section title.
   */
  String getXml(String packageId, String doctypeId);

  void filter(PlaceholderReplacer replacer);

  /**
   * Allows to make the defaults for a section materialized.
   */
  void initializeWithDefaults(String doctypeId);

  // --- object basics --------------------------------------------------------
}
