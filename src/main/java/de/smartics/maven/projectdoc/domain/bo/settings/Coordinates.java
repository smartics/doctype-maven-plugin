/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.settings;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Artifact coordinates within the project settings providing defaults.
 */
@XmlRootElement(name = "coordinates")
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressFBWarnings("UWF_UNWRITTEN_FIELD")
public class Coordinates {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The default group identifier.
   *
   * @since 1.0
   */
  private String groupId;

  /**
   * The default prefix for OBR/add-on artifacts. The shortId will be appended.
   *
   * @since 1.0
   */
  private String artifactIdPrefix;

  /**
   * The default prefix for model artifacts. The shortId will be appended.
   *
   * @since 1.0
   */
  private String modelArtifactIdPrefix;

  /**
   * The default version.
   *
   * @since 1.0
   */
  private String version;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the default group identifier.
   *
   * @return the default group identifier.
   */
  public String getGroupId() {
    return groupId;
  }

  /**
   * Returns the default prefix for artifacts. The shortId will be appended.
   *
   * @return the default prefix for artifacts.
   */
  public String getArtifactIdPrefix() {
    return artifactIdPrefix;
  }

  /**
   * Returns the default prefix for model artifacts. The shortId will be
   * appended.
   *
   * @return the default prefix for model artifacts.
   */
  public String getModelArtifactIdPrefix() {
    return modelArtifactIdPrefix;
  }

  /**
   * Returns the default version.
   *
   * @return the default version.
   */
  public String getVersion() {
    return version;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return StringUtils.defaultIfBlank(groupId, "") + ':'
        + StringUtils.defaultIfBlank(artifactIdPrefix, "") + +'/'
        + StringUtils.defaultIfBlank(modelArtifactIdPrefix, "") + ':'
        + StringUtils.defaultIfBlank(version, "");
  }
}
