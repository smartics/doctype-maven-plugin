/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.project.ExportLocation;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import org.apache.commons.lang.StringUtils;

public class AddonExportLocations extends ExportLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final String projectName;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private AddonExportLocations(final ProjectConfiguration projectConfig) {
    super(projectConfig);
    this.projectName = projectConfig.getProjectName();
  }

  // ****************************** Inner Classes *****************************

  public static enum Location implements ExportLocation {
    ATLASSIAN_PLUGIN(RESOURCES_ROOT, "atlassian-plugin.xml"), DOCMAP(
        RESOURCES_ROOT, "projectdoc/docmap.json"), CATEGORY_TREE(RESOURCES_ROOT,
            "projectdoc/category-trees");

    private final String folder;

    private final String file;

    private final String pathPattern;

    Location(final String folder, final String file) {
      this.folder = folder;
      this.file = file;
      this.pathPattern = folder + '/' + file;
    }

    @Override
    public String getFolder() {
      return folder;
    }

    @Override
    public String getFile() {
      return file;
    }

    @Override
    public String getPathPattern() {
      return pathPattern;
    }

    @Override
    public String toString(final String... args) {
      final String locale = args[2] != null ? args[2] : "";
      args[2] = locale;
      final String value = StringUtils.replaceEach(pathPattern,
          new String[] {"${shortId}", "${packagePath}", "${locale}"}, args);
      return value;
    }

    @Override
    public String toString() {
      return pathPattern;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  public static AddonExportLocations createForExport(
      final ProjectConfiguration projectConfig) {
    return new AddonExportLocations(projectConfig);
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected String[] createPathArgs(final String locale) {
    return new String[] {shortId, packagePath, locale};
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return shortId + ':' + projectName;
  }
}
