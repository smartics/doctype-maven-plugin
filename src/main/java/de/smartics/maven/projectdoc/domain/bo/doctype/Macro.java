/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;

import java.util.Collections;
import java.util.List;

import javax.annotation.CheckForNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 */
@XmlRootElement(name = "macro")
@XmlAccessorType(XmlAccessType.FIELD)
public class Macro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The name of the macro.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String name;

  @XmlElement(name = "param")
  private List<MacroParam> params;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getName() {
    return name;
  }

  public List<MacroParam> getParams() {
    if (params == null) {
      return Collections.emptyList();
    }
    return params;
  }

  @CheckForNull
  public MacroParam getParameter(final String name) {
    if (params != null) {
      for (final MacroParam param : params) {
        final String paramName = param.getName();
        if (name.equals(paramName)) {
          return param;
        }
      }
    }

    return null;
  }

  // --- business -------------------------------------------------------------

  public void filter(final PlaceholderReplacer replacer) {
    for (final MacroParam parameter : params) {
      parameter.filter(replacer);
    }
  }

  // --- object basics --------------------------------------------------------

  public String toString(final String prefix) {
    final StringBuilder buffer = new StringBuilder(512);

    buffer.append("<ac:structured-macro ac:name=\"").append(name)
        .append("\">\n").append(prefix);
    for (final MacroParam param : getParams()) {
      buffer.append("<ac:parameter ac:name=\"").append(param.getName())
          .append("\">").append(param.getValue()).append("</ac:parameter>")
          .append("\n").append(prefix);
    }
    buffer.append("</ac:structured-macro>");

    return buffer.toString();
  }

  @Override
  public String toString() {
    return toString("  ");
  }
}
