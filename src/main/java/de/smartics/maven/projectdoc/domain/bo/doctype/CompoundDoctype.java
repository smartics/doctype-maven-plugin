/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import java.util.ArrayList;
import java.util.List;

/**
 * A doctype with properties and sections inherited. Note that the current
 * implementation does not override, but simply add.
 */
public class CompoundDoctype implements Doctype {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Doctype primary;

  private final Doctype defaults;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public CompoundDoctype(final Doctype primary, final Doctype defaults) {
    this.primary = primary;
    this.defaults = defaults;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public Doctype getPrimary() {
    return primary;
  }

  public Doctype getDefaults() {
    return defaults;
  }

  // --- business -------------------------------------------------------------

  @Override
  public String getId() {
    return primary.getId();
  }

  @Override
  public String getCategory() {
    return primary.getCategory();
  }

  @Override
  public boolean hasHomepage() {
    return primary.hasHomepage();
  }

  @Override
  public String getName(final String locale) {
    return primary.getName(locale);
  }

  @Override
  public String getNamePlural(final String locale) {
    return primary.getNamePlural(locale);
  }

  @Override
  public boolean hasType() {
    return primary.hasType();
  }

  @Override
  public boolean isType() {
    return primary.isType();
  }

  @Override
  public boolean isStandardType() {
    return primary.isStandardType();
  }

  @Override
  public String getSecondaryId() {
    return primary.getSecondaryId();
  }

  @Override
  public void setSecondaryId(final String secondaryId) {
    primary.setSecondaryId(secondaryId);
  }

  @Override
  public String getTypeName(final String locale) {
    return primary.getTypeName(locale);
  }

  @Override
  public String getTypeNamePlural(final String locale) {
    return primary.getTypeNamePlural(locale);
  }

  @Override
  public String getBaseTemplateName() {
    return primary.getBaseTemplateName();
  }

  @Override
  public String getProvideType() {
    return primary.getProvideType();
  }

  @Override
  public DoctypeResourceBundle getResourceBundle() {
    return primary.getResourceBundle();
  }

  @Override
  public List<Property> getProperties() {
    final List<Property> primaryProperties = primary.getProperties();
    final List<Property> defaultProperties = defaults.getProperties();
    if (defaultProperties.isEmpty()) {
      return primaryProperties;
    }
    if (primaryProperties.isEmpty()) {
      return defaultProperties;
    }

    final int defaultPropertiesCount = defaultProperties.size();
    final List<Property> copy = new ArrayList<Property>(
        defaultPropertiesCount + primaryProperties.size());
    copy.addAll(defaultProperties);
    Property sortByProperty = copy.remove(copy.size() - 1);
    final String sortByPropertyName = sortByProperty.getName();
    for (final Property property : primaryProperties) {
      final String propertyName = property.getName();
      if (sortByPropertyName.equals(propertyName)) {
        sortByProperty = property;
        continue;
      }
      if (!replaceProperty(defaultPropertiesCount - 1, copy, property)) {
        copy.add(property);
      }
    }
    // copy.addAll(primaryProperties);
    copy.add(sortByProperty);
    return copy;
  }

  private boolean replaceProperty(final int max, final List<Property> copy,
      final Property property) {
    final String propertyName = property.getName();
    for (int i = 0; i < max; i++) {
      final Property defaultProperty = copy.get(i);
      final String defaultPropertyName = defaultProperty.getName();
      if (propertyName.equals(defaultPropertyName)) {
        copy.set(i, property);
        return true;
      }
    }
    return false;
  }

  @Override
  public List<Section> getSections() {
    final List<Section> defaultSections = defaults.getSections();
    if (defaultSections.isEmpty()) {
      return primary.getSections();
    }
    if (primary.getSections().isEmpty()) {
      return defaultSections;
    }

    final SectionMapper mapper = new SectionMapper(primary, defaults);
    return mapper.getSections();
  }

  @Override
  public List<Metadata> getDoctypeMetadata() {
    final List<Metadata> primaryMetadata = primary.getDoctypeMetadata();
    final List<Metadata> defaultsMetadata = defaults.getDoctypeMetadata();

    final ArrayList<Metadata> list =
        new ArrayList<>(primaryMetadata.size() + defaultsMetadata.size());

    list.addAll(primary.getDoctypeMetadata());
    list.addAll(defaults.getDoctypeMetadata());

    return list;
  }

  @Override
  public void setResourceBundle(final DoctypeResourceBundle resourceBundle) {
    primary.setResourceBundle(resourceBundle);
  }

  @Override
  public void setBaseTemplateName(final String baseTemplateName) {
    primary.setBaseTemplateName(baseTemplateName);
  }

  @Override
  public RelatedDoctypes getRelatedDoctypes() {
    return primary.getRelatedDoctypes();
  }

  @Override
  public void setRelatedDoctypes(final RelatedDoctypes relatedDoctypes) {
    primary.setRelatedDoctypes(relatedDoctypes);
  }

  @Override
  public boolean hasSection(final String key) {
    return primary.hasSection(key) || this.defaults.hasSection(key);
  }

  @Override
  public Section getSection(final String key) {
    Section section = primary.getSection(key);
    if (section == null) {
      section = this.defaults.getSection(key);
    }

    return section;
  }

  @Override
  public Wizard getWizard() {
    Wizard wizard = primary.getWizard();
    if (wizard.isSystemDefault()) {
      wizard = defaults.getWizard();
    }

    return wizard;
  }

  @Override
  public TypeReferences getTypeReferences() {
    return primary.getTypeReferences();
  }

  @Override
  public String getContextProvider(String packagePrefix) {
    return primary.getContextProvider(packagePrefix);
  }

  @Override
  public String getCreateResult() {
    return primary.getCreateResult();
  }

  // --- object basics --------------------------------------------------------

}
