/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.importer;

import de.smartics.maven.projectdoc.domain.ConfigurationException;
import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.doctype.CompoundDoctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.Property;
import de.smartics.maven.projectdoc.domain.bo.doctype.XmlSection;
import de.smartics.maven.projectdoc.domain.bo.doctype.XmlDoctype;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.DoctypeTemplateLocations;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import de.smartics.maven.projectdoc.domain.project.TemplateLocations;
import de.smartics.maven.projectdoc.domain.project.TemplateLocations.Context;
import de.smartics.maven.projectdoc.io.XmlFileFilter;

import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Service to access to doctype definitions.
 */
public class DoctypesImportService implements Iterable<Doctype> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ProjectConfiguration config;

  private final DoctypeIterator iterator;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private DoctypesImportService(final AddonRepository repository)
      throws SystemException {
    this.config = repository.getConfig();
    this.iterator = new DoctypeIterator();
  }

  // ****************************** Inner Classes *****************************

  private final class DoctypeAccessor {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final JAXBContext xmlContext;

    private final Unmarshaller unmarshaller;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private DoctypeAccessor() throws JAXBException {
      this.xmlContext = JAXBContext.newInstance(XmlDoctype.class,
          Property.class, XmlSection.class);
      this.unmarshaller = xmlContext.createUnmarshaller();
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    private Doctype unmarshal(final File file) throws JAXBException {
      final Doctype doctype = (Doctype) unmarshaller.unmarshal(file);
      return doctype;
    }

    private Doctype unmarshal(final String xml) throws JAXBException {
      try {
        final InputStream input =
            new ByteArrayInputStream(xml.getBytes("UTF-8"));
        final Doctype doctype = (Doctype) unmarshaller.unmarshal(input);
        return doctype;
      } catch (final UnsupportedEncodingException e) {
        // UTF-8 is provided per spec
        throw new IllegalStateException("Platform does not support UTF-8");
      }
    }

    // --- object basics ------------------------------------------------------
  }

  private final class DoctypeIterator implements Iterator<Doctype> {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final DoctypeAccessor accessor;

    private final File[] doctypeFiles;

    private int currentFileIndex;

    private Doctype cachedTypeDoctype;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private DoctypeIterator() throws SystemException {
      try {
        this.accessor = new DoctypeAccessor();
        this.doctypeFiles = config.listDoctypeFiles(XmlFileFilter.INSTANCE);
      } catch (final JAXBException e) {
        throw new SystemException(
            "Cannot access doctype files for XML parsing: " + e.getMessage(),
            e);
      }
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    @Override
    public boolean hasNext() {
      return cachedTypeDoctype != null
          || currentFileIndex < doctypeFiles.length;
    }

    @Override
    public Doctype next() throws ConfigurationException {
      if (hasNext()) {
        if (cachedTypeDoctype != null) {
          final Doctype cached = cachedTypeDoctype;
          cachedTypeDoctype = null;
          return cached;
        }

        final File currentFile = doctypeFiles[currentFileIndex];
        try {
          Doctype doctype = accessor.unmarshal(currentFile);
          final String baseTemplate = doctype.getBaseTemplateName();
          final TemplateLocations locations =
              new DoctypeTemplateLocations(doctype, config);
          if (StringUtils.isNotBlank(baseTemplate)) {
            final PlaceholderReplacer replacer =
                createBaseTemplateReplacer(doctype);
            final Context context = Context.Builder.a()
                .withLocation(DoctypeTemplateLocations.Location.DOCTYPE)
                .withReplacer(replacer).build();
            final String xmlContent = locations.getResourceAsString(context);
            final Doctype baseDoctype = accessor.unmarshal(xmlContent);
            doctype = new CompoundDoctype(doctype, baseDoctype);
          }

          final String provideType = doctype.getProvideType();
          if (StringUtils.isNotBlank(provideType)
              && !"none".equals(provideType)) {
            final Context context = Context.Builder.a()
                .withLocation(DoctypeTemplateLocations.Location.DOCTYPE)
                .build();
            final String xmlContent =
                locations.getResourceAsString(provideType, context);
            cachedTypeDoctype = accessor.unmarshal(xmlContent);
            cachedTypeDoctype.setResourceBundle(doctype.getResourceBundle());
            cachedTypeDoctype.setBaseTemplateName(provideType);
            cachedTypeDoctype.setRelatedDoctypes(doctype.getRelatedDoctypes());
            cachedTypeDoctype.setSecondaryId(doctype.getId());
          }

          currentFileIndex++;
          return doctype;
        } catch (final JAXBException e) {
          throw new ConfigurationException(
              "Cannot parse doctype definition from file '"
                  + currentFile.getAbsolutePath() + "' due to XML problems: "
                  + e.getMessage(),
              e);
        } catch (final IOException e) {
          throw new ConfigurationException(
              "Cannot parse doctype definition from file '"
                  + currentFile.getAbsolutePath() + "' due to IO problems: "
                  + e.getMessage(),
              e);
        }
      }

      throw new NoSuchElementException("No more doctypes available!");
    }

    private PlaceholderReplacer createBaseTemplateReplacer(
        final Doctype doctype) {
      final String doctypeId = doctype.getId();
      final String doctypeSpecificDescriptionKey =
          config.getPackage() + ".doctype." + doctypeId + ".description";
      final String descriptionId =
          doctype.hasSection(doctypeSpecificDescriptionKey) ? doctypeId
              : "common";
      final PlaceholderReplacer replacer = new PlaceholderReplacer(
          Tuple.a("${common.description}", descriptionId + ".description"));
      return replacer;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException(
          "Removing doctypes not supported.");
    }

    // --- object basics ------------------------------------------------------
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  public static DoctypesImportService create(final AddonRepository repository)
      throws ConfigurationException, SystemException {
    final ProjectConfiguration config = repository.getConfig();
    final File folder = config.getDoctypesFolder();
    if (!folder.exists()) {
      throw new ConfigurationException("Folder '" + folder.getAbsolutePath()
          + "', which should contain doctype definition files, does not exist.");
    }
    if (!folder.isDirectory()) {
      throw new ConfigurationException("Folder '" + folder.getAbsolutePath()
          + "', which should contain doctype definition files, exists, but is not a folder.");
    }
    if (!folder.canRead()) {
      throw new ConfigurationException("Folder '" + folder.getAbsolutePath()
          + "', which should contain doctype definition files, exists, but cannot be read.");
    }

    return new DoctypesImportService(repository);
  }

  @Override
  public Iterator<Doctype> iterator() {
    return iterator;
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
