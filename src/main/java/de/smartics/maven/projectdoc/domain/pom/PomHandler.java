/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.pom;

import de.smartics.maven.projectdoc.domain.bo.settings.ProjectSettings;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;

import org.apache.maven.model.Developer;
import org.apache.maven.model.DistributionManagement;
import org.apache.maven.model.IssueManagement;
import org.apache.maven.model.License;
import org.apache.maven.model.Model;
import org.apache.maven.model.Organization;
import org.apache.maven.model.Scm;
import org.apache.maven.model.io.DefaultModelReader;
import org.apache.maven.model.io.DefaultModelWriter;
import org.apache.maven.model.io.ModelParseException;
import org.apache.maven.model.io.ModelReader;
import org.apache.maven.model.io.ModelWriter;
import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Merges the information of the supplier POM file into the POM file.
 */
public class PomHandler {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Log log;

  private final PlaceholderReplacer replacer;

  // private final File supplierPomFile;

  private final Model supplierPom;

  private final File pomFile;

  private final Model pom;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public PomHandler(final Log log, final PlaceholderReplacer replacer,
      final File supplierPomFile, final File pomFile)
      throws ModelParseException, IOException {
    this.log = log;
    this.replacer = replacer;

    final ModelReader reader = new DefaultModelReader();

    // this.supplierPomFile = supplierPomFile;
    this.supplierPom = reader.read(supplierPomFile, null);

    this.pomFile = pomFile;
    this.pom = reader.read(pomFile, null);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void execute(final ProjectSettings settings) throws IOException {
    final Organization organization = supplierPom.getOrganization();
    if (organization != null) {
      log.info("    Found organization to add to POM.");
      pom.setOrganization(organization);
    }

    final List<License> licenses = supplierPom.getLicenses();
    if (!licenses.isEmpty()) {
      log.info("    Found licenses to add to POM.");
      for (final License license : licenses) {
        final String comments = license.getComments();
        final String normalizedComments = replacer.replace(comments);
        license.setComments(normalizedComments);

        pom.addLicense(license);
      }
    }

    final List<Developer> developers = supplierPom.getDevelopers();
    if (!developers.isEmpty()) {
      log.info("    Found developers to add to POM.");
      for (final Developer developer : developers) {
        pom.addDeveloper(developer);
      }
    }

    final Scm scm = supplierPom.getScm();
    if (scm != null) {
      log.info("    Found SCM configuration to add to POM.");
      pom.setScm(scm);
    }

    final IssueManagement issueManagement = supplierPom.getIssueManagement();
    if (issueManagement != null) {
      log.info("    Found issue management configuration to add to POM.");

      final String url = issueManagement.getUrl();
      final String normalizedUrl = replacer.replace(url);
      issueManagement.setUrl(normalizedUrl);

      pom.setIssueManagement(issueManagement);
    }

    final DistributionManagement distributionManagement =
        supplierPom.getDistributionManagement();
    if (distributionManagement != null) {
      log.info(
          "    Found distribution management configuration to add to POM.");
      pom.setDistributionManagement(distributionManagement);
    }

    final ModelWriter writer = new DefaultModelWriter();
    writer.write(pomFile, null, pom);
  }

  // --- object basics --------------------------------------------------------

}
