/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import static de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.calcKey;

import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.Label;
import de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.Labels;
import de.smartics.maven.projectdoc.domain.bo.doctype.L10n;
import de.smartics.maven.projectdoc.domain.bo.doctype.Property;
import de.smartics.maven.projectdoc.domain.bo.doctype.Section;
import de.smartics.maven.projectdoc.domain.bo.doctype.Term;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

class L10nHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Map<String, Properties> resourceBundles =
      new HashMap<String, Properties>();

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private L10nHelper() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  static L10nHelper create(final ProjectConfiguration projectConfig,
      final Doctype doctype) {
    final String doctypeId = doctype.getId();
    final L10nHelper helper = new L10nHelper();
    final String packageId = projectConfig.getPackage();

    for (final Labels l10n : doctype.getResourceBundle()) {
      final String locale = l10n.getLocale();
      final Properties properties = getProperties(helper, locale);

      if (!doctype.isStandardType()) {
        add(projectConfig, properties, packageId + ".blueprint.wizard."
            + doctypeId + ".page1.desc.content", l10n.getAbout());
        add(projectConfig, properties,
            packageId + ".blueprint." + doctypeId + ".create-link.description",
            l10n.getDescription());

        // We do not support additional labels at doctype level currently
//        for (final Label label : l10n) {
//          add(properties, label.getKey(), label.getValue());
//        }
      }
    }

    for (final Property property : doctype.getProperties()) {
      final String propertyId = calcKey(doctypeId, property.getKey());
      for (final L10n l10n : property.getResourceBundle()) {
        final String locale = l10n.getLocale();
        final Properties properties = getProperties(helper, locale);
        add(projectConfig, properties, propertyId, l10n.getName());
        final String description = l10n.getDescription();
        if (StringUtils.isNotBlank(description)
            || property.hasPlaceholderValue()) {
          // Either a placeholder value has been specified or demanded. Not all
          // placeholder values are used in the template, but may be used in the
          // wizard or elsewhere.
          add(projectConfig, properties, propertyId + ".placeholder",
              description);
        }
      }
    }

    for (final Section section : doctype.getSections()) {
      final String sectionId = calcKey(doctypeId, section.getKey());
      for (final Labels l10n : section.getResourceBundle()) {
        final String locale = l10n.getLocale();
        final Properties properties = getProperties(helper, locale);
        add(projectConfig, properties, sectionId, l10n.getName());
        add(projectConfig, properties, sectionId, l10n.getDescription(),
            ".placeholder");
        add(projectConfig, properties, sectionId, l10n.getIntro(), ".intro");
        add(projectConfig, properties, sectionId, l10n.getExtro(), ".extro");
        for (final Label label : l10n) {
          add(projectConfig, properties, label.getKey(), label.getValue());
        }
      }
    }

    return helper;
  }

  private static void add(final ProjectConfiguration projectConfig,
      final Properties properties, final String sectionId, final String intro,
      final String suffix) {
    if (StringUtils.isNotBlank(intro)) {
      add(projectConfig, properties, sectionId + suffix, intro);
    }
  }

  private static void add(final ProjectConfiguration projectConfig,
      Properties properties, String propertyId, Term name) {
    if (name != null) {
      add(projectConfig, properties, propertyId, name.getSingular());
    }
  }

  private static void add(final ProjectConfiguration projectConfig,
      final Properties properties, final String key, final String value) {
    final String oldValue = properties.getProperty(key);
    if (oldValue != null) {
      projectConfig.info("Skipped overriding property value '" + oldValue
          + "' for key '" + key + "': " + value);
      return;
    }

    if (StringUtils.isNotBlank(value)) {
      properties.put(key, value);
    } else {
      properties.put(key, "TODO");
    }
  }

  private static Properties getProperties(final L10nHelper helper,
      final String locale) {
    Properties properties = helper.resourceBundles.get(locale);
    if (properties == null) {
      properties = new Properties();
      helper.resourceBundles.put(locale, properties);
    }
    return properties;
  }

  // --- get&set --------------------------------------------------------------

  public Map<String, Properties> getResourceBundles() {
    return resourceBundles;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
