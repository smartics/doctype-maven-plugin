/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.Labels;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information on a single projectdoc doctype based on information from an XML
 * file.
 */
@XmlRootElement(name = "doctype")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlDoctype implements Doctype {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The identifier of the doctype.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String id;

  /**
   * The plural version of the identifier. If not specified, an 's' will be
   * appended to the name.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String plural;

  /**
   * The identifier of the doctype's category.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String category;

  /**
   * The flag indicates whether (<code>true</code>) or not (<code>false</code>)
   * the doctype provides a homepage.
   *
   * @since 1.0
   */
  @XmlAttribute(name = "has-homepage")
  private Boolean hasHomepage;

  /**
   * The name of the base template to fetch default information. This
   * information includes standard properties, standard sections, and other
   * resources.
   *
   * @since 1.0
   */
  @XmlAttribute(name = "base-template")
  private String baseTemplateName;

  /**
   * If set to a value different to <code>none</code>, a type will be created
   * based on the specified template name.
   *
   * @since 1.0
   */
  @XmlAttribute(name = "provide-type")
  private String provideType;

  /**
   * The optional name of the context provider class for the blueprint.
   *
   * @since 1.0
   */
  @XmlAttribute(name = "context-provider")
  private String contextProvider;

  /**
   * The action to take on for page creation. May be <tt>edit</tt> (default) or
   * <tt>view</tt>.
   *
   * @since 1.0
   */
  @XmlAttribute(name = "create-result")
  private String createResult;

  /**
   * Provides localized information.
   *
   * @since 1.0
   */
  @XmlElement(name = "resource-bundle")
  private DoctypeResourceBundle resourceBundle;

  /**
   * The list of properties of this doctype.
   *
   * @since 1.0
   */
  @XmlElementWrapper(name = "properties")
  @XmlElement(name = "property")
  private List<Property> properties;

  /**
   * The list of metadata of this doctype.
   *
   * @since 1.1
   */
  @XmlElementWrapper(name = "metadata")
  @XmlElement(name = "property")
  private List<Metadata> metadata;

  /**
   * The list of sections of this doctype.
   *
   * @since 1.0
   */
  @XmlElementWrapper(name = "sections")
  @XmlElement(name = "section", type = XmlSection.class)
  private List<Section> sections;

  /**
   * The list of related doctypes. Information is used to add to home- and index
   * pages. But also stores other kind of relations that cannot be deducted from
   * the doctype repository.
   *
   * @since 1.0
   */
  @XmlElement(name = "related-doctypes")
  private RelatedDoctypes relatedDoctypes;

  /**
   * The selection of the wizard components.
   *
   * @since 1.0
   */
  private Wizard wizard;

  private String secondaryId;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public XmlDoctype() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getCategory() {
    if (StringUtils.isBlank(category)) {
      return "default";
    }
    return category;
  }

  @Override
  public boolean hasHomepage() {
    if (hasHomepage != null) {
      return hasHomepage;
    }

    return true;
  }

  @Override
  public String getContextProvider(final String packagePrefix) {
    if (StringUtils.isNotBlank(contextProvider)) {
      return contextProvider;
    }
    return packagePrefix + ".ProjectDocContextProviderExt";
  }

  @Override
  public String getCreateResult() {
    if (StringUtils.isBlank(createResult)) {
      return "edit";
    }
    return createResult;
  }

  @Override
  public String getName(final String locale) {
    if (resourceBundle != null) {
      for (final Labels l10n : resourceBundle) {
        if (!ObjectUtils.equals(locale, l10n.getLocale())) {
          continue;
        }

        final Term term = l10n.getName();
        if (term != null) {
          final String name = term.getSingular();
          return name;
        }
      }
    }

    return null;
  }

  @Override
  public String getNamePlural(final String locale) {
    if (resourceBundle != null) {
      for (final Labels l10n : resourceBundle) {
        if (!ObjectUtils.equals(locale, l10n.getLocale())) {
          continue;
        }

        final Term term = l10n.getName();
        if (term != null) {
          final String name = term.getPlural();
          if (StringUtils.isBlank(name)) {
            return term.getSingular() + "s";
          }
          return name;
        }
      }
    }

    return null;
  }

  @Override
  public boolean isType() {
    return (baseTemplateName.equals("type")
        || baseTemplateName.contains("-type"));
  }

  @Override
  public boolean isStandardType() {
    return baseTemplateName.contains("standard-type");
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String getSecondaryId() {
    if (StringUtils.isBlank(secondaryId)) {
      return id + "-type";
    }

    return secondaryId;
  }

  @Override
  public void setSecondaryId(final String secondaryId) {
    this.secondaryId = secondaryId;
  }

  @Override
  public boolean hasType() {
    final String type = getProvideType();
    return type != null && !"none".equals(type);
  }

  @Override
  public String getTypeName(final String locale) {
    if (resourceBundle != null) {
      for (final Labels l10n : resourceBundle) {
        if (!ObjectUtils.equals(locale, l10n.getLocale())) {
          continue;
        }

        final Term term = l10n.getType();
        if (term != null) {
          final String name = term.getSingular();
          if (name != null) {
            return name;
          }
        }

        final String name = getName(locale);
        if (StringUtils.isNotBlank(name)) {
          return name + " Type";
        }
      }
    }

    return null;
  }

  @Override
  public String getTypeNamePlural(final String locale) {
    if (resourceBundle != null) {
      for (final Labels l10n : resourceBundle) {
        if (!ObjectUtils.equals(locale, l10n.getLocale())) {
          continue;
        }

        final Term term = l10n.getType();
        if (term != null) {
          final String plural = term.getPlural();
          if (StringUtils.isNotBlank(plural)) {
            return plural;
          }

          final String singular = term.getSingular();
          if (StringUtils.isNotBlank(singular)) {
            return singular + 's';
          }
        }

        final String name = getName(locale);
        if (StringUtils.isNotBlank(name)) {
          return name + " Types";
        }
      }
    }

    return null;
  }

  @Override
  public String getBaseTemplateName() {
    if (StringUtils.isNotBlank(baseTemplateName)) {
      return baseTemplateName;
    }
    return "standard";
  }

  @Override
  public String getProvideType() {
    return provideType;
  }

  @Override
  public DoctypeResourceBundle getResourceBundle() {
    if (resourceBundle != null) {
      return resourceBundle;
    }
    return new DoctypeResourceBundle();
  }

  @Override
  public List<Property> getProperties() {
    if (properties != null) {
      return properties;
    }

    return Collections.emptyList();
  }

  @Override
  public List<Section> getSections() {
    if (sections != null) {
      return sections;
    }

    return Collections.emptyList();
  }

  @Override
  public void setResourceBundle(final DoctypeResourceBundle resourceBundle) {
    this.resourceBundle = resourceBundle;
  }

  @Override
  public void setBaseTemplateName(final String baseTemplateName) {
    this.baseTemplateName = baseTemplateName;
  }

  @Override
  public RelatedDoctypes getRelatedDoctypes() {
    if (relatedDoctypes != null) {
      return relatedDoctypes;
    }
    return new RelatedDoctypes();
  }

  @Override
  public void setRelatedDoctypes(final RelatedDoctypes relatedDoctypes) {
    this.relatedDoctypes = relatedDoctypes;
  }

  @Override
  public Wizard getWizard() {
    if (wizard != null) {
      return wizard;
    }

    return Wizard.DEFAULT;
  }


  @Override
  public TypeReferences getTypeReferences() {
    final TypeReferences references = new TypeReferences();

    if (properties != null) {
      for (final Property property : properties) {
        final TypeReference reference = property.getTypeReference(id);
        if (reference != null) {
          references.add(reference);
        }
      }
    }

    return references;
  }

  /**
   * Return the list of metadata of this doctype.
   *
   * @return the list of metadata of this doctype.
   */
  @Override
  public List<Metadata> getDoctypeMetadata() {
    if (metadata == null) {
      return new ArrayList<Metadata>();
    }
    return metadata;
  }

  // --- business -------------------------------------------------------------

  @Override
  public boolean hasSection(final String key) {
    for (final Section section : getSections()) {
      if (key.equals(section.getKey())) {
        return true;
      }
    }
    return false;
  }


  @Override
  public Section getSection(final String key) {
    for (final Section section : getSections()) {
      if (key.equals(section.getKey())) {
        return section;
      }
    }
    return null;
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "  ID        : " + id + "\n  Category  : " + getCategory()
        + "\n  Properties: " + properties + "\n  Sections  : " + sections;
  }
}
