/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.project.ExportLocation;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;

class DoctypeExportLocations extends ExportLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String DOCTYPES = CONTENT + "/doctypes/${doctypeId}";

  // --- members --------------------------------------------------------------


  private final String doctypeId;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private DoctypeExportLocations(final ProjectConfiguration projectConfig,
      final String doctypeId) {
    super(projectConfig);
    this.doctypeId = doctypeId;
  }

  // ****************************** Inner Classes *****************************

  static enum Location implements ExportLocation {
    ATLASSIAN_PLUGIN(RESOURCES_ROOT, "atlassian-plugin.xml"),

    TEXT_RESOURCE(RESOURCES, "text-resources${locale}.properties"),

    WIZARD_SOY(RESOURCES + "/soy/page-templates",
        "projectdoc-template-${doctypeId}.soy"),

    SUBSPACE_OPTION_SOY(RESOURCES + "/soy/page-templates/subspaces/",
        "projectdoc-template-homepages.soy"),

    PARTITION_CONTEXT_PROVIDER_JAVA(SOURCES, "subspace/PartitionContextProvider.java"),

    SPACE_TEMPLATE(CONTENT, "spaces/main.xml"),

//    CONTENT_MANAGEMENT_TEMPLATE(CONTENT,
//        "tools/content-management-dashboard.xml"),

    TEMPLATE(DOCTYPES, "${doctypeId}-template.xml"),

    HOME(DOCTYPES, "home.xml"),

    INDEX(DOCTYPES, "index.xml"),

    WIZARD_JS(RESOURCES + "/js", "blueprints.js");

    private final String folder;

    private final String file;

    private final String pathPattern;

    Location(final String folder, final String file) {
      this.folder = folder;
      this.file = file;
      this.pathPattern = folder + '/' + file;
    }

    @Override
    public String getFolder() {
      return folder;
    }

    @Override
    public String getFile() {
      return file;
    }

    @Override
    public String getPathPattern() {
      return pathPattern;
    }

    @Override
    public String toString(final String... args) {
      final String locale = args[3] != null ? args[3] : "";
      args[3] = locale;
      final String value = StringUtils.replaceEach(pathPattern, new String[] {
          "${shortId}", "${packagePath}", "${doctypeId}", "${locale}"}, args);
      return value;
    }

    @Override
    public String toString() {
      return pathPattern;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  static DoctypeExportLocations create(final ProjectConfiguration projectConfig,
      final String doctypeId) throws IOException {
    final DoctypeExportLocations locations =
        new DoctypeExportLocations(projectConfig, doctypeId);
    return locations;
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public boolean hasResource(final Location template) {
    final File file = getResource(template).getFile();
    return file.exists();
  }

  @Override
  protected String[] createPathArgs(String locale) {
    return new String[] {shortId, packagePath, doctypeId, locale};
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return shortId + ":" + doctypeId;
  }
}
