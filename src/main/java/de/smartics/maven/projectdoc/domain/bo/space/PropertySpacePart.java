/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.space;

import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;

import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Information on a property part. The content may be plain XML or other
 * elements that are easier to write down.
 *
 * @since 1.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressFBWarnings("UWF_UNWRITTEN_FIELD")
public class PropertySpacePart {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @XmlAttribute
  private String key;

  /**
   * The content of the property part.
   */
  private String xml;

  private Macro macro;

  private Placeholder placeholder;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getKey() {
    return key;
  }

  // --- business -------------------------------------------------------------

  public static boolean hasPlaceholderValue(final PropertySpacePart value) {
    // If there is no value, the placeholder is implied.
    return value == null || value.placeholder != null;
  }

  public void filter(final PlaceholderReplacer replacer) {
    if (macro != null) {
      macro.filter(replacer);
    } else if (StringUtils.isNotBlank(xml)) {
      xml = replacer.replace(xml);
    }
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    if (StringUtils.isBlank(xml)) {
      if (null != macro) {
        return macro.toString("                  ");
      }

      if (null != placeholder) {
        return placeholder.toString();
      }

      if (StringUtils.isNotBlank(key)) {
        return "<at:i18n at:key=\"" + key + "\"/>";
      }
    }
    return xml;
  }
}
