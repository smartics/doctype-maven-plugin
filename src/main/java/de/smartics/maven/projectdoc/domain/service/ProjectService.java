/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.service;

import de.smartics.maven.projectdoc.domain.ApplicationException;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.bo.tool.Tool;
import de.smartics.maven.projectdoc.domain.download.DownloadService;
import de.smartics.maven.projectdoc.domain.exporter.AddonExportService;
import de.smartics.maven.projectdoc.domain.exporter.DoctypeExportService;
import de.smartics.maven.projectdoc.domain.exporter.SpaceExportService;
import de.smartics.maven.projectdoc.domain.importer.DoctypesImportService;
import de.smartics.maven.projectdoc.domain.importer.DoctypesRelationService;
import de.smartics.maven.projectdoc.domain.importer.SpacesImportService;
import de.smartics.maven.projectdoc.domain.metadata.DoctypeMetadataBuilder;
import de.smartics.maven.projectdoc.domain.patch.PatchService;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.Mode;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import de.smartics.maven.projectdoc.domain.rdf.DoctypeRdfBuilder;

import java.io.File;

/**
 * Runs the transformation process for a doctype add-on project.
 */
public class ProjectService {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ProjectConfiguration projectConfig;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectService(final ProjectConfiguration projectConfig) {
    this.projectConfig = projectConfig;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void execute() {
    final AddonRepository repository = AddonRepository.create(projectConfig);
    try {
      final AddonExportService addonExportService =
          new AddonExportService(repository);
      addonExportService.export();

      if (!repository.hasGeneratedTemplates()) {
        projectConfig.info("EXPORTING Spaces ...");
        final SpacesImportService spaceImporter =
            SpacesImportService.create(repository);
        final SpaceExportService spaceExporter =
            new SpaceExportService(repository);
        for (final Space space : spaceImporter) {
          repository.addSpace(space);
        }

        final File toolsDir = repository.getConfig().getToolsFolder();
        if (toolsDir.isDirectory() && toolsDir.canRead()) {
          final File[] files = toolsDir.listFiles();
          if (files != null) {
            for (final File toolDir : files) {
              if (toolDir.isDirectory() && toolDir.canRead()) {
                final Tool tool = new Tool(toolDir);
                repository.addTool(tool);
              }
            }
          }
        }

        for (final Space space : repository.getSpaces()) {
          spaceExporter.export(space);
        }

        projectConfig.info("EXPORTING Spaces successful.");
      } else {
        projectConfig.info("SKIPPED EXPORTING Spaces.");
      }

      if (!repository.hasDoctypeModels()) {
        projectConfig.info("Skipping, since no doctype models are provided.");
      } else {
        projectConfig.info("EXPORTING Doctypes ...");
        final DoctypesImportService doctypeImporter =
            DoctypesImportService.create(repository);

        for (final Doctype doctype : doctypeImporter) {
          repository.addDoctype(doctype);
        }

        final DoctypesRelationService relationshipService =
            DoctypesRelationService.create(repository);
        for (final Doctype doctype : relationshipService) {
          projectConfig.info("Applied relation information to doctype '"
              + doctype.getId() + "'.");
        }

        final DoctypeExportService exporter =
            new DoctypeExportService(repository);
        final boolean categoryTree =
            this.projectConfig.isCategoryTreeRequested();
        final boolean createDocmap = this.projectConfig.isDocmapRequested();
        final boolean createCategoryTree =
            this.projectConfig.isCategoryTreeRequested();
        final boolean createRdf = this.projectConfig.isRdfRequested();
        final DoctypeRdfBuilder rdfBuilder =
            new DoctypeRdfBuilder(repository.getConfig().getRdfFolder());
        final DoctypeMetadataBuilder doctypeMetadataExportService =
            new DoctypeMetadataBuilder(repository.getConfig());
        for (final Doctype doctype : repository.getDoctypes()) {
          final boolean exported = exporter.export(doctype);
          if (createRdf) {
            rdfBuilder.createRdf(doctype);
          }
          if (createDocmap) {
            addonExportService.registerForDocmap(doctype);
          }
          if (categoryTree) {
            addonExportService.registerForCategoryTree(doctype);
          }

          doctypeMetadataExportService.writeMetadata(doctype);

          projectConfig.info("    " + (exported ? "" : "NOT ")
              + (projectConfig.getMode() == Mode.CREATE ? "EXPORTED"
                  : "UPDATED")
              + ": " + doctype.getId());
        }
        if (createRdf) {
          rdfBuilder.writeRdfFile(repository.getConfig().getShortId());
        }
        if (createDocmap) {
          addonExportService.writeDocmap();
        }
        if (createCategoryTree) {
          addonExportService.writeCategoryTree();
        }
      }

      if (projectConfig.skipPatching()) {
        projectConfig.info("Skipping patching since skip flag is set.");
      } else {
        projectConfig.info("Start patching ...");
        final PatchService patcher = new PatchService(repository);
        patcher.patch();
        projectConfig.info("Patching complete.");
      }

      final DownloadService downloadService = new DownloadService(repository);
      downloadService.downloadHomepageXmlFiles();
    } catch (final ApplicationException e) {
      throw e;
    }
  }

  // --- object basics --------------------------------------------------------

}
