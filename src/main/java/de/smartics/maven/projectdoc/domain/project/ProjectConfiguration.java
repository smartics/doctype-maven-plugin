/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import de.smartics.maven.projectdoc.domain.bo.settings.Reference;
import de.smartics.maven.projectdoc.mojo.PluginArtifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileFilter;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple.a;

/**
 * Provides project information.
 */
public class ProjectConfiguration {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String TEXT_RESOURCES_BASENAME = "l10n.text-resources";

  // --- members --------------------------------------------------------------

  private final Log log;

  private final String projectName;

  private final String shortId;

  private final String mainSpaceId;

  private final String groupId;

  private final String artifactId;

  private final String packagePrefix;

  private final String thePackage;

  private final String packagePath;

  private final Mode mode;

  private final boolean skipPatching;

  private final boolean generateDocmap;

  private final boolean generateCategoryTree;

  private final boolean generateRdf;

  private final File workingFolder;

  private final File modelsFolder;

  private final File rdfFolder;

  private final File doctypeMetadataFolder;

  private final File addonFolder;

  private final File spacesFolder;

  private final File toolsFolder;

  private final File doctypesFolder;

  private final File patchFolder;

  private final File patchFilesFolder;

  private final File backupFolder;

  private final PlaceholderReplacer replacer;

  private final List<Reference> references;

  private final List<PluginArtifact> pluginArtifacts;

  private final String pathTemplate;

  private final MavenSession mavenSession;

  @Nullable
  private final String locale;

  private boolean dataCenterApproved;
  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private ProjectConfiguration(final Builder builder) {
    this.log = builder.log;
    this.shortId = builder.shortId;
    this.mainSpaceId = builder.mainSpaceId;
    this.projectName = builder.projectName;
    this.groupId = builder.groupId;
    this.artifactId = builder.artifactId;
    this.packagePrefix = builder.packagePrefix;
    this.thePackage = packagePrefix + '.' + shortId;
    this.packagePath = thePackage.replace('.', '/');
    this.mode = builder.mode;
    this.skipPatching = builder.skipPatching;
    this.generateDocmap = builder.generateDocmap;
    this.generateCategoryTree = builder.generateCategoryTree;
    this.generateRdf = builder.generateRdf;
    this.workingFolder = builder.workingFolder;
    this.modelsFolder = builder.modelsFolder;
    this.rdfFolder = builder.rfdFolder;
    this.doctypeMetadataFolder = builder.doctypeMetadataFolder;
    this.spacesFolder = new File(modelsFolder, "spaces");
    this.addonFolder = new File(modelsFolder, "addon");
    this.toolsFolder = new File(modelsFolder, "tools");
    this.doctypesFolder = new File(modelsFolder, "doctypes");
    this.patchFolder = new File(modelsFolder, "patch");
    this.patchFilesFolder = new File(patchFolder, "files");
    this.backupFolder = new File(workingFolder, "target/backup");
    this.pluginArtifacts = builder.pluginArtifacts;
    this.pathTemplate = builder.pathTemplate;
    this.mavenSession = builder.mavenSession;
    this.locale = builder.locale;
    this.dataCenterApproved = builder.dataCenterApproved;

    this.replacer = new PlaceholderReplacer(a("${shortId}", shortId),
        a("${packageInPathFormat}", packagePath),
        a("${packagePrefix}", packagePrefix), a("${package}", thePackage));
    this.references = builder.references;
  }

  // ****************************** Inner Classes *****************************


  public static final class Builder {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private Log log;

    private String shortId;

    private String mainSpaceId;

    private String projectName;

    private String groupId;

    private String artifactId;

    private String packagePrefix;

    private Mode mode;

    private boolean skipPatching;

    private boolean generateDocmap;

    private boolean generateCategoryTree;

    public boolean generateRdf;

    private File workingFolder;

    private File modelsFolder;

    private File rfdFolder;

    private File doctypeMetadataFolder;

    private List<Reference> references;

    private List<PluginArtifact> pluginArtifacts;

    private String pathTemplate;

    private MavenSession mavenSession;

    private String locale;

    private boolean dataCenterApproved;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    public Builder withReferences(final List<Reference> references) {
      this.references = references;
      return this;
    }

    public Builder withPluginArtifacts(
        final List<PluginArtifact> pluginArtifacts) {
      this.pluginArtifacts = pluginArtifacts;
      return this;
    }

    public Builder withPathTemplate(final String pathTemplate) {
      this.pathTemplate = pathTemplate;
      return this;
    }

    public Builder withLog(final Log log) {
      this.log = log;
      return this;
    }

    public Builder withShortId(final String shortId) {
      this.shortId = shortId;
      return this;
    }

    public Builder withMainSpaceId(final String mainSpaceId) {
      this.mainSpaceId = mainSpaceId;
      return this;
    }

    public Builder withProjectName(final String projectName) {
      this.projectName = projectName;
      return this;
    }

    public Builder withGroupId(final String groupId) {
      this.groupId = groupId;
      return this;
    }

    public Builder withArtifactId(final String artifactId) {
      this.artifactId = artifactId;
      return this;
    }

    public Builder withPackagePrefix(final String packagePrefix) {
      this.packagePrefix = packagePrefix;
      return this;
    }

    public Builder withMode(final String mode) throws IllegalArgumentException {
      this.mode = Mode.fromString(mode);
      return this;
    }

    public Builder skipPatching(final boolean skipPatching) {
      this.skipPatching = skipPatching;
      return this;
    }

    public Builder generateDocmap(final boolean generateDocmap) {
      this.generateDocmap = generateDocmap;
      return this;
    }

    public Builder generateCategoryTree(final boolean generateCategoryTree) {
      this.generateCategoryTree = generateCategoryTree;
      return this;
    }

    public Builder generateRdf(final boolean generateRdf) {
      this.generateRdf = generateRdf;
      return this;
    }

    public Builder withWorkingFolder(final File workingFolder) {
      this.workingFolder = workingFolder;
      return this;
    }

    public Builder withModelsFolder(final File modelsFolder) {
      this.modelsFolder = modelsFolder;
      return this;
    }

    public Builder withRdfFolder(final File rfdFolder) {
      this.rfdFolder = rfdFolder;
      return this;
    }

    public Builder withDoctypeMetadataBaseFolder(
        final File doctypeMetadataFolder) {
      this.doctypeMetadataFolder = doctypeMetadataFolder;
      return this;
    }

    public Builder withSession(final MavenSession mavenSession) {
      this.mavenSession = mavenSession;
      return this;
    }

    public Builder withLocale(@Nullable final String locale) {
      this.locale = locale;
      return this;
    }

    public Builder withDataCenterApproved(@Nullable final boolean dataCenterApproved) {
      this.dataCenterApproved = dataCenterApproved;
      return this;
    }

    public ProjectConfiguration build() {
      if (references == null) {
        references = Collections.emptyList();
      }
      if (pluginArtifacts == null) {
        pluginArtifacts = Collections.emptyList();
      }
      return new ProjectConfiguration(this);
    }

    // --- business -----------------------------------------------------------

    // --- object basics ------------------------------------------------------
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getShortId() {
    return shortId;
  }

  public String getMainSpaceId() {
    return mainSpaceId;
  }

  public String getProjectName() {
    return projectName;
  }

  public String getGroupId() {
    return groupId;
  }

  public String getArtifactId() {
    return artifactId;
  }

  /**
   * The prefix of the package (without the short ID).
   *
   * @return the prefix of the package.
   */
  public String getPackagePrefix() {
    return packagePrefix;
  }

  /**
   * The package, including the short ID, with normal dot separators.
   *
   * @return the package.
   */
  public String getPackage() {
    return thePackage;
  }

  /**
   * The package, including the short ID, as a path.
   *
   * @return the package as a path.
   */
  public String getPackagePath() {
    return packagePath;
  }

  public Mode getMode() {
    return mode;
  }

  public boolean skipPatching() {
    return skipPatching;
  }

  public boolean isDocmapRequested() {
    return generateDocmap;
  }

  public boolean isCategoryTreeRequested() {
    return generateCategoryTree;
  }

  public boolean isRdfRequested() {
    return generateRdf;
  }

  public boolean isDataCenterApproved() {
    return dataCenterApproved;
  }

  public File getWorkingFolder() {
    return workingFolder;
  }

  public File getModelsFolder() {
    return modelsFolder;
  }

  public File getRdfFolder() {
    return rdfFolder;
  }

  public File getDoctypeMetadataFolder() {
    return doctypeMetadataFolder;
  }

  public File getAddonFolder() {
    return addonFolder;
  }

  public File getSpacesFolder() {
    return spacesFolder;
  }

  public File getToolsFolder() {
    return toolsFolder;
  }

  public File getDoctypesFolder() {
    return doctypesFolder;
  }

  public File getPatchFolder() {
    return patchFolder;
  }

  public File getPatchFilesFolder() {
    return patchFilesFolder;
  }

  public File getBackupFolder() {
    return backupFolder;
  }

  public File[] listDoctypeFiles(final FileFilter filter) {
    return doctypesFolder.listFiles(filter);
  }

  public File[] listSpaceFiles(final FileFilter filter) {
    return spacesFolder.listFiles(filter);
  }

  public void info(final String message) {
    if (log != null) {
      log.info(message);
    }
  }

  public PlaceholderReplacer getReplacer() {
    return replacer;
  }

  public List<Reference> getReferences() {
    return references;
  }

  public List<PluginArtifact> getPluginArtifacts() {
    return pluginArtifacts;
  }

  @CheckForNull
  public PluginArtifact getPluginArtifact(final String groupId,
      final String artifactId) {
    for (final PluginArtifact artifact : pluginArtifacts) {
      if (groupId.equals(artifact.getGroupId()) &&
          artifactId.equals(artifact.getArtifactId())) {
        return artifact;
      }
    }
    return null;
  }

  @CheckForNull
  public String getPluginArtifactVersion(final String groupId,
      final String artifactId) {
    for (final PluginArtifact artifact : pluginArtifacts) {
      if (groupId.equals(artifact.getGroupId()) &&
          artifactId.equals(artifact.getArtifactId())) {
        return artifact.getVersion();
      }
    }
    return null;
  }

  public String getPathTemplate() {
    return pathTemplate;
  }

  @CheckForNull
  public Reference getReference(final String name) {
    for (final Reference reference : references) {
      if (name.equals(reference.getName())) {
        return reference;
      }
    }

    return null;
  }

  public MavenSession getMavenSession() {
    return mavenSession;
  }

  public String getLocale() {
    return locale;
  }

  public ResourceBundle getResourceBundle(final String locale) {
    if (locale == null) {
      return ResourceBundle.getBundle(TEXT_RESOURCES_BASENAME, Locale.ENGLISH);
    }
    final Locale localeInstance = new Locale(locale);
    return ResourceBundle.getBundle(TEXT_RESOURCES_BASENAME, localeInstance);
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------
}
