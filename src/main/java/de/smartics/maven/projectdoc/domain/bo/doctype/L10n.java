/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import de.smartics.maven.projectdoc.io.StringFunction;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Provides a localized text resource.
 */
@XmlRootElement(name = "l10n")
@XmlAccessorType(XmlAccessType.FIELD)
public class L10n implements Iterable<XmlLabel> {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The locale of the text resource. If this value is <code>null</code> it is
   * the default representation.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String locale;

  /**
   * The name resource.
   *
   * @since 1.0
   */
  private Term name;

  /**
   * The description resource.
   *
   * @since 1.0
   */
  private String description;

  /**
   * The type resource.
   *
   * @since 1.0
   */
  private Term type;

  /**
   * The introduction text resource.
   *
   * @since 1.0
   */
  private String intro;

  /**
   * The extroduction text resource.
   *
   * @since 1.0
   */
  private String extro;

  /**
   * The about resource used for doctype wizards.
   *
   * @since 1.0
   */
  private String about;

  /**
   * The resources for labels with key and localized value.
   *
   * @since 1.0
   */
  @XmlElement(name = "label")
  private List<XmlLabel> labels;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  /**
   * Returns the locale of the text resource. If this value is <code>null</code>
   * it is the default representation.
   *
   * @return the locale of the text resource.
   */
  public String getLocale() {
    return locale;
  }

  /**
   * Returns the name resource.
   *
   * @return the name resource.
   */
  public Term getName() {
    return name;
  }

  /**
   * Returns the description resource.
   *
   * @return the description resource.
   */
  public String getDescription() {
    return normalize(description);
  }

  /**
   * Returns the type resource.
   *
   * @return the type resource.
   */
  public Term getType() {
    return type;
  }

  /**
   * Returns the introduction text resource.
   *
   * @return the introduction text resource.
   */
  public String getIntro() {
    return normalize(intro);
  }

  /**
   * Returns the extroduction text resource.
   *
   * @return the extroduction text resource.
   */
  public String getExtro() {
    return normalize(extro);
  }

  /**
   * Returns the about resource used for doctype wizards.
   *
   * @return the about resource used for doctype wizards.
   */
  public String getAbout() {
    return normalize(about);
  }

  // --- business -------------------------------------------------------------

  public static String normalize(final String input) {
    if (input == null) {
      return null;
    }
    return StringFunction.strip(input); // StringUtils.replace(, "\\n", "\\");
  }

  @Override
  public Iterator<XmlLabel> iterator() {
    if (labels != null) {
      return labels.iterator();
    }

    return Collections.<XmlLabel> emptyList().iterator();
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this,
        ToStringStyle.SHORT_PREFIX_STYLE);
  }
}
