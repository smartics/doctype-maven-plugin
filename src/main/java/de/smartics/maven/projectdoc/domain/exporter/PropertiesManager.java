/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.bo.addon.Category;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

/**
 *
 */
final class PropertiesManager {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Map<String, Properties> map = new HashMap<String, Properties>();

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  PropertiesManager(final List<String> locales) {
    for (final String locale : locales) {
      map.put(locale, new Properties());
    }
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public Map<String, Properties> getMap() {
    return map;
  }

  // --- business -------------------------------------------------------------

  public void setProperty(final String key, final Category category) {
    for (final Entry<String, Properties> entry : map.entrySet()) {
      final String locale = entry.getKey();
      final Properties properties = entry.getValue();
      final String namePlural = category.getNamePlural(locale);
      if (StringUtils.isNotBlank(namePlural)) {
        properties.setProperty(key, namePlural);
      }
    }
  }

  // --- object basics --------------------------------------------------------

}
