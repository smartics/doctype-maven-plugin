/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import de.smartics.maven.projectdoc.domain.project.DoctypeTemplateLocations.Location;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.IOException;

/**
 * Provides access to templates provided by the system.
 */
public interface TemplateLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Inner Classes *****************************

  public static final class Context {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final ImportLocation location;

    private final String locale;

    private final PlaceholderReplacer replacer;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private Context(final Builder builder) {
      this.location = builder.location;
      this.locale = builder.locale;
      this.replacer = builder.replacer;
    }

    // ***************************** Inner Classes ****************************

    public static final class Builder {
      private ImportLocation location;

      private String locale;

      private PlaceholderReplacer replacer;

      private Builder() {}

      public static Builder a() {
        return new Builder();
      }

      public Builder withLocation(final ImportLocation location) {
        this.location = location;
        return this;
      }

      public Builder withLocale(final String locale) {
        this.locale = locale;
        return this;
      }

      public Builder withReplacer(final PlaceholderReplacer replacer) {
        this.replacer = replacer;
        return this;
      }

      public Context build() {
        return new Context(this);
      }

      public static Context build(Location location) {
        return a().withLocation(location).build();
      }
    }

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- factory ------------------------------------------------------------

    public static Context defaultLocale(Context context) {
      return Builder.a().withLocation(context.location)
          .withReplacer(context.getReplacer()).withLocale(null).build();
    }

    // --- get&set ------------------------------------------------------------

    public ImportLocation getLocation() {
      return location;
    }

    public String getLocale() {
      return locale;
    }

    public PlaceholderReplacer getReplacer() {
      return replacer;
    }

    public String replace(String template) {
      if(replacer != null) {
        return replacer.replace(template);
      }
      return template;
    }

    // --- business -----------------------------------------------------------

    // --- object basics ------------------------------------------------------

    @Override
    public String toString() {
      return ToStringBuilder.reflectionToString(this,
          ToStringStyle.SHORT_PREFIX_STYLE);
    }
  }

  // ********************************* Methods ********************************

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * The context allows to provide standardized information to the location
   * resolver.
   *
   * @param context information relevant to load the resource.
   * @return the contents of the resource.
   * @throws IOException on any problem accessing and loading the contents of
   *         the resource.
   */
  String getResourceAsString(Context context) throws IOException;

  /**
   * The context allows to provide standardized information to the location
   * resolver.
   *
   * @param templateSetId the identifier of the template set. Normally this can
   *        be derived from the doctype. If a related information needs to be
   *        fetched, the value specifies the template set for the location
   *        context.
   * @param context information relevant to load the resource.
   * @return the contents of the resource.
   * @throws IOException on any problem accessing and loading the contents of
   *         the resource.
   */
  String getResourceAsString(String templateSetId, Context context)
      throws IOException;

  // --- object basics --------------------------------------------------------

}
