/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.project.ExportLocation;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import org.apache.commons.lang.StringUtils;

public class SpaceExportLocations extends ExportLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final String projectName;

  private final Space space;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private SpaceExportLocations(final ProjectConfiguration projectConfig,
      final Space space) {
    super(projectConfig);
    this.projectName = projectConfig.getProjectName();
    this.space = space;
  }

  // ****************************** Inner Classes *****************************


  public static enum Location implements ExportLocation {
    ATLASSIAN_PLUGIN(RESOURCES_ROOT, "atlassian-plugin.xml"),

    TEXT_RESOURCE(RESOURCES, "text-resources${locale}.properties"),

    SPACE_TEMPLATE(CONTENT, "spaces/${spaceId}.xml"),

    WIZARD_JS(RESOURCES + "/js", "space-blueprint.js"),

    WIZARD_SOY(RESOURCES + "/soy", "create-space-${spaceId}.soy"),

    SPECIALPAGES_OPTION_SOY(RESOURCES + "/soy/page-templates/subspaces",
        "projectdoc-template-specialpages.soy");

    //    CONTENT_MANAGEMENT_DASHBOARD_TEMPLATE(CONTENT,
    //        "tools/content-management-dashboard.xml");

    private final String folder;

    private final String file;

    private final String pathPattern;

    Location(final String folder, final String file) {
      this.folder = folder;
      this.file = file;
      this.pathPattern = folder + '/' + file;
    }

    @Override
    public String getFolder() {
      return folder;
    }

    @Override
    public String getFile() {
      return file;
    }

    @Override
    public String getPathPattern() {
      return pathPattern;
    }

    @Override
    public String toString(final String... args) {
      final String locale = args[2] != null ? args[2] : "";
      args[2] = locale;
      final String value = StringUtils.replaceEach(pathPattern,
          new String[] {"${shortId}", "${packagePath}", "${locale}",
              "${spaceId}"}, args);
      return value;
    }

    @Override
    public String toString() {
      return pathPattern;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  public static SpaceExportLocations createForExport(
      final ProjectConfiguration projectConfig, final Space space) {
    return new SpaceExportLocations(projectConfig, space);
  }

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected String[] createPathArgs(final String locale) {
    return new String[] {shortId, packagePath, locale,
        space != null ? space.getId() : ""};
  }

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return shortId + ':' + projectName +
           (space != null ? ':' + space.getId() : "");
  }
}
