/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.settings.Reference;
import de.smartics.maven.projectdoc.domain.project.AbstractTemplateLocations;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.ExportLocation;
import de.smartics.maven.projectdoc.domain.project.ImportLocation;
import de.smartics.maven.projectdoc.domain.project.Mode;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import de.smartics.maven.projectdoc.io.FileFunction;

import org.apache.commons.lang.StringUtils;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;

abstract class CopyHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  protected final AddonRepository repository;

  protected final String id;

  protected final Mode mode;

  protected final String shortId;

  protected final String groupId;
  protected final String artifactId;


  protected final String packageId;
  protected final String packageInPathFormat;

  protected final Reference addonDocumentation;

  protected final ExportLocations exportLocations;

  protected final AbstractTemplateLocations importLocations;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  protected CopyHelper(final AddonRepository repository, final String id,
      final Mode mode, final ExportLocations exportLocations,
      final AbstractTemplateLocations importLocations) throws IOException {
    this.repository = repository;
    this.id = id;
    this.mode = mode;

    final ProjectConfiguration projectConfig = repository.getConfig();
    this.shortId = projectConfig.getShortId();
    this.groupId = projectConfig.getGroupId();
    this.artifactId = projectConfig.getArtifactId();
    this.packageId = projectConfig.getPackage();
    this.packageInPathFormat = projectConfig.getPackagePath();
    this.addonDocumentation = projectConfig.getReference("addon-documentation");
    this.exportLocations = exportLocations;
    this.importLocations = importLocations;
  }

  // ****************************** Inner Classes *****************************

  protected static final class ExportMarker {
    public static final ExportMarker PLAIN = new ExportMarker(null, null);

    private final String startMarker;
    private final String endMarker;

    protected ExportMarker(final String startMarker, final String endMarker) {
      this.startMarker = startMarker;
      this.endMarker = endMarker;
    }

    protected String mark(final String string) {
      if (startMarker != null) {
        return startMarker + string + endMarker;
      }
      return string;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  protected void writePage(final File file, final String marker,
      final StringBuilder buffer) throws IOException {
    final String xml = FileUtils.fileRead(file, "UTF-8");
    final String fragment =
        StringUtils.replace(xml, marker, marker + "\n\n" + buffer);
    FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", fragment);
  }

  protected String fragment(final ImportLocation template) {
    return fragment(template, null, null);
  }

  protected String fragment(final ImportLocation template,
      final PlaceholderReplacer replacer) {
    return fragment(template, null, replacer);
  }

  protected String fragment(final ImportLocation location, final String locale,
      final PlaceholderReplacer replacer) {
    try {
      final String data =
          importLocations.getResourceAsString(id, location, replacer);
      return data;
    } catch (final IOException e) {
      throw new SystemException("Cannot fetch fragment " + location.toString()
          + ": " + e.getMessage(), e);
    }
  }

  protected void replace(final ExportLocation export,
      final ImportLocation location, final String encoding,
      final PlaceholderReplacer replacer, final ExportMarker marker) {
    final File file = exportLocations.getResource(export).getFile();
    if (file.exists()) {
      try {
        final String fragment = marker.mark(fragment(location, replacer));
        String xml = FileUtils.fileRead(file, encoding);
        xml = replace(replacer, xml, location, fragment, "\n");
        FileUtils.fileWrite(file.getAbsolutePath(), encoding, xml);
      } catch (final IOException e) {
        throw new SystemException("Cannot apply changes to "
            + file.getAbsolutePath() + ": " + e.getMessage(), e);
      }
    } else {
      final ProjectConfiguration projectConfig = repository.getConfig();
      projectConfig.info("Cannot configure with " + location.getPathPattern()
          + ", since " + file.getAbsolutePath() + " does not exist.");
    }
  }

  protected String replace(final PlaceholderReplacer replacer,
      final String content, final ImportLocation location,
      final String fragment, final String separator) {
    final String marker = replacer.replace(location.getMarker());
    return replace(content, location, fragment, separator, marker);
  }

  protected String replace(final String content, final ImportLocation location,
      final String fragment, final String separator, final String marker) {
    final String replaced = StringUtils.replaceEach(content,
        new String[] {marker}, new String[] {marker + separator + fragment});
    return replaced;
  }

  protected void append(final ExportLocation export,
      final ImportLocation location, final String encoding, final String locale,
      final PlaceholderReplacer replacer, final String... locationParams) {
    final File file = exportLocations.getResource(export, locale).getFile();
    try {
      final String data = importLocations.getResourceAsString(id, location,
          locale, replacer, locationParams);
      if (StringUtils.isBlank(data)) {
        return;
      }
      if (!file.exists()) {
        FileFunction.ensureContainingFolder(file);
        final String replaced = replacer.replace(data);
        FileUtils.fileWrite(file.getAbsolutePath(), encoding, replaced);
      } else {
        FileUtils.fileAppend(file.getAbsolutePath(), encoding, "\n\n" + data);
      }
    } catch (final IOException e) {
      throw new SystemException("Cannot apply changes to "
          + file.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  protected void append(final SpaceExportLocations.Location export,
      final String data, final String encoding, final String locale) {
    final File file = exportLocations.getResource(export, locale).getFile();
    try {
      if (!file.exists()) {
        FileFunction.ensureContainingFolder(file);
        FileUtils.fileWrite(file.getAbsolutePath(), encoding, data);
      } else {
        FileUtils.fileAppend(file.getAbsolutePath(), encoding, "\n\n" + data);
      }
    } catch (final IOException e) {
      throw new SystemException("Cannot apply changes to "
          + file.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  protected void copy(final SpaceExportLocations.Location export,
      final ImportLocation location) {
    copy(export, location, null);
  }

  protected void copy(final SpaceExportLocations.Location export,
      final ImportLocation location, final PlaceholderReplacer replacer) {
    copy(export, location, "UTF-8", null, replacer);
  }

  protected void copy(final SpaceExportLocations.Location export,
      final ImportLocation location, final String encoding, final String locale,
      final PlaceholderReplacer replacer) {
    final File file = exportLocations.getResource(export).getFile();
    try {
      final String data = importLocations.getResourceAsString(id, location,
          locale, replacer, new String[] {id, locale});
      handleFileIssues(file);
      FileUtils.fileWrite(file.getAbsolutePath(), encoding, data);
    } catch (final IOException e) {
      throw new SystemException("Cannot apply changes to "
          + file.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  private void handleFileIssues(final File file) throws IOException {
    if (file.exists()) {
      if (Mode.UPDATE == mode) {
        FileUtils.forceDelete(file);
      }
    } else {
      FileFunction.ensureContainingFolder(file);
    }
  }

  // --- object basics --------------------------------------------------------

}
