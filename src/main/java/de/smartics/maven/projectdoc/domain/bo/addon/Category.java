/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.addon;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Information on a single projectdoc category based on information from an XML
 * file.
 */
@XmlRootElement(name = "category")
@XmlAccessorType(XmlAccessType.FIELD)
public class Category {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The identifier of the category.
   *
   * @since 1.0
   */
  @XmlAttribute
  private String id;

  /**
   * Provides localized information.
   *
   * @since 1.0
   */
  @XmlElementWrapper(name = "resource-bundle")
  @XmlElement(name = "l10n")
  private List<L10n> resourceBundle;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public Category() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getId() {
    return id;
  }

  private List<L10n> getResourceBundle() {
    if (resourceBundle != null) {
      return resourceBundle;
    }

    return Collections.emptyList();
  }


  public String getName(final String locale) {
    for (final L10n l10n : getResourceBundle()) {
      if (StringUtils.equals(locale, l10n.getLocale())) {
        return l10n.getName();
      }
    }
    return null;
  }

  public String getNamePlural(final String locale) {
    for (final L10n l10n : getResourceBundle()) {
      if (StringUtils.equals(locale, l10n.getLocale())) {
        return l10n.getPlural();
      }
    }
    return null;
  }

  public String getDescription(final String locale) {
    for (final L10n l10n : getResourceBundle()) {
      if (StringUtils.equals(locale, l10n.getLocale())) {
        return l10n.getDescription();
      }
    }
    return null;
  }

  public List<String> getSupportedLocales() {
    final List<L10n> resourceBundle = getResourceBundle();
    final List<String> locales = new ArrayList<String>(resourceBundle.size());
    for (final L10n l10n : resourceBundle) {
      final String locale = l10n.getLocale();
      locales.add(locale);
    }
    return locales;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return "  ID        : " + id;
  }
}
