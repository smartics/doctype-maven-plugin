/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.download;

import de.smartics.maven.projectdoc.domain.SystemException;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.ArtifactRepositoryPolicy;
import org.apache.maven.artifact.repository.MavenArtifactRepository;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.artifact.repository.metadata.ArtifactRepositoryMetadata;
import org.apache.maven.artifact.repository.metadata.RepositoryMetadata;
import org.apache.maven.artifact.repository.metadata.RepositoryMetadataManager;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionRequest;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.repository.RepositorySystem;
import org.apache.maven.settings.Profile;
import org.apache.maven.settings.Repository;
import org.apache.maven.settings.RepositoryPolicy;
import org.apache.maven.settings.Settings;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class ArtifactRepositoryService {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final MavenSession session;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ArtifactRepositoryService(final MavenSession session) {
    this.session = session;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public ArchiveExtractor resolveFrom(final String groupId,
      final String artifactId, final String version, final String type,
      final String classifier) throws ArtifactNotFoundException,
      NullPointerException, IllegalArgumentException, IOException {
    final Artifact artifact = new DefaultArtifact(groupId, artifactId, version,
        "compile", type, classifier, new DefaultArtifactHandler(type));

    final ArtifactResolutionRequest artifactRequest = createRequest(artifact);

    try {
      final RepositorySystem repositorySystem = (RepositorySystem) session
          .lookup("org.apache.maven.repository.RepositorySystem", "default");
//      repositorySystem.injectAuthentication(session.getRepositorySession(),
//          artifactRequest.getRemoteRepositories());
      final ArtifactResolutionResult result =
          repositorySystem.resolve(artifactRequest);

      final File localFile = artifact.getFile();
      if (localFile.canRead()) {
        return new ArchiveExtractor(localFile);
      }

      final Set<Artifact> resolvedArtifacts = result.getArtifacts();
      if (!resolvedArtifacts.isEmpty()) {
        final Artifact resolvedArtifact = resolvedArtifacts.iterator()
            .next();
        return new ArchiveExtractor(resolvedArtifact.getFile());
      } else {
        RepositoryMetadataManager manager = (RepositoryMetadataManager) session
            .lookup(RepositoryMetadataManager.class.getName(), "default");
        final RepositoryMetadata metadata =
            new ArtifactRepositoryMetadata(artifact);
        manager.resolve(metadata, calcRemoteRepos(),
            session.getLocalRepository());
        if (localFile.canRead()) {
          return new ArchiveExtractor(localFile);
        }
      }
      throw new SystemException("Cannot resolve artifact.");
    } catch (final Exception e) {
      throw new SystemException("Cannot resolve artifact.", e);
    }
  }

  private List<ArtifactRepository> calcRemoteRepos() {
    final List<ArtifactRepository> repos = new ArrayList<>();

    repos.addAll(session.getRequest()
        .getRemoteRepositories());

    final Settings settings = session.getSettings();
    final Map<String, Profile> mapping = settings.getProfilesAsMap();
    for (final String profileName : settings.getActiveProfiles()) {
      final Profile profile = mapping.get(profileName);
      if (profile != null) {
        addRepos(repos, profile.getRepositories());
      }
    }

    return repos;
  }

  private void addRepos(List<ArtifactRepository> repos,
      List<Repository> repositories) {
    for (final Repository repository : repositories) {
      final MavenArtifactRepository repo =
          new MavenArtifactRepository(repository.getId(), repository.getUrl(),
              new DefaultRepositoryLayout(), wrap(repository.getSnapshots()),
              wrap(repository.getReleases()));
      repos.add(repo);
    }
  }

  private ArtifactRepositoryPolicy wrap(final RepositoryPolicy settingsPolicy) {
    if (settingsPolicy == null) {
      return null;
    }
    final ArtifactRepositoryPolicy policy = new ArtifactRepositoryPolicy(
        settingsPolicy.isEnabled(), settingsPolicy.getChecksumPolicy(),
        settingsPolicy.getUpdatePolicy());
    return policy;
  }

  private ArtifactResolutionRequest createRequest(final Artifact artifact) {
    final ArtifactResolutionRequest artifactRequest =
        new ArtifactResolutionRequest();
    artifactRequest.setArtifact(artifact);
    artifactRequest.setLocalRepository(session.getLocalRepository());
    artifactRequest.setRemoteRepositories(calcRemoteRepos());
    artifactRequest.setOffline(session.isOffline());
    artifactRequest.setCache(session.getRepositoryCache());
    artifactRequest.setMirrors(session.getSettings()
        .getMirrors());
    artifactRequest.setProxies(session.getSettings()
        .getProxies());
    artifactRequest.setResolveTransitively(false);
    artifactRequest.setServers(session.getSettings()
        .getServers());
    return artifactRequest;
  }

  // --- object basics --------------------------------------------------------

}
