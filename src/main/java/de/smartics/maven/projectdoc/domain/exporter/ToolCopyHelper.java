/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import static de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple.a;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.bo.tool.Tool;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.ExportLocation;
import de.smartics.maven.projectdoc.domain.project.ImportLocation;
import de.smartics.maven.projectdoc.domain.project.Mode;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import de.smartics.maven.projectdoc.domain.project.ToolBundleLocations;
import de.smartics.maven.projectdoc.domain.project.ToolsTemplateLocations;
import de.smartics.maven.projectdoc.io.XmlFileFilter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.IOException;

class ToolCopyHelper extends CopyHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Space space;

  private final String toolId;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  ToolCopyHelper(final AddonRepository repository, final Space space,
      final String toolId, final Mode mode) throws IOException {
    super(repository, toolId, mode,
        SpaceExportLocations.createForExport(repository.getConfig(), space),
        new ToolsTemplateLocations(repository.getConfig().getToolsFolder()));
    this.space = space;
    this.toolId = toolId;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  void provide() {
    final Tool tool = repository.getTool(toolId);
    copyPages(tool);
    subspaceOptionSoy();
    addProperties();
    addToAtlassianPluginXml();
  }

  private void subspaceOptionSoy() throws SystemException {
    final String toolIdSoyFix = StringEscapeUtils.escapeHtml4(toolId);
    final PlaceholderReplacer replacer = new PlaceholderReplacer(
        a("${toolId}", toolId), a("${toolIdSoyFix}", toolIdSoyFix));
    replaceContent(SpaceExportLocations.Location.SPECIALPAGES_OPTION_SOY,
        ToolsTemplateLocations.Location.TEMPLATE, "UTF-8", replacer);
  }

  private void replaceContent(final ExportLocation export,
      final ImportLocation template, final String encoding,
      final PlaceholderReplacer replacer) {
    final File file = exportLocations.getResource(export).getFile();
    if (file.exists()) {
      try {
        final String fragment = fragment(template, replacer);
        String xml =
            org.codehaus.plexus.util.FileUtils.fileRead(file, encoding);
        xml = replace(xml, template, fragment, "\n", ExportMarker.PLAIN, null);
        org.codehaus.plexus.util.FileUtils.fileWrite(file.getAbsolutePath(),
            encoding, xml);
      } catch (final IOException e) {
        throw new SystemException("Cannot apply changes to "
            + file.getAbsolutePath() + ": " + e.getMessage(), e);
      }
    }
  }

  private String replace(final String content, final ImportLocation location,
      final String fragment, final String separator,
      final ExportMarker exportMarker, final PlaceholderReplacer replacer) {
    String marker = location.getMarker();
    if (replacer != null) {
      marker = replacer.replace(marker);
    }

    final String replaced =
        StringUtils.replaceEach(content, new String[] {marker},
            new String[] {marker + separator + exportMarker.mark(fragment)});
    return replaced;
  }

  private void copyPages(final Tool tool) {
    if (tool == null) {
      return;
    }
    final File root = tool.getToolDir();
    if (root.isDirectory() && root.canRead()) {
      final File pagesFolder = new File(root, "pages");
      if (pagesFolder.isDirectory() && pagesFolder.canRead()) {
        final File targetToolsFolder = repository.getTargetToolsFolder();
        final File[] files = pagesFolder.listFiles(XmlFileFilter.INSTANCE);
        if (files != null) {
          for (final File pageFile : files) {
            try {
              FileUtils.copyFileToDirectory(pageFile, targetToolsFolder);
            } catch (final IOException e) {
              throw new SystemException(
                  "Cannot copy file " + pageFile.getAbsolutePath()
                      + " for tool " + tool.getId() + ": " + e.getMessage(),
                  e);
            }
          }
        }
      }
    }
  }

  private void addProperties() {
    for (final String locale : repository.getSupportedLocales()) {
      final PlaceholderReplacer replacer = new PlaceholderReplacer(
          a("${shortId}", shortId), a("${spaceId}", space.getId()),
          a("${package}", packageId), a("${toolId}", toolId));
      append(SpaceExportLocations.Location.TEXT_RESOURCE,
          ToolBundleLocations.Location.TEXT_RESOURCE, "ISO-8859-1", locale,
          replacer, new String[] {toolId, locale});
    }
  }

  private void addToAtlassianPluginXml() {
    final String spaceId = space.getId();
    final PlaceholderReplacer replacer = new PlaceholderReplacer(
        a("${shortId}", shortId), a("${spaceId}", spaceId),
        a("${packageInPathFormat}", packageInPathFormat),
        a("${package}", packageId), a("${toolId}", toolId));
    replace(SpaceExportLocations.Location.ATLASSIAN_PLUGIN,
        ToolBundleLocations.Location.SPACE, "UTF-8", replacer,
        ExportMarker.PLAIN);
    replace(SpaceExportLocations.Location.ATLASSIAN_PLUGIN,
        ToolBundleLocations.Location.TOOL, "UTF-8", replacer,
        ExportMarker.PLAIN);
    replace(SpaceExportLocations.Location.ATLASSIAN_PLUGIN,
        ToolBundleLocations.Location.SPECIAL, "UTF-8", replacer,
        ExportMarker.PLAIN);
  }

  // --- object basics --------------------------------------------------------

}
