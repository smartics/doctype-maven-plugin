/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeRef;
import de.smartics.maven.projectdoc.domain.bo.doctype.XmlDoctype;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.Mode;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import java.io.IOException;
import java.util.Collection;

/**
 * Writes doctype information to the add-on.
 */
public class DoctypeExportService {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final AddonRepository repository;

  private final ProjectConfiguration projectConfig;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DoctypeExportService(final AddonRepository repository) {
    this.repository = repository;
    this.projectConfig = repository.getConfig();
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void writeToHome(final String doctypeId, final String homepageXml)
      throws SystemException {
    try {
      final XmlDoctype doctype = new XmlDoctype();
      doctype.setId(doctypeId);
      final DoctypeCopyHelper copy = new DoctypeCopyHelper(repository, doctype);
      copy.writeToHome(homepageXml);
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot create backup folder: " + e.getMessage(), e);
    }
  }

  public boolean export(final Doctype doctype) throws SystemException {
    try {
      final DoctypeCopyHelper copy = new DoctypeCopyHelper(repository, doctype);

      if (copy.skip()) {
        if (Mode.CREATE == projectConfig.getMode()) {
          return false;
        } else {
          copy.template();
          final String relatedDoctypeItems =
              createRelatedDoctypeItems(copy, doctype);
          if (doctype.hasHomepage()) {
            copy.home(relatedDoctypeItems);
          }
          copy.index(relatedDoctypeItems);

          copy.properties();
          return true;
        }
      }

      copy.wizardSoy();
      if (doctype.hasHomepage()) {
        copy.subspaceOptionSoy();
      }
      copy.wizardJs();

      copy.template();
      final String relatedDoctypeItems =
          createRelatedDoctypeItems(copy, doctype);
      if (doctype.hasHomepage()) {
        copy.home(relatedDoctypeItems);
        if (doctype.isType()) {
          copy.subspaceListenerJava();
        }
      }
      copy.index(relatedDoctypeItems);
      copy.spaceHomepage();
      // copy.contentManagementDashboard();

      copy.properties();

      copy.pluginXml();
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot create backup folder: " + e.getMessage(), e);
    }

    return true;
  }

  private static final String TEMPLATE_PREFIX =
      "<li><ac:structured-macro ac:name=\"projectdoc-link-wiki\">"
          + "<ac:parameter ac:name=\"empty-handling\">empty</ac:parameter>"
          + "<ac:parameter ac:name=\"page\">";
  private static final String TEMPLATE_SUFFIX =
      "</ac:parameter></ac:structured-macro></li>";

  private String createHomePageKey(final String id) {
    return "projectdoc.content." + id + ".home.title";
  }

  private String createIndexPageKey(final String id) {
    return "projectdoc.content." + id + ".index.all.title";
  }

  private String createKey(final Doctype doctype) {
    final String id = doctype.getId();
    return doctype.hasHomepage() ? createHomePageKey(id)
        : createIndexPageKey(id);
  }

  private String createRelatedDoctypeItems(final DoctypeCopyHelper copy,
      final Doctype doctype) {
    final StringBuilder buffer = new StringBuilder(1024);

    if (doctype.hasType()) {
      final String typeId = doctype.getSecondaryId();
      final Doctype typeDoctype = repository.getDoctype(typeId);
      final String key = typeDoctype != null ? createKey(typeDoctype)
          : createHomePageKey(typeId);
      buffer.append('\n')
          .append(TEMPLATE_PREFIX)
          .append(key)
          .append(TEMPLATE_SUFFIX);
    }
    if (doctype.isType()) {
      final Collection<Doctype> typedDoctypes =
          repository.getDoctypeWithType(doctype.getId());
      for (final Doctype typedDoctype : typedDoctypes) {
        final String key = createKey(typedDoctype);
        buffer.append('\n')
            .append(TEMPLATE_PREFIX)
            .append(key)
            .append(TEMPLATE_SUFFIX);
      }
    }

    for (final DoctypeRef doctypeRef : doctype.getRelatedDoctypes()) {
      final String id = doctypeRef.getId();
      final Doctype typeDoctype = repository.getDoctype(id);
      final String key =
          typeDoctype != null ? createKey(typeDoctype) : createHomePageKey(id);
      buffer.append('\n')
          .append(TEMPLATE_PREFIX)
          .append(key)
          .append(TEMPLATE_SUFFIX);
    }
    return buffer.toString();
  }

  // --- object basics --------------------------------------------------------

}
