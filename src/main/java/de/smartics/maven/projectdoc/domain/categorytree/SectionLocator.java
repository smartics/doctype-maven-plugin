/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.categorytree;

import org.apache.commons.lang3.StringUtils;

/**
 * Defines the location of a section.
 *
 * @since 1.8
 */
public class SectionLocator {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Defines the relative position to which the section is located.
   */
  private final Position position;

  private final SectionRef sectionRef;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  /**
   * Default constructor.
   */
  public SectionLocator(final Position position, final SectionRef sectionRef) {
    this.position = position;
    this.sectionRef = sectionRef;
  }

  // ****************************** Inner Classes *****************************

  public static enum Position {
    BEFORE("before"), AFTER("after"), REPLACE("replace"), DELETE("delete"), ANY(
        "any"), FIRST("first"), LAST("last");

    private final String id;

    private Position(final String id) {
      this.id = id;
    }

    public static Position from(final String string)
        throws IllegalArgumentException {
      for (final Position position : values()) {
        if (position.id.equals(string)) {
          return position;
        }
      }
      throw new IllegalArgumentException("Unknown position: " + string);
    }

    public static Position toPosition(final String position)
        throws IllegalArgumentException {
      return StringUtils.isBlank(position) ? Position.REPLACE
          : Position.from(position);
    }

    @Override
    public String toString() {
      return id;
    }

    public boolean isAdding() {
      return this == BEFORE || this == AFTER || this == FIRST || this == LAST;
    }
  }

  public static final class SectionRef {
    /**
     * The title is used to identify the section.
     */
    private final String title;

    public SectionRef(final String title) {
      this.title = title;
    }

    public String getTitle() {
      return title;
    }

    @Override
    public String toString() {
      return title;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public Position getPosition() {
    return position;
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

  @Override
  public String toString() {
    return position + " '" + sectionRef + "\'";
  }
}
