/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.metadata;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.Metadata;
import de.smartics.maven.projectdoc.domain.bo.doctype.Property;
import de.smartics.maven.projectdoc.domain.bo.doctype.Section;
import de.smartics.maven.projectdoc.domain.bo.doctype.TypeReference;

import de.smartics.maven.projectdoc.domain.bo.doctype.Wizard;
import de.smartics.maven.projectdoc.domain.bo.doctype.WizardParam;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

final class DoctypeXmlExporter {

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Doctype doctype;

  private final String shortId;

  private final File metadataRootFolder;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public DoctypeXmlExporter(final Doctype doctype, final String shortId,
      final File metadataRootFolder) {
    this.doctype = doctype;
    this.shortId = shortId;
    this.metadataRootFolder = metadataRootFolder;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void export() {
    final String doctypeId = doctype.getId();

    try {
      final DocumentBuilder xmlBuilder =
          DocumentBuilderFactory.newInstance().newDocumentBuilder();
      final Document xml = xmlBuilder.newDocument();
      final Element rootElement = xml.createElement("doctype");
      rootElement.setAttribute("id", doctypeId);
      xml.appendChild(rootElement);

      final Element matadataElement = xml.createElement("metadata");
      add(xml, matadataElement,
          Metadata.create("projectdoc.doctype.common.in-context", shortId));
      final List<Metadata> metadata = doctype.getDoctypeMetadata();
      if (!metadata.isEmpty()) {
        for (final Metadata metadataProperty : metadata) {
          add(xml, matadataElement, metadataProperty);
        }
      }
      rootElement.appendChild(matadataElement);

      final List<Property> properties = doctype.getProperties();
      if (!properties.isEmpty()) {
        final Element propertiesElement = xml.createElement("properties");
        for (final Property property : properties) {
          add(xml, propertiesElement, doctypeId, property);
        }
        rootElement.appendChild(propertiesElement);
      }

      final List<Section> sections = doctype.getSections();
      if (!sections.isEmpty()) {
        final Element sectionsElement = xml.createElement("sections");
        for (final Section section : sections) {
          add(xml, sectionsElement, section);
        }
        rootElement.appendChild(sectionsElement);
      }

      final Wizard wizard = doctype.getWizard();
      if (wizard != null) {
        final List<WizardParam> wizardParameters =
            wizard.getAdditionalParameters();
        if (wizardParameters != null) {
          final Element wizardElement = xml.createElement("wizard");
          for (final WizardParam param : wizardParameters) {
            final Element parameterElement = xml.createElement("parameter");
            parameterElement.setAttribute("name", param.getName());
            parameterElement.setTextContent(param.getValue());
            wizardElement.appendChild(parameterElement);
          }
          rootElement.appendChild(wizardElement);
        }
      }

      writeToFile(doctypeId, xml);
    } catch (final ParserConfigurationException e) {
      throw new SystemException(
          "Cannot write properties doctype metadata for doctype '" + doctypeId +
          "': " + e.getMessage());
    }
  }

  private void add(final Document xml, final Element parentElement,
      final Metadata metadataProperty) {
    final Element propertyElement = xml.createElement("property");
    propertyElement.setAttribute("key", metadataProperty.getKey());
    propertyElement.setTextContent(metadataProperty.getValue());
    parentElement.appendChild(propertyElement);
  }

  private void add(final Document xml, final Element parentElement,
      final String doctypeId, final Property property) {
    final Element propertyElement = xml.createElement("property");
    propertyElement.setAttribute("key", property.getKey());

    final TypeReference typeReference = property.getTypeReference(doctypeId);
    if (typeReference != null) {
      final Element valueElement = xml.createElement("value");
      valueElement.setAttribute("refDoctype", typeReference.getForDoctypeId());
      valueElement.setAttribute("identifyingPropertyKey",
          typeReference.getUniqueIdentifierPropertyKey());
      propertyElement.appendChild(valueElement);
    }

    final String controls = property.getControls();
    if (StringUtils.isNotBlank(controls)) {
      final Element controlsElement = xml.createElement("controls");
      controlsElement.setTextContent(controls);
    }

    parentElement.appendChild(propertyElement);
  }

  private void add(final Document xml, final Element parentElement,
      final Section section) {
    final Element sectionElement = xml.createElement("section");
    sectionElement.setAttribute("key", section.getKey());
    parentElement.appendChild(sectionElement);
  }

  private void writeToFile(final String doctypeId, final Document xml)
      throws TransformerFactoryConfigurationError {
    final File file = new File(metadataRootFolder, doctypeId + ".xml");
    try (final OutputStream out = FileUtils.openOutputStream(file)) {
      final StreamResult result = new StreamResult(out);
      final TransformerFactory factory = TransformerFactory.newInstance();
      final Transformer transformer = factory.newTransformer();
      final DOMSource domSource = new DOMSource(xml);
      transformer.transform(domSource, result);
    } catch (final IOException | TransformerException e) {
      throw new SystemException(
          "Cannot write xml doctype metadata file '" + file.getAbsolutePath() +
          "': " + e.getMessage());
    }
  }

  // --- object basics --------------------------------------------------------

}
