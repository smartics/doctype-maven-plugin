/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import java.io.IOException;

/**
 * Writes space information to the add-on.
 */
public class SpaceExportService {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final AddonRepository repository;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public SpaceExportService(final AddonRepository repository) {
    this.repository = repository;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public boolean export(final Space space) throws SystemException {
    try {
      final ProjectConfiguration config = repository.getConfig();
      //config.info("EXPORTING Space " + space.getId() + "...");

      final SpaceCopyHelper copy = new SpaceCopyHelper(repository, space,
          repository.getConfig().getMode());
      copy.provideTemplate();
      // copy.providedPages();
      copy.properties();
      copy.registerWizard();
      copy.wizardSoy();
      copy.addSpace();
      copy.homepages();
      // copy.contentManagementDashboard();
      copy.tools();
      copy.importedDoctypes();
      config.info("    EXPORTED: " + space.getId());
    } catch (final IOException e) {
      throw new SystemException(
          "Cannot create backup folder: " + e.getMessage(), e);
    }

    return true;
  }

  // --- object basics --------------------------------------------------------

}
