/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.patch;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

class PatchLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String L10N = "/l10n";

  // --- members --------------------------------------------------------------

  private final File patchFolder;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  PatchLocations(final File patchFolder) {
    this.patchFolder = patchFolder;
  }

  // ****************************** Inner Classes *****************************

  static enum Location {
    TEXT_RESOURCE(L10N, "text-resources${locale}.properties");

    private final String pathPattern;

    Location(final String folder, final String file) {
      this.pathPattern = folder + '/' + file;
    }

    String toString(final String locale) {
      if (locale == null) {
        return StringUtils.replace(pathPattern, "${locale}", "");
      } else {
        return StringUtils.replace(pathPattern, "${locale}", "_" + locale);
      }
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public File getFile(final Location location, final String locale) {
    final String path = location.toString(locale);
    final File file = new File(patchFolder, path);
    return file;
  }

  // --- object basics --------------------------------------------------------

}
