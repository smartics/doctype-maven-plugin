/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.bo.doctype;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

final class SectionMapper {
  /**
   *
   */
  private static final String SECTION_DESCRIPTION_KEY =
      "projectdoc.doctype.common.description";

  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final Doctype doctype;

  private final Map<String, Section> primarySections;

  private final Map<String, Section> defaultSections;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  SectionMapper(final Doctype primary, final Doctype defaults) {
    this.doctype = primary;
    this.primarySections = map(primary);
    this.defaultSections = map(defaults);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  private Map<String, Section> map(final Doctype doctype) {
    final Map<String, Section> map = new LinkedHashMap<String, Section>();
    for (final Section section : doctype.getSections()) {
      map.put(section.getKey(), section);
    }
    return map;
  }


  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public List<Section> getSections() {
    final List<Section> copy =
        new ArrayList<Section>(defaultSections.size() + primarySections.size());

    final Section primaryDescription =
        primarySections.remove(SECTION_DESCRIPTION_KEY);
    if (primaryDescription != null) {
      final Section defaultSection =
          defaultSections.remove(SECTION_DESCRIPTION_KEY);
      primaryDescription.initializeWithDefaults(doctype.getId());
      copy.add(new CompoundSection(primaryDescription, defaultSection));
    } else {
      final Section defaultDescription =
          defaultSections.remove(SECTION_DESCRIPTION_KEY);
      if (defaultDescription != null) {
        copy.add(defaultDescription);
      }
    }

    copy.addAll(primarySections.values());

    for (final Section section : defaultSections.values()) {
      final String sectionKey = section.getKey();
      if (!primarySections.containsKey(sectionKey)) {
        copy.add(section);
      } else {
        final Iterator<Section> i = copy.iterator();
        while (i.hasNext()) {
          final Section current = i.next();
          if (sectionKey.equals(current.getKey())) {
            i.remove();
            break;
          }
        }
        final Section storedSection = primarySections.get(sectionKey);
        copy.add(new CompoundSection(storedSection, section));
      }
    }

    return copy;
  }


  // --- object basics --------------------------------------------------------

}
