/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.patch;

import de.smartics.maven.projectdoc.domain.SystemException;
import de.smartics.maven.projectdoc.domain.exporter.SpaceExportLocations;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import de.smartics.maven.projectdoc.io.PropertiesHelper;
import de.smartics.maven.projectdoc.io.Resource;

import org.apache.commons.lang.StringUtils;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;

final class Patcher {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final AddonRepository repository;

  private final SpaceExportLocations spaceExportLocations;

  private final PatchLocations patchLocations;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  Patcher(final AddonRepository repository) {
    final ProjectConfiguration projectConfig = repository.getConfig();
    this.repository = repository;
    this.spaceExportLocations =
        SpaceExportLocations.createForExport(projectConfig, null); // no
                                                                   // space-specific
                                                                   // resources
                                                                   // must be
                                                                   // accessed!
    this.patchLocations = new PatchLocations(projectConfig.getPatchFolder());
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  void properties() {
    final PropertiesHelper propertiesHelper = new PropertiesHelper(true);
    for (final String locale : repository.getSupportedLocales()) {
      final Resource exportedResource = spaceExportLocations
          .getResource(SpaceExportLocations.Location.TEXT_RESOURCE, locale);
      final File exportedFile = exportedResource.getFile();
      final File patchFile =
          patchLocations.getFile(PatchLocations.Location.TEXT_RESOURCE, locale);
      if (patchFile.exists() && exportedFile.exists()) {
        try {
          propertiesHelper.mergeAndWriteProperties(exportedFile, patchFile,
              "\nPatched Resources - note that overridden resources are replaced, marked with PATCHED!.");
        } catch (final IOException e) {
          throw new SystemException("Cannot patch properties with patch file "
              + patchFile.getAbsolutePath() + ": " + e.getMessage(), e);
        } catch (final SystemException e) {
          throw new SystemException("Cannot patch properties with patch file "
              + patchFile.getAbsolutePath() + ": " + e.getMessage(), e);
        }
      }
    }

    final String defaultLocale = repository.getAddon().getDefaultLocale();
    if (StringUtils.isNotBlank(defaultLocale)) {
      final File defaultLocaleFile = spaceExportLocations.getResource(
          SpaceExportLocations.Location.TEXT_RESOURCE, defaultLocale).getFile();
      if (defaultLocaleFile != null && defaultLocaleFile.canRead()) {
        final File currentDefaultLocaleFile = spaceExportLocations
            .getResource(SpaceExportLocations.Location.TEXT_RESOURCE, null)
            .getFile();
        try {
          repository.getConfig()
              .info("Copying resources file "
                  + defaultLocaleFile.getAbsolutePath()
                  + " for specified default locale '" + defaultLocale + "' to "
                  + currentDefaultLocaleFile + " ...");
          FileUtils.copyFile(defaultLocaleFile, currentDefaultLocaleFile);
        } catch (final IOException e) {
          throw new SystemException("Cannot copy default locale properties "
              + defaultLocaleFile.getAbsolutePath() + " to "
              + currentDefaultLocaleFile.getAbsolutePath() + ": "
              + e.getMessage(), e);
        }
      } else {
        repository.getConfig()
            .info("Cannot find resource file for specified default locale "
                + defaultLocale + ". Skipping ...");
      }
    }
  }

  // --- object basics --------------------------------------------------------

}
