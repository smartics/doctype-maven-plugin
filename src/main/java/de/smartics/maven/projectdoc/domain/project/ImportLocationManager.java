/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.project;

import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;

import java.io.IOException;

/**
 * Determines the import locations implementation based on the import location
 * type.
 */
public class ImportLocationManager implements TemplateLocations {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final TemplateLocations doctypeImportLocations;

  private final TemplateLocations wizardImportLocations;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ImportLocationManager(final ProjectConfiguration projectConfig,
      final Doctype doctype) {
    this.doctypeImportLocations =
        new DoctypeTemplateLocations(doctype, projectConfig);
    this.wizardImportLocations =
        new WizardTemplateLocations(doctype, projectConfig);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public TemplateLocations getTemplateLocations(final ImportLocation location)
      throws IllegalArgumentException {
    if (location instanceof DoctypeTemplateLocations.Location) {
      return doctypeImportLocations;
    } else if (location instanceof WizardTemplateLocations.Location) {
      return wizardImportLocations;
    } else {
      throw new IllegalArgumentException(
          "No locations for type: " + location.getClass().getName());
    }
  }

  @Override
  public String getResourceAsString(final Context context)
      throws IOException, IllegalArgumentException {
    final TemplateLocations locations =
        getTemplateLocations(context.getLocation());
    return locations.getResourceAsString(context);
  }

  @Override
  public String getResourceAsString(final String templateSetId,
      final Context context) throws IOException {
    final TemplateLocations locations =
        getTemplateLocations(context.getLocation());
    return locations.getResourceAsString(templateSetId, context);
  }

  // --- object basics --------------------------------------------------------

}
