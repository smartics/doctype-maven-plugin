/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.io;

import java.io.File;

public final class Resource {
  private final File file;
  private final String path;

  public Resource(final File file, final String path) {
    this.file = file;
    this.path = path;
  }

  public boolean exists() {
    return file.exists();
  }

  public File createBackupFile(final File baseFolder) {
    final File file = new File(baseFolder, path);
    final File folder = file.getParentFile();
    if (!folder.exists()) {
      folder.mkdirs();
    }
    return file;
  }

  public File getFile() {
    return file;
  }
}