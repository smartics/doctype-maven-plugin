/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.io;

import de.smartics.maven.projectdoc.domain.SystemException;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.MapConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.PropertiesConfigurationLayout;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Properties;

public class PropertiesHelper {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final boolean allowOverride;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public PropertiesHelper() {
    this(false);
  }

  public PropertiesHelper(final boolean allowOverride) {
    this.allowOverride = allowOverride;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public void mergeAndWriteProperties(final Configuration patch,
      final File fileToUpdate) throws SystemException {
    mergeAndWriteProperties(patch, fileToUpdate, null);
  }

  public void mergeAndWriteProperties(final Configuration patch,
      final File fileToUpdate, final String separator) throws SystemException {
    try {
      final PropertiesConfiguration properties =
          mergeProperties(patch, fileToUpdate, separator);
      normalizeLayout(properties);
      StringWriter sout = new StringWriter(8192);
      try {
        properties.write(sout);
      } catch (final ConfigurationException e) {
        throw new IOException(e);
      } finally {
        IOUtils.closeQuietly(sout);
      }

      fixLeadingSpacesInPropertyValues(fileToUpdate, sout);
    } catch (final IOException e) {
      throw new SystemException("Cannot merge properties "
          + fileToUpdate.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  /* This is a brute force workaround that replaces patterns.
   * [DTSMP-15] Support Leading Spaces in Property Values
   */
  private void fixLeadingSpacesInPropertyValues(final File fileToUpdate,
      StringWriter sout) throws FileNotFoundException, IOException {
    final String propertiesFileContent = sout.toString();
    final String patchedFileContent =
        StringUtils.replaceEach(propertiesFileContent,
            new String[] {"=\\\\ ", "= "}, new String[] {"=\\ ", "=\\ "});
    Writer out = null;
    try {
      out = new OutputStreamWriter(
          new BufferedOutputStream(new FileOutputStream(fileToUpdate)),
          Charset.forName("ISO-8859-1"));
      IOUtils.write(patchedFileContent, out);
    } finally {
      IOUtils.closeQuietly(out);
    }
  }

  public static void normalizeLayout(final PropertiesConfiguration properties) {
    final PropertiesConfigurationLayout layout =
        new PropertiesConfigurationLayout(properties.getLayout());
    layout.setGlobalSeparator("=");
    properties.setLayout(layout);
  }

  public PropertiesConfiguration mergeProperties(final Configuration patch,
      final File fileToUpdate) throws SystemException {
    return mergeProperties(patch, fileToUpdate, null);
  }

  public PropertiesConfiguration mergeProperties(final Configuration patch,
      final File fileToUpdate, final String separator) throws SystemException {
    try {
      final PropertiesConfiguration properties = readProperties(fileToUpdate);
      boolean first = true;
      for (final Iterator<String> i = patch.getKeys(); i.hasNext();) {
        final String key = i.next();

        final boolean isNewKey = !properties.containsKey(key);
        if (allowOverride || isNewKey) {
          final PropertiesConfigurationLayout layout = properties.getLayout();
          if (first && separator != null && isNewKey) {
            first = false;
            layout.setComment(key, separator);
          } else {
            if (allowOverride || first) {
              final String currentComment = layout.getComment(key);
              layout.setComment(key,
                  StringUtils.isNotBlank(currentComment)
                      ? currentComment + "\n# PATCHED!"
                      : "# PATCHED!");
            }
          }

          final String value = patch.getString(key);
          final String escapedValue = StringFunction.escapePropertyValue(value);
          properties.setProperty(key, escapedValue);
        }
      }
      return properties;
    } catch (final IOException e) {
      throw new SystemException("Cannot merge properties "
          + fileToUpdate.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  public PropertiesConfiguration readProperties(final File file)
      throws IOException {
    final PropertiesConfiguration config = new PropertiesConfiguration();

    Reader in = null;
    try {
      in = new InputStreamReader(
          new BufferedInputStream(new FileInputStream(file)),
          Charset.forName("ISO-8859-1"));
      config.getLayout().load(config, in);
      return config;
    } catch (final ConfigurationException e) {
      throw new IOException(e);
    } finally {
      IOUtils.closeQuietly(in);
    }
  }

  public Configuration mergeProperties(final Properties patch,
      final File fileToUpdate) {
    return mergeProperties(new MapConfiguration(patch), fileToUpdate);
  }

  public void mergeAndWriteProperties(final Properties patch,
      final File fileToUpdate) {
    mergeAndWriteProperties(new MapConfiguration(patch), fileToUpdate);
  }

  public void mergeAndWriteProperties(final Properties patch,
      final File fileToUpdate, final String separator) {
    mergeAndWriteProperties(new MapConfiguration(patch), fileToUpdate,
        separator);
  }

  public void mergeAndWriteProperties(final File exportedFile,
      final File patchFile, final String separator) throws IOException {
    final Configuration patch = readProperties(patchFile);
    mergeAndWriteProperties(patch, exportedFile, separator);
  }

  public static void readTo(final PropertiesConfiguration properties,
      final File file) throws ConfigurationException, IOException {
    final InputStream fis = new BufferedInputStream(new FileInputStream(file));
    final Reader reader = new InputStreamReader(fis, "ISO-8859-1");
    try {
      properties.read(reader);
    } finally {
      IOUtils.closeQuietly(reader);
    }
  }

  public static void writeTo(final PropertiesConfiguration propertiesConfig,
      final File file) throws ConfigurationException, IOException {
    final OutputStream out =
        new BufferedOutputStream(new FileOutputStream(file));
    final Writer writer = new OutputStreamWriter(out, "ISO-8859-1");
    try {
      propertiesConfig.write(writer);
    } finally {
      IOUtils.closeQuietly(writer);
    }
  }

  // --- object basics --------------------------------------------------------

}
