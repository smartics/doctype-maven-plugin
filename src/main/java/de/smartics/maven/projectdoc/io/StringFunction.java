/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.io;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.MapConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.translate.UnicodeEscaper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * String utilities.
 */
public final class StringFunction {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final Pattern SPLIT_PATTERN = Pattern.compile("\\s*,\\s*");

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * Replaces all consecutive white spaces by one space and removes white spaces
   * from the start and end of the string.
   *
   * @param string the string to strip.
   * @return the stripped string.
   */
  public static String strip(final String string) {
    return string.trim().replaceAll("\\s+", " ");
  }

  private static final UnicodeEscaper UNICODE =
      UnicodeEscaper.outsideOf(32, 0x7f);

  public static String escapeUnicode(final String input) {
    return UNICODE.translate(input);
  }

  public static String toData(final Configuration properties) {
    if (properties.isEmpty()) {
      return StringUtils.EMPTY;
    }
    try {
      final Properties apostrophFix = new Properties();


      for (final Iterator<String> i = properties.getKeys(); i.hasNext();) {
        final String key = i.next();
        final String value = properties.getString(key);

        final String escapedValue = escapePropertyValue(value);
        apostrophFix.setProperty(key, escapedValue);
      }

      final ByteArrayOutputStream out = new ByteArrayOutputStream(8092);
      apostrophFix.store(out, "");
      final String moreData = out.toString("ISO-8859-1");
      return moreData;
    } catch (final UnsupportedEncodingException e) {
      return null;
    } catch (final IOException e) {
      return null;
    }
  }

  public static String toLower(final String input) {
    if (StringUtils.isBlank(input)) {
      return input;
    }

    final int length = input.length();
    final StringBuilder buffer = new StringBuilder(length);

    char lastChar = input.charAt(0);
    boolean lastCharWasUpperCase = Character.isUpperCase(lastChar);
    boolean inUppercaseMode = false;

    for (int i = 1; i < length; i++) {
      final char thisChar = input.charAt(i);

      final boolean thisCharIsUpperCase = Character.isUpperCase(thisChar);
      final boolean thisCharIsWhitespace = Character.isWhitespace(thisChar);

      if (inUppercaseMode) {
        buffer.append(lastChar);
        if (!thisCharIsUpperCase) {
          inUppercaseMode = false;
        }
      } else if (lastCharWasUpperCase
          && (thisCharIsUpperCase || thisCharIsWhitespace)) {
        buffer.append(lastChar);
        inUppercaseMode = !thisCharIsWhitespace;
      } else {
        buffer.append(Character.toLowerCase(lastChar));
        inUppercaseMode = false;
      }

      lastChar = thisChar;
      lastCharWasUpperCase = thisCharIsUpperCase;
    }

    if (inUppercaseMode) {
      buffer.append(lastChar);
    } else {
      buffer.append(Character.toLowerCase(lastChar));
    }
    return buffer.toString();
  }

  // This is a very crude workaround to the problem, that single >'< are removed
  // by Confluence when read from the properties file. Therefore we double it.
  // This won't work where more than one >'< is involved.
  public static String escapePropertyValue(final String value) {
    if (StringUtils.isBlank(value) || value.indexOf('\'') == -1) {
      return value;
    }

    final StringBuilder buffer = new StringBuilder(value.length() + 8);
    char lastChar = ' ';
    for (final char c : value.toCharArray()) {
      if ('\'' == c) {
        if (lastChar != '\'') {
          if (lastChar != '\\') {
            buffer.append('\'');
          }
        } else {
          continue;
        }
      }
      buffer.append(c);
      lastChar = c;
    }
    return buffer.toString();
  }

  public static String toData(final Properties properties) {
    return toData(new MapConfiguration(properties));
  }

  public static String toName(final String input) {
    if (input == null) {
      return null;
    }
    return StringUtils.capitalize(input).replaceAll("-", "");
  }

  private static final Pattern NORMALIZER_PATTERN =
      Pattern.compile("[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+");

  public static String normalizeUnicode(final String input) {
    if (input == null) {
      return null;
    }
    final String decomposed = Normalizer.normalize(input, Normalizer.Form.NFD);
    final String normalized =
        NORMALIZER_PATTERN.matcher(decomposed).replaceAll("");
    return normalized;
  }

  public static String[] splitByComma(final String value) {
    return splitByComma(value, true);
  }

  public static List<String> splitByCommaToList(final String value) {
    final ArrayList<String> list = new ArrayList<>(value.length());
    for (final String element : splitByComma(value, true)) {
      list.add(element);
    }
    return list;
  }

  public static String[] splitByComma(final String value, final boolean trim) {
    return splitBy(value, SPLIT_PATTERN, trim);
  }

  public static String[] splitByChar(final String value, final char character) {
    final String quoted = Pattern.quote(String.valueOf(character));
    final Pattern splitPattern = Pattern.compile("\\s*" + quoted + "\\s*");
    return splitBy(value, splitPattern);
  }

  public static String[] splitBy(final String value,
      final Pattern splitPattern) {
    return splitBy(value, splitPattern, true);
  }

  public static String[] splitBy(final String value, final Pattern splitPattern,
      final boolean trim) {
    if (StringUtils.isBlank(value)) {
      return new String[0];
    }

    final String[] tokens = splitPattern.split(value);
    if (trim) {
      for (int i = tokens.length - 1; i >= 0; i--) {
        final String normalizedToken = tokens[i].trim();
        tokens[i] = normalizedToken;
      }
    }
    return tokens;
  }

  // --- object basics --------------------------------------------------------

}
