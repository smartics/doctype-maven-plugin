/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.io;

import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.FileFilter;

/**
 * Filter on files that have an XML file name extension.
 */
public class XmlFileFilter implements FileFilter {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  public static final FileFilter INSTANCE = new XmlFileFilter();

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public boolean accept(final File file) {
    final String extension = FileUtils.getExtension(file.getName());
    return "xml".equals(extension);
  }

  // --- object basics --------------------------------------------------------

}
