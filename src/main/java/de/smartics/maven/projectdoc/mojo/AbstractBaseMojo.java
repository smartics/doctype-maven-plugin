/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import de.smartics.maven.projectdoc.maven.UserPropertiesManager;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * Base services for all mojos of this plugin.
 */
public abstract class AbstractBaseMojo extends AbstractMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Project for the execution environment.
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "${project}", readonly = true)
  protected MavenProject project;

  /**
   * Session for the execution environment.
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "${session}", readonly = true)
  protected MavenSession mavenSession;

  /**
   * A simple flag to skip the build process for the add-on project.
   *
   * @since 1.0
   */
  @Parameter(property = "doctype.skip", defaultValue = "false")
  private boolean skip;

  /**
   * A simple flag to log verbosely.
   *
   * @since 1.0
   */
  @Parameter(property = "doctype.verbose", defaultValue = "false")
  private boolean verbose;

  private UserPropertiesManager userPropertiesManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected UserPropertiesManager getUserPropertiesManager() {
    if (userPropertiesManager == null) {
      userPropertiesManager = new UserPropertiesManager(mavenSession);
    }
    return userPropertiesManager;
  }

  protected boolean isVerbose() {
    return verbose ||
           getUserPropertiesManager().getPropertyAsBoolean("verbose");
  }

  protected boolean isSkip() {
    return skip || getUserPropertiesManager().getPropertyAsBoolean("skip");
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
