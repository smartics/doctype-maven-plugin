/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import org.apache.maven.execution.ExecutionEvent;
import org.apache.maven.execution.ExecutionListener;
import org.apache.maven.execution.MavenSession;

final class DelegatingExecutionListener implements ExecutionListener {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final ExecutionListener delegate;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  DelegatingExecutionListener(final MavenSession mavenSession) {
    this(mavenSession.getRequest().getExecutionListener());
  }

  DelegatingExecutionListener(final ExecutionListener delegate) {
    this.delegate = delegate;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void projectDiscoveryStarted(final ExecutionEvent event) {
    delegate.projectDiscoveryStarted(event);
  }

  @Override
  public void sessionStarted(final ExecutionEvent event) {
    delegate.sessionStarted(event);
  }

  @Override
  public void sessionEnded(final ExecutionEvent event) {
    delegate.sessionEnded(event);
  }

  @Override
  public void projectSkipped(final ExecutionEvent event) {
    delegate.projectSkipped(event);
  }

  @Override
  public void projectStarted(final ExecutionEvent event) {
    delegate.projectStarted(event);
  }

  @Override
  public void projectSucceeded(final ExecutionEvent event) {
    delegate.projectSucceeded(event);
  }

  @Override
  public void projectFailed(final ExecutionEvent event) {
    delegate.projectFailed(event);
  }

  @Override
  public void mojoSkipped(final ExecutionEvent event) {
    delegate.mojoSkipped(event);
  }

  @Override
  public void mojoStarted(final ExecutionEvent event) {
    delegate.mojoStarted(event);
  }

  @Override
  public void mojoSucceeded(final ExecutionEvent event) {
    delegate.mojoSucceeded(event);
  }

  @Override
  public void mojoFailed(final ExecutionEvent event) {
    delegate.mojoFailed(event);
  }

  @Override
  public void forkStarted(final ExecutionEvent event) {
    delegate.forkStarted(event);
  }

  @Override
  public void forkSucceeded(final ExecutionEvent event) {
    delegate.forkSucceeded(event);
  }

  @Override
  public void forkFailed(final ExecutionEvent event) {
    delegate.forkFailed(event);
  }

  @Override
  public void forkedProjectStarted(final ExecutionEvent event) {
    delegate.forkedProjectStarted(event);
  }

  @Override
  public void forkedProjectSucceeded(final ExecutionEvent event) {
    delegate.forkedProjectSucceeded(event);
  }

  @Override
  public void forkedProjectFailed(final ExecutionEvent event) {
    delegate.forkedProjectFailed(event);
  }

  // --- object basics --------------------------------------------------------

}
