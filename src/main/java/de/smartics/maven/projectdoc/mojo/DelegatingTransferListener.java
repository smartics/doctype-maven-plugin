/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import org.apache.maven.execution.MavenSession;
import org.sonatype.aether.transfer.TransferCancelledException;
import org.sonatype.aether.transfer.TransferEvent;
import org.sonatype.aether.transfer.TransferListener;

class DelegatingTransferListener implements TransferListener {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final TransferListener transferListener;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  DelegatingTransferListener(final MavenSession mavenSession) {
    this(mavenSession.getRequest().getTransferListener());
  }

  DelegatingTransferListener(final TransferListener transferListener) {
    this.transferListener = transferListener;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void transferInitiated(final TransferEvent event)
      throws TransferCancelledException {
    transferListener.transferInitiated(event);
  }

  @Override
  public void transferStarted(final TransferEvent event)
      throws TransferCancelledException {
    transferListener.transferStarted(event);
  }

  @Override
  public void transferProgressed(final TransferEvent event)
      throws TransferCancelledException {
    transferListener.transferProgressed(event);
  }

  @Override
  public void transferCorrupted(final TransferEvent event)
      throws TransferCancelledException {
    transferListener.transferCorrupted(event);
  }

  @Override
  public void transferSucceeded(final TransferEvent event) {
    transferListener.transferSucceeded(event);
  }

  @Override
  public void transferFailed(final TransferEvent event) {
    transferListener.transferFailed(event);
  }

  // --- object basics --------------------------------------------------------

}
