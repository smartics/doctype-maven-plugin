/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.Maven;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DefaultArtifact;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionRequest;
import org.apache.maven.execution.MavenExecutionResult;
import org.apache.maven.model.Profile;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Builds the ORB artifact for the add-on project in the target folder.
 *
 * @description Builds the ORB artifact.
 * @since 1.0
 */
@Mojo(name = "build", threadSafe = true, requiresProject = true,
    defaultPhase = LifecyclePhase.PACKAGE)
public class BuildMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The execution environment for the ORB project.
   *
   * @since 1.0
   */
  @Component
  private Maven maven;

  //  @Parameter(defaultValue = "${mojoExecution}", readonly = true)
  //  private MojoExecution mojoExecution;

  /**
   * The identifier part of artifact ID and package path of the new project.
   *
   * @since 1.0
   */
  @Parameter(required = true)
  private String shortId;

  /**
   * Artifact ID for the project files in the target folder. <code>null</code>
   * if in archetype mode.
   * <p>
   * Archetype mode generates to the source folder. Otherwise the generation is
   * run in the target folder (not using the POM in the target folder).
   * </p>
   *
   * @since 1.0
   */
  @Parameter(required = true)
  private String artifactIdPrefix;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    if (isSkip()) {
      final Log log = getLog();
      log.info("Skipping ORB build since skip='true'.");
      return;
    }

    configure();
    run();
  }

  private void configure() {
    final Log log = getLog();
    if (isVerbose() || log.isDebugEnabled()) {
      final StringBuilder buffer = new StringBuilder(64);

      buffer.append(project.getName()).append(": ")
          .append(project.getBasedir().getAbsolutePath());

      //      buffer.append("\nExecution: ");
      //      buffer.append("\n  exec ID   ---> ")
      //          .append(mojoExecution.getExecutionId());
      //      buffer.append("\n  goal      ---> ")
      //          .append(mojoExecution.getGoal());
      //      buffer.append("\n  lifecycle ---> ")
      //          .append(mojoExecution.getLifecyclePhase());
      //      buffer.append("\n  source    ---> ")
      //          .append(mojoExecution.getSource());

      final List<Profile> profiles = project.getActiveProfiles();
      if (!profiles.isEmpty()) {
        buffer.append("\nProfiles: ");
        for (final Profile profile : profiles) {
          buffer.append(" ").append(profile.getId());
        }
      }

      //      renderProperties(buffer, "Project Properties", project
      //      .getProperties());
      //      renderProperties(buffer, "System Properties",
      //          mavenSession.getSystemProperties());
      //      renderProperties(buffer, "User Properties",
      //          mavenSession.getUserProperties());
      //      buffer.append("\nSettings Active Profiles: ")
      //          .append(mavenSession.getSettings()
      //              .getActiveProfiles());

      if (log.isDebugEnabled()) {
        log.debug(buffer);
      } else {
        log.info(buffer);
      }
    }
  }

  //  private void renderProperties(final StringBuilder buffer,
  //      final String heading, final Properties properties) {
  //    if (properties != null && !properties.isEmpty()) {
  //      buffer.append('\n')
  //          .append(heading)
  //          .append(':');
  //      for (final Map.Entry<Object, Object> entry : properties.entrySet()) {
  //        buffer.append("\n  ")
  //            .append(entry.getKey())
  //            .append('=')
  //            .append(entry.getValue());
  //      }
  //    }
  //  }

  private void run() throws MojoExecutionException, MojoFailureException {
    try {
      final Log log = getLog();
      if (isVerbose()) {
        logArtifactsOnInfo(log);
      }

      final File targetFolder = new File(project.getBuild().getDirectory());
      final String artifactId = artifactIdPrefix + shortId;
      final File baseFolder = new File(targetFolder, artifactId);
      final File projectFile = new File(baseFolder, "pom.xml");

      final MavenExecutionRequest orbRequest =
          DefaultMavenExecutionRequest.copy(mavenSession.getRequest());
      orbRequest.setBaseDirectory(baseFolder);
      // required to be 'verify', not 'package', to run reports
      // 'initialize' and 'license:format' is required to update license headers
      orbRequest.setGoals(
          Arrays.asList("initialize", "license:format", "verify"));
      orbRequest.setInteractiveMode(false);
      orbRequest.setPom(projectFile);
      orbRequest.setProfiles(project.getActiveProfiles());
      orbRequest.setExecutionListener(
          new DelegatingExecutionListener(mavenSession));
      //      orbRequest
      //          .setTransferListener(new DelegatingTransferListener
      //          (mavenSession));
      final MavenExecutionResult result = maven.execute(orbRequest);

      if (result.hasExceptions()) {
        throw new MojoExecutionException(
            "Cannot build project! " + result.getExceptions());
      }

      final MavenProject addonProject = result.getProject();
      final List<Artifact> attachedArtifacts =
          addonProject.getAttachedArtifacts();
      if (attachedArtifacts.isEmpty()) {
        final Artifact addonArtifact = addonProject.getArtifact();
        project.addAttachedArtifact(addonArtifact);
        log.info(" Attached '" + addonArtifact + "' ...");
        //        attachArtifactAs("jar", addonArtifact);
        attachArtifactAsPom(addonArtifact);
        log.info(
            "  No additional artifacts are present for attaching in add-on " +
            "project '" + addonProject.getId() + "'.");
      } else {
        log.info(
            "  Attaching additional artifacts are provided by add-on project " +
            "'" + addonProject.getId() + "' ...");
        for (final Artifact attachedArtifact : attachedArtifacts) {
          if (StringUtils.isNotBlank(attachedArtifact.getClassifier())) {
            if(attachedArtifacts.size() == 1 ) {
              attachArtifactAsPom(attachedArtifact);
              attachArtifactWithoutClassifierAsPluginJar(attachedArtifact);
            }
            attachArtifactAs("jar", attachedArtifact.getClassifier(),
                attachedArtifact);
          } else {
            project.addAttachedArtifact(attachedArtifact);
          }
          log.info("  Attached '" + attachedArtifact + "' ...");
          if ("obr".equals(attachedArtifact.getType())) {
            log.info("  Attaching additional artifacts for OBR ...");
            attachArtifactAsPom(attachedArtifact);
            attachArtifactAs("jar", attachedArtifact);
          }
        }
      }
      if (isVerbose()) {
        log.info("The artifacts for this project are finally ...");
        logArtifactsOnInfo(log);
      }
    } catch (final RuntimeException e) {
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private void logArtifactsOnInfo(final Log log) {
    final StringBuilder buffer = new StringBuilder(4096);
    final Artifact mainArtifact = project.getArtifact();
    buffer.append("Main artifact: ").append(mainArtifact.getId());
    final List<Artifact> attachedArtifacts = project.getAttachedArtifacts();
    if (attachedArtifacts.isEmpty()) {
      buffer.append("\nCurrently no artifacts are attached.");
    } else {
      buffer.append("\nCurrently attached artifacts:");
      for (final Artifact attachedArtifact : attachedArtifacts) {
        buffer.append("\n  ").append(attachedArtifact.getId());
      }
    }
    log.info(buffer.toString());
  }

  private void attachArtifactAsPom(Artifact artifact) {
    final String type = "pom";
    final Artifact pomArtifact =
        new DefaultArtifact(artifact.getGroupId(), artifact.getArtifactId(),
            artifact.getVersion(), artifact.getScope(), type, null,
            new DefaultArtifactHandler(type));
    final File pomFile =
        new File(artifact.getFile().getParentFile().getParentFile(), "pom.xml");
    if (pomFile.canRead()) {
      pomArtifact.setFile(pomFile);
      project.addAttachedArtifact(pomArtifact);
      getLog().info("  Attached '" + pomArtifact + "'.");
    } else {
      getLog().warn("  Attaching " + pomArtifact + " failed: Not found at " +
                    pomFile.getAbsolutePath());
    }
  }

  private void attachArtifactAs(final String type, final Artifact artifact) {
    attachArtifactAs(type, null, artifact);
  }

  private void attachArtifactAs(final String type, final String classifier,
      final Artifact artifact) {
    final Artifact attachedArtifact =
        new DefaultArtifact(artifact.getGroupId(), artifact.getArtifactId(),
            artifact.getVersion(), artifact.getScope(), type, classifier,
            new DefaultArtifactHandler(type));
    final String filePath = artifact.getFile().getAbsolutePath();
    final File attachedArtifactFile =
        new File(filePath.substring(0, filePath.length() - 4) +
        /*(StringUtils.isNotBlank(classifier) ? '-' + classifier :
         StringUtils.EMPTY) + */ '.' + type);
    if (attachedArtifactFile.canRead()) {
      attachedArtifact.setFile(attachedArtifactFile);
      project.addAttachedArtifact(attachedArtifact);
      getLog().info("  Attached '" + attachedArtifact + "'.");
    } else {
      getLog().warn("  Attaching artifact " + attachedArtifact +
                    " failed: Not found at " +
                    attachedArtifactFile.getAbsolutePath());
    }
  }

  private void attachArtifactWithoutClassifierAsPluginJar(
      final Artifact artifact) {
    final String type = "atlassian-plugin";
    final DefaultArtifactHandler artifactHandler =
        new DefaultArtifactHandler(type);
    artifactHandler.setExtension("jar");
    final Artifact attachedArtifact =
        new DefaultArtifact(artifact.getGroupId(), artifact.getArtifactId(),
            artifact.getVersion(), artifact.getScope(), type, null,
            artifactHandler);

    final String filePath = artifact.getFile().getAbsolutePath();
    String prefix = filePath.substring(0, filePath.length() - 4);
    final String classifier = artifact.getClassifier();
    if (prefix.endsWith('-' + classifier)) {
      prefix = prefix.substring(0, prefix.length() - classifier.length() - 1);
    }

    final File attachedArtifactFile = new File(prefix + ".jar");
    if (attachedArtifactFile.canRead()) {
      attachedArtifact.setFile(attachedArtifactFile);
      project.addAttachedArtifact(attachedArtifact);
      getLog().info("  Attached '" + attachedArtifact + "'.");
    } else {
      getLog().warn("  Attaching artifact " + attachedArtifact +
                    " failed: Not found at " +
                    attachedArtifactFile.getAbsolutePath());
    }
  }


  // --- object basics --------------------------------------------------------

}
