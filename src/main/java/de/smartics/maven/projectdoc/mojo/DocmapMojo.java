/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AndFileFilter;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * Create a docmap JSON file from resources found in the file system.
 *
 * @description Creates the docmap JSON file.
 * @since 1.1
 */
@Mojo(name = "concat-docmaps", threadSafe = true, requiresProject = true)
public class DocmapMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The root folder to search for the docmap JSON files to concatenate.
   */
  @Parameter(property = "root", defaultValue = "..")
  private String rootFolderName;

  /**
   * Allows to override the name of the file containing the information to
   * concatenate. All files are required to have the same name.
   *
   * @since 1.1
   */
  @Parameter(property = "file-name", defaultValue = "docmap.json")
  private String docmapFileName;

  /**
   * The character encoding for the docmap files to read.
   *
   * @since 1.1
   */
  @Parameter(property = "encoding", defaultValue = "UTF-8")
  private String encoding;

  /**
   * Content to add to the generated docmap file.
   *
   * @since 1.1
   */
  @Parameter(property = "prefix")
  private String docmapPrefix;

  /**
   * Content to append to the generated docmap file.
   *
   * @since 1.1
   */
  @Parameter(property = "suffix")
  private String docmapSuffix;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    if (isSkip()) {
      final Log log = getLog();
      log.info("Skipping creating docmap JSON file since skip='true'.");
      return;
    }

    configure();

    run();
  }

  private void configure() {}

  private void run() throws MojoExecutionException, MojoFailureException {
    try {
      final File targetFolder = new File(project.getBuild()
          .getDirectory(), "projectdoc");
      final File targetFile = new File(targetFolder, docmapFileName);

      if (targetFile.exists()) {
        FileUtils.forceDelete(targetFile);
      }

      final String contents = createFileContents();
      FileUtils.write(targetFile, contents, encoding);
      if (isVerbose()) {
        getLog().info(
            "Wrote file " + targetFile.getAbsolutePath() + " successfully.");
      }
    } catch (final IOException e) {
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private String createFileContents() throws IOException {
    final File rootFolder = calcRootFolder();

    final StringBuilder buffer = new StringBuilder(1024 * 16);
    buffer.append("{\n");
    if (StringUtils.isNotBlank(docmapPrefix)) {
      buffer.append("  ")
          .append(docmapPrefix);
    }

    final Collection<File> files =
        FileUtils.listFiles(rootFolder, new NameFileFilter(docmapFileName),
            new AndFileFilter(DirectoryFileFilter.INSTANCE,
                new NotFileFilter(new NameFileFilter("classes"))));
    if (files.isEmpty()) {
      getLog().info("No files found matching name '" + docmapFileName
          + "' skipping generation.");
    }
    for (final File file : files) {
      if (file.isFile() && file.canRead()) {
        if (isVerbose()) {
          getLog().info("   processing " + file.getAbsolutePath() + " ...");
        }
        final String contents = FileUtils.readFileToString(file, encoding);
        final String strippedContents = strip(contents);
        if (StringUtils.isNotBlank(strippedContents)) {
          insertSeparator(buffer);
          buffer.append(strippedContents);
        }
      }
    }
    if (StringUtils.isNotBlank(docmapSuffix)) {
      insertSeparator(buffer);
      buffer.append(docmapSuffix);
    }
    buffer.append("\n}\n");

    return buffer.toString();
  }

  private static void insertSeparator(final StringBuilder buffer) {
    if (buffer.length() > 2) {
      buffer.append(",\n  ");
    }
  }

  private File calcRootFolder() throws IOException {
    File folder = new File(rootFolderName);
    if (folder.isAbsolute()) {
      return folder;
    }

    folder = new File(project.getBasedir(), rootFolderName).getCanonicalFile();
    return folder;
  }

  private String strip(final String contents) {
    final int startIndex = contents.indexOf('{');
    final int endIndex = contents.lastIndexOf('}');
    if (startIndex != -1 && startIndex < endIndex) {
      return contents.substring(startIndex + 1, endIndex)
          .trim();
    }
    return null;
  }

  // --- object basics --------------------------------------------------------

}
