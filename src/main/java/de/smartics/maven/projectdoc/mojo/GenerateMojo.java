/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import de.smartics.maven.projectdoc.domain.ApplicationException;
import de.smartics.maven.projectdoc.domain.bo.settings.Reference;
import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;
import de.smartics.maven.projectdoc.domain.service.ProjectService;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import javax.annotation.Nullable;
import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Applies changes to a doctype add-on project for the projectdoc Toolbox for
 * Confluence. There is no rollback or backup function! Make sure to run this
 * only against projects that are in version control and up-to-date. On any
 * problem sources (not files in the target folder) have to be fetched from
 * version control.
 *
 * @description Applies changes to a doctype add-on project for the projectdoc
 * Toolbox for Confluence.
 * @since 1.0
 */
@Mojo(name = "generate", threadSafe = true, requiresProject = true,
    defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class GenerateMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * A simple flag to skip the patching process.
   *
   * @since 1.0
   */
  @Parameter(property = "skip.patching", defaultValue = "false")
  private boolean skipPatching;

  /**
   * Controls whether or not the docmap JSON file should be generated. This
   * fragment needs to be deployed to the server for documentation support on
   * doctypes. The map allows to link to sections on a doctype documentation
   * document.
   *
   * @since 1.1
   */
  @Parameter(property = "generate.docmap", defaultValue = "true")
  private boolean generateDocmap;

  /**
   * Controls whether or not the category tree JSON file should be generate.
   *
   * @since 1.8
   */
  @Parameter(property = "generate.categoryTree", defaultValue = "false")
  private boolean generateCategoryTree;

  /**
   * Controls whether or not the RDF files for doctypes and the whole add-on
   * should be generated or not.
   *
   * @since 1.1
   */
  @Parameter(property = "generate.rdf", defaultValue = "true")
  private boolean generateRdf;

  /**
   * Instruction on whether doctypes should only be created
   * (<code>create</code>) or updated (<code>update</code>). Creation will not
   * run, if a template of a doctype already exists. If the update mode is
   * selected, the doctype will be removed and the resources in the resource
   * bundle will be updated.
   * <p>
   * Mode applies only to doctypes, not to spaces.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "mode", defaultValue = "create")
  private String mode;

  /**
   * The identifier part of artifact ID and package path of the new project.
   *
   * @since 1.0
   */
  @Parameter(required = true)
  private String shortId;

  /**
   * The identifier of the main space to associate the subspace blueprint.
   *
   * @since 1.0
   */
  @Parameter(required = true, defaultValue = "main")
  private String mainSpaceId;

  /**
   * The package prefix for the new project. Defaults to
   * '<code>com.myorg.projectdoc.doctypes</code>'.
   *
   * @since 1.0
   */
  @Parameter(property = "packagePrefix", defaultValue = "com.myorg.projectdoc" +
                                                        ".doctypes")
  private String packagePrefix;

  /**
   * The location of the model descriptors.
   * <p>
   * The models folder contains the <code>add-on.xml</code> and folders for
   * space and doctype descriptors.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "/src/main/resources/projectdoc-models",
      required = true)
  private String modelsFolder;

  /**
   * The location of the RDF files for doctype models.
   *
   * @since 1.1
   */
  @Parameter(defaultValue = "/src/main/resources/projectdoc/rdf", required =
      true)
  private String rdfFolder;

  /**
   * The location of the metadata files for doctype metadata.
   *
   * @since 1.1
   */
  @Parameter(defaultValue = "/src/main/resources/projectdoc/doctype-metadata"
      , required = true)
  private String doctypeMetadataFolder;

  /**
   * The name of the add-on. Defaults to the value of the short ID.
   *
   * @since 1.0
   */
  @Parameter
  private String projectName;

  /**
   * Group ID for the project files in the target folder. <code>null</code> if
   * in archetype mode.
   * <p>
   * Archetype mode generates to the source folder. Otherwise the generation is
   * run in the target folder (not using the POM in the target folder).
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String groupId;

  /**
   * Artifact ID for the project files in the target folder. <code>null</code>
   * if in archetype mode.
   * <p>
   * Archetype mode generates to the source folder. Otherwise the generation is
   * run in the target folder (not using the POM in the target folder).
   * </p>
   *
   * @since 1.0
   */
  @Parameter
  private String artifactIdPrefix;

  @Parameter
  private List<de.smartics.maven.projectdoc.mojo.Reference> references;

  @Parameter(defaultValue = "de/smartics/projectdoc/${addonId}/content" +
                            "/doctypes")
  private String pathTemplate;

  @Parameter
  private List<PluginArtifact> pluginArtifacts;

  /**
   * Specify a locale in which docmap information is generated.
   *
   * @since 2.1
   */
  @Nullable
  @Parameter
  private String locale;

  /**
   * Specify whether the app is approved by Atlassian to be run on data center.
   *
   * @since 3.1
   */
  @Parameter
  private boolean dataCenterApproved;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    if (isSkip()) {
      final Log log = getLog();
      log.info("Skipping doctype adjustments since skip='true'.");
      return;
    }

    configure();

    run();
  }

  private void configure() {
  }

  private void run() throws MojoExecutionException, MojoFailureException {
    try {
      final ProjectConfiguration config = createConfig();

      final ProjectService projectService = new ProjectService(config);
      projectService.execute();
    } catch (final ApplicationException e) {
      throw new MojoFailureException(e.getMessage(), e);
    } catch (final RuntimeException e) {
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  private ProjectConfiguration createConfig() {
    final ProjectConfiguration.Builder builder;
    final boolean archetypeMode =
        StringUtils.isBlank(groupId) && StringUtils.isBlank(artifactIdPrefix);
    if (archetypeMode) {
      final File modelsBaseFolder =
          new File(project.getBasedir(), modelsFolder);
      final File rfdBaseFolder = new File(project.getBasedir(), rdfFolder);
      final File doctypeMetadataBaseFolder =
          new File(project.getBasedir(), doctypeMetadataFolder);

      builder = new ProjectConfiguration.Builder().withShortId(shortId)
          .withMainSpaceId(mainSpaceId).withProjectName(project.getName())
          .withGroupId(project.getGroupId())
          .withArtifactId(project.getArtifactId())
          .withPackagePrefix(packagePrefix).withMode(mode)
          .skipPatching(skipPatching).generateDocmap(generateDocmap)
          .generateCategoryTree(generateCategoryTree).generateRdf(generateRdf)
          .withWorkingFolder(project.getBasedir())
          .withModelsFolder(modelsBaseFolder).withRdfFolder(rfdBaseFolder)
          .withDoctypeMetadataBaseFolder(doctypeMetadataBaseFolder)
          .withPluginArtifacts(pluginArtifacts).withPathTemplate(pathTemplate)
          .withSession(mavenSession).withLocale(locale)
          .withDataCenterApproved(dataCenterApproved);
    } else {
      final String artifactId = artifactIdPrefix + shortId;
      final File baseDir =
          new File(project.getBuild().getDirectory(), artifactId);
      final File modelsBaseFolder = new File(baseDir, modelsFolder);
      final File rfdBaseFolder = new File(baseDir, rdfFolder);
      final File doctypeMetadataBaseFolder =
          new File(baseDir, doctypeMetadataFolder);

      final String resolvedProjectName =
          StringUtils.isNotBlank(projectName) ? projectName : shortId;
      builder = new ProjectConfiguration.Builder().withShortId(shortId)
          .withMainSpaceId(mainSpaceId).withProjectName(resolvedProjectName)
          .withGroupId(groupId).withArtifactId(artifactId)
          .withPackagePrefix(packagePrefix).withMode(mode)
          .skipPatching(skipPatching).generateDocmap(generateDocmap)
          .generateCategoryTree(generateCategoryTree).generateRdf(generateRdf)
          .withWorkingFolder(baseDir).withModelsFolder(modelsBaseFolder)
          .withRdfFolder(rfdBaseFolder)
          .withDoctypeMetadataBaseFolder(doctypeMetadataBaseFolder)
          .withReferences(getReferences()).withPluginArtifacts(pluginArtifacts)
          .withPathTemplate(pathTemplate).withSession(mavenSession)
          .withLocale(locale).withDataCenterApproved(dataCenterApproved);
    }
    if (isVerbose()) {
      builder.withLog(getLog());
    }
    final ProjectConfiguration config = builder.build();
    return config;
  }

  private List<Reference> getReferences() {
    if (references != null && !references.isEmpty()) {
      return de.smartics.maven.projectdoc.mojo.Reference.map(references);
    }
    return Collections.emptyList();
  }

  // --- object basics --------------------------------------------------------

}
