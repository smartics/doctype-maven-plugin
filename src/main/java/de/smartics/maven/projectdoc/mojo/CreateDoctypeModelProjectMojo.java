/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import de.smartics.maven.projectdoc.domain.bo.settings.ProjectSettings;
import de.smartics.maven.projectdoc.domain.bo.settings.Reference;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.util.List;

/**
 * Creates a new project based on the doctype model archetype.
 *
 * @description Creates a new project based on the doctype model archetype.
 * @since 1.0
 */
@Mojo(name = "create-model", requiresProject = false, requiresReports = false,
    defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class CreateDoctypeModelProjectMojo extends AbstractCreateProjectMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The prefix of the artifact ID for the new project. Defaults to
   * '<code>myorg-doctype-model-</code>'.
   *
   * @since 1.0
   */
  @Parameter(property = "artifactIdPrefix",
      defaultValue = "myorg-doctype-model-")
  private String artifactIdPrefix;

  /**
   * The prefix of the artifact ID for the OBR (add-on) project. Defaults to
   * '<code>myorg-doctype-addon-</code>'.
   *
   * @since 1.0
   */
  @Parameter(property = "obrArtifactIdPrefix",
      defaultValue = "myorg-doctype-addon-")
  private String obrArtifactIdPrefix;

  /**
   * The version of the archetype
   * '<code>de.smartics.atlassian.confluence:smartics-projectdoc-doctype-model-archetype</code>'.
   * Defaults to '<code>2.1.2-SNAPSHOT</code>'.
   *
   * @since 1.0
   */
  @Parameter(property = "archetypeVersion", defaultValue = "2.1.2-SNAPSHOT")
  private String archetypeVersion;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  protected String getArtifactIdPrefix() {
    return artifactIdPrefix;
  }

  @Override
  protected String getArchetypeVersion() {
    return archetypeVersion;
  }

  @Override
  protected String getArchetypeArtifactId() {
    return "smartics-projectdoc-doctype-model-archetype";
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    check();

    final ProjectSettings settings = createSettings();

    if (settings == null) {
      getLog().info("Project already created, skipping ...");
      return;
    }

    if (isVerbose()) {
      getLog().info("Starting project creation based on archtetype "
          + getArchetypeArtifactId() + ":" + archetypeVersion + " ...");
    }

    final boolean mojoOverrides = false;
    final String artifactPrefix =
        extract("artifactIdPrefix", settings.getCoordinates()
            .getModelArtifactIdPrefix(), getArtifactIdPrefix(), mojoOverrides);
    final String obrArtifactPrefix = extract("obrArtifactIdPrefix",
        settings.getCoordinates()
            .getArtifactIdPrefix(),
        CreateDoctypeModelProjectMojo.this.obrArtifactIdPrefix, mojoOverrides);
    final Executor executor = new Executor(settings, artifactPrefix,
        obrArtifactPrefix, mojoOverrides);
    executor.run();

    if (!isProjectPresent()) {
      // The changes to the POM need only be applied if it is a project of its
      // own. The applied changes are not relevant if the artifact is attached
      // to the enclosing element.
      applyStandardPomContribution(settings);
    }
  }

  @Override
  protected String getProjectFolderName(final ProjectSettings projectSettings) {
    String prefix = projectSettings.getCoordinates()
        .getModelArtifactIdPrefix();
    if (StringUtils.isBlank(prefix)) {
      prefix = getArtifactIdPrefix();
    }

    final String projectFolderName = prefix + getShortId();
    return projectFolderName;
  }

  @Override
  protected List<Reference> getReferences(final ProjectSettings settings)
      throws MojoExecutionException {
    return settings.getReferences();
  }

  @Override
  protected String getCopyright(final String signature) {
    return "Copyright ${year} " + signature;
  }

  // --- object basics --------------------------------------------------------

}
