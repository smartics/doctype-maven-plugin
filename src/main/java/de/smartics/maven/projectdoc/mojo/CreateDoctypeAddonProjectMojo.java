/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import de.smartics.maven.projectdoc.domain.bo.settings.ProjectSettings;
import de.smartics.maven.projectdoc.domain.bo.settings.Reference;
import de.smartics.maven.projectdoc.domain.pom.PomHack;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.License;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.util.List;

/**
 * Creates a new project based on the doctype add-on archetype.
 *
 * @description Creates a new project based on the doctype add-on archetype.
 * @since 1.0
 */
@Mojo(name = "create", requiresProject = false, requiresReports = false,
    defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class CreateDoctypeAddonProjectMojo extends AbstractCreateProjectMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The prefix of the artifact ID for the new project. Defaults to
   * '<code>myorg-doctype-addon-</code>'.
   *
   * @since 1.0
   */
  @Parameter(property = "artifactIdPrefix",
      defaultValue = "myorg-doctype-addon-")
  private String artifactIdPrefix;

  /**
   * The version of the archetype
   * '<code>de.smartics.atlassian.confluence:smartics-projectdoc-doctype-addon-archetype</code>'.
   * Defaults to '<code>6.0.1-SNAPSHOT</code>'.
   *
   * @since 1.0
   */
  @Parameter(property = "archetypeVersion", defaultValue = "6.0.1-SNAPSHOT")
  private String archetypeVersion;

  @Parameter
  private List<de.smartics.maven.projectdoc.mojo.Reference> references;

  @Parameter
  private List<Dependency> dependencies;

  @Parameter
  private List<PluginArtifact> pluginArtifacts;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected String getArtifactIdPrefix() {
    return artifactIdPrefix;
  }

  protected String getArchetypeVersion() {
    return archetypeVersion;
  }

  @Override
  protected String getArchetypeArtifactId() {
    return "smartics-projectdoc-doctype-addon-archetype";
  }

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    check();
    final ProjectSettings settings = createSettings();

    if (settings == null) {
      getLog().info("Project already created, skipping ...");
      return;
    }

    if (isVerbose()) {
      getLog().info("Starting project creation based on archtetype "
          + getArchetypeArtifactId() + ":" + archetypeVersion + " ...");
    }

    final String artifactPrefixFromMojo = getArtifactIdPrefix();
    final String artifactPrefix = extract("artifactIdPrefix",
        settings.getCoordinates()
            .getArtifactIdPrefix(),
        artifactPrefixFromMojo,
        isNotDefault(artifactPrefixFromMojo, "myorg-doctype-addon-"));
    final String obrArtifactPrefix = null;
    final Executor executor =
        new Executor(settings, artifactPrefix, obrArtifactPrefix, true);
    executor.run();

    if (!isProjectPresent()) {
      // The changes to the POM need only be applied if it is a project of its
      // own. The applied changes are not relevant if the artifact is attached
      // to the enclosing element.
      applyStandardPomContribution(settings);
    }

    final PomHack pomHack = new PomHack(project, dependencies, pluginArtifacts);
    pomHack.applyToPom(projectFolder);
  }


  private static boolean isNotDefault(String artifactPrefixFromMojo,
      String defaultValue) {
    return !defaultValue.equals(artifactPrefixFromMojo);
  }

  protected String getProjectFolderName(final ProjectSettings projectSettings) {
    final String overridePrefix = getArtifactIdPrefix();
    String prefix = projectSettings.getCoordinates()
        .getArtifactIdPrefix();
    if (StringUtils.isNotBlank(overridePrefix) || StringUtils.isBlank(prefix)) {
      prefix = overridePrefix;
    }

    final String projectFolderName = prefix + getShortId();
    return projectFolderName;
  }

  @Override
  protected List<Reference> getReferences(final ProjectSettings settings)
      throws MojoExecutionException {
    if (references != null && !references.isEmpty()) {
      return de.smartics.maven.projectdoc.mojo.Reference.map(references);
    }
    return settings.getReferences();
  }

  @Override
  protected String getCopyright(final String signature) {
    final List<License> licenses = project.getLicenses();
    if (!licenses.isEmpty()) {
      final License license = licenses.get(0);
      final String comments = license.getComments();
      return comments;
    }
    return "Copyright ${year} " + signature;
  }

  // --- object basics --------------------------------------------------------

}
