/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import static org.twdata.maven.mojoexecutor.MojoExecutor.artifactId;
import static org.twdata.maven.mojoexecutor.MojoExecutor.configuration;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.executeMojo;
import static org.twdata.maven.mojoexecutor.MojoExecutor.executionEnvironment;
import static org.twdata.maven.mojoexecutor.MojoExecutor.goal;
import static org.twdata.maven.mojoexecutor.MojoExecutor.groupId;
import static org.twdata.maven.mojoexecutor.MojoExecutor.name;
import static org.twdata.maven.mojoexecutor.MojoExecutor.plugin;

import de.smartics.maven.projectdoc.domain.bo.settings.Coordinates;
import de.smartics.maven.projectdoc.domain.bo.settings.Keys;
import de.smartics.maven.projectdoc.domain.bo.settings.Layout;
import de.smartics.maven.projectdoc.domain.bo.settings.ProjectSettings;
import de.smartics.maven.projectdoc.domain.bo.settings.Reference;
import de.smartics.maven.projectdoc.domain.pom.PomHandler;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer;
import de.smartics.maven.projectdoc.domain.project.PlaceholderReplacer.Tuple;
import de.smartics.maven.projectdoc.io.StringFunction;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.io.ModelParseException;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.annotation.CheckForNull;

/**
 * Base for project creation mojos.
 *
 * @since 1.0
 */
public abstract class AbstractCreateProjectMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * Plugin manager for the execution environment.
   *
   * @since 1.0
   */
  @Component
  protected BuildPluginManager pluginManager;

  /**
   * Controls whether or not default settings for a project should be read from
   * the user's <tt>.m2</tt> folder.
   *
   * @since 1.0
   */
  @Parameter(property = "readProjectSettings", defaultValue = "true")
  private boolean readProjectSettings;

  /**
   * The group ID of the new project. Defaults to
   * '<code>com.myorg.projectdoc.doctypes</code>'.
   *
   * @since 1.0
   */
  @Parameter(property = "groupId",
      defaultValue = "com.myorg.projectdoc.doctypes")
  protected String groupId;

  /**
   * The package prefix for the new project. Defaults to
   * '<code>com.myorg.projectdoc.doctypes</code>'.
   *
   * @since 1.0
   */
  @Parameter(property = "packagePrefix",
      defaultValue = "com.myorg.projectdoc.doctypes")
  private String packagePrefix;

  /**
   * The identifier part of artifact ID and package path of the new project.
   *
   * @since 1.0
   */
  @Parameter(property = "shortId")
  private String shortId;

  /**
   * The identifier of the main space to associate the subspace blueprint.
   *
   * @since 1.0
   */
  @Parameter(property = "mainSpaceId", required = true, defaultValue = "main")
  private String mainSpaceId;

  /**
   * The name of the add-on. Defaults to the value of the short ID.
   *
   * @since 1.0
   */
  @Parameter(property = "projectName")
  private String projectName;

  /**
   * The optional project description.
   *
   * @since 1.0
   */
  @Parameter(property = "projectDescription")
  private String projectDescription;

  /**
   * The optional organization signature which is used to construct the
   * copyright notice.
   *
   * @since 1.0
   */
  @Parameter
  private String organizationSignature;

  /**
   * The version of the archetype plugin to call. Defaults to
   * <code>3.0.1</code>.
   *
   * @since 1.0
   */
  @Parameter(property = "archetypePluginVersion", defaultValue = "3.0.1")
  private String archetypePluginVersion;

  /**
   * The starting version new project. Defaults to
   * '<code>0.1.0-SNAPSHOT</code>'.
   *
   * @since 1.0
   */
  @Parameter(property = "projectVersion", defaultValue = "0.1.0-SNAPSHOT")
  private String projectVersion;

  /**
   * The identifier of the project settings to use. This id is the name of the
   * file (without the filename extension) that is expected to be stored in the
   * Maven directory (<code>.m2</code>).
   *
   * @since 1.0
   */
  @Parameter(property = "projectSettingsId", defaultValue = "project-settings")
  private String projectSettingsId;

  /**
   * The location of the model descriptors.
   * <p>
   * The models folder contains the <code>add-on.xml</code> and folders for
   * space and doctype descriptors.
   * </p>
   * <p>
   * Use <code>createExamples</code> to create this folder structure with
   * example files.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(property = "modelsFolder",
      defaultValue = "/src/main/resources/projectdoc-models")
  private String modelsFolder;

  /**
   * Per default the folder structure for models (descriptor files) is created
   * with example files. If you do not want this folder to be added, set this
   * flag to <code>false</code>.
   *
   * @since 1.0
   */
  @Parameter(property = "createExamples", defaultValue = "true")
  private boolean createExamples;

  @Parameter(property = "confluenceCompatibilityMin")
  private String confluenceCompatibilityMin;

  @Parameter(property = "confluenceCompatibilityMax")
  private String confluenceCompatibilityMax;

  protected File workFolder;

  protected File projectFolder;

  protected PlaceholderReplacer standardReplacer;

  /**
   * A tool to create a new model project with a short command line. The
   * elements are '<tt>shortId|projectName|projectDescription</tt>'.
   * <p>
   * This also deactivates the generation of examples.
   * </p>
   *
   * @since 1.1
   */
  @Parameter(property = "efault")
  private String commandLine;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  protected final class Executor {

    private final ProjectSettings settings;

    private final String groupId;
    private final String artifactIdPrefix;
    private final String obrArtifactIdPrefix;
    private final String version;
    private final String packagePrefix;
    private final String modelsFolder;

    protected Executor(final ProjectSettings settings,
        final String artifactIdPrefix, final String obrArtifactIdPrefix,
        final boolean mojoOverrides) {
      this.settings = settings;

      final Coordinates coordinates = settings.getCoordinates();
      this.groupId = extract("groupId", coordinates.getGroupId(), getGroupId(),
          mojoOverrides);
      this.artifactIdPrefix = artifactIdPrefix;
      this.obrArtifactIdPrefix = obrArtifactIdPrefix;
      this.version = extract("projectVersion", coordinates.getVersion(),
          getProjectVersion(), mojoOverrides);
      final Keys keys = settings.getKeys();
      this.packagePrefix = extract("packagePrefix", keys.getPackagePrefix(),
          getPackagePrefix(), mojoOverrides);
      final Layout layout = settings.getLayout();
      this.modelsFolder = extract("modelsFolder", layout.getModelsFolder(),
          getModelsFolder(), mojoOverrides);
    }

    protected void run() throws MojoExecutionException, MojoFailureException {
      final String shortId = getShortId();
      final String mainSpaceId = getMainSpaceId();
      final String projectName = calcProjectName(shortId);
      final String projectNameUrl = encodeUrl(projectName);
      final String projectNameXml = encodeXml(projectName);
      final String normalizedProjectDescription =
          StringUtils.isNotBlank(getProjectDescription())
              ? getProjectDescription()
              : projectName
                  + " Add-on for the projectdoc Toolbox on Confluence.";
      final String projectDescriptionXml =
          encodeXml(normalizedProjectDescription);
      final String year = calcCurrentYear();
      final MavenSession mavenSession = getMavenSession();
      final Properties properties = mavenSession.getUserProperties();
      properties.setProperty("shortId", shortId);
      properties.setProperty("mainSpaceId", mainSpaceId);
      properties.setProperty("projectName", projectName);
      properties.setProperty("projectNameUnicode",
          StringFunction.escapeUnicode(projectName));
      properties.setProperty("projectNameXml", projectNameXml);
      properties.setProperty("projectNameUrl", projectNameUrl);
      properties.setProperty("projectDescription",
          normalizedProjectDescription);
      properties.setProperty("projectDescriptionXml", projectDescriptionXml);
      properties.setProperty("inceptionYear", year);
      final String shortIdUpper = shortId.toUpperCase(Locale.ENGLISH);
      properties.setProperty("shortId.upper", shortIdUpper);
      properties.setProperty("groupId", groupId);
      properties.setProperty("artifactId", artifactIdPrefix + shortId);
      if (StringUtils.isNotBlank(obrArtifactIdPrefix)) {
        properties.setProperty("obrArtifactIdPrefix", obrArtifactIdPrefix);
      }
      properties.setProperty("version", version);
      properties.setProperty("packagePrefix", packagePrefix);
      properties.setProperty("package", packagePrefix + '.' + shortId);
      properties.setProperty("modelsFolder", modelsFolder);
      if (StringUtils.isNotBlank(confluenceCompatibilityMin)) {
        properties.setProperty("confluenceCompatibilityMin",
            confluenceCompatibilityMin);
      }
      if (StringUtils.isNotBlank(confluenceCompatibilityMax)) {
        properties.setProperty("confluenceCompatibilityMax",
            confluenceCompatibilityMax);
      }

      final String versionDoctypeMavenPlugin = copyProjectProperty(properties,
          "doctype-maven-plugin.version", "version_doctypeMavenPlugin");
      final String versionProjectdoc = copyProjectProperty(properties,
          "smartics-projectdoc-confluence.version", "version_projectdoc");

      final String signature = calcOrganizationSignature();
      if (StringUtils.isNotBlank(signature)) {
        properties.setProperty("organizationSignature", signature);
        properties.setProperty("organizationSignatureXml",
            encodeXml(signature));
        final String copyright = getCopyright(signature);
        properties.setProperty("copyright", copyright);
      }

      standardReplacer = new PlaceholderReplacer(
          Tuple.a("${inceptionYear}", year), Tuple.a("${shortId}", shortId),
          Tuple.a("${shortId.upper}", shortIdUpper),
          Tuple.a("${organizationSignature}", signature),
          Tuple.a("${doctype-maven-plugin.version}", versionDoctypeMavenPlugin),
          Tuple.a("${smartics-projectdoc-confluence.version}",
              versionProjectdoc));

      for (final Reference reference : getReferences(settings)) {
        final String key = reference.getName();
        final String value = reference.getLocator();
        final String normalizedValue = standardReplacer.replace(value);
        properties.setProperty(key, normalizedValue);
      }

      final String workingDir = workFolder.getAbsolutePath();
      final String interactiveMode = isProjectPresent() ? "false" : "true";
      executeMojo(plugin(groupId("org.apache.maven.plugins"),
          artifactId("maven-archetype-plugin"), getArchetypePluginVersion()),
          goal("generate"),
          configuration(element(name("basedir"), workingDir),
              element(name("interactiveMode"), interactiveMode),
              element(name("archetypeCatalog"), "local"),
              element(name("archetypeVersion"), getArchetypeVersion()),
              element(name("archetypeGroupId"),
                  "de.smartics.atlassian.confluence"),
              element(name("archetypeArtifactId"), getArchetypeArtifactId())),
          executionEnvironment(getMavenProject(), mavenSession,
              getPluginManager()));
    }

    private String calcProjectName(final String shortId) {
      final String projectNameFromPom = getProjectName();
      return StringUtils.isNotBlank(projectNameFromPom) ? projectNameFromPom
          : shortId;
    }

    private String copyProjectProperty(final Properties properties,
        final String srcKey, final String destKey) {
      final String value = project.getProperties()
          .getProperty(srcKey);
      if (value != null) {
        properties.setProperty(destKey, value);
      } else {
        getLog().info("No property value for " + srcKey + " set.");
      }
      return value;
    }

    private String calcOrganizationSignature() {
      String signature = settings.getKeys()
          .getOrganizationSignature();
      if (StringUtils.isBlank(signature)) {
        signature = organizationSignature;
      }
      return signature;
    }

    private String encodeXml(final String value) {
      return StringEscapeUtils.escapeXml10(value);
    }

    private String calcCurrentYear() {
      final Calendar calendar = Calendar.getInstance();
      calendar.setTime(new Date());
      final int year = calendar.get(Calendar.YEAR);
      return String.valueOf(year);
    }

    private String encodeUrl(final String label) {
      if (label != null) {
        try {
          return URLEncoder.encode(label, "UTF-8");
        } catch (final UnsupportedEncodingException e) {
          // return the unencoded label
        }
      }
      return label;
    }
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  protected MavenProject getMavenProject() {
    return project;
  }

  protected MavenSession getMavenSession() {
    return mavenSession;
  }

  protected BuildPluginManager getPluginManager() {
    return pluginManager;
  }

  protected String getGroupId() {
    return groupId;
  }

  protected String getPackagePrefix() {
    return packagePrefix;
  }

  protected String getShortId() {
    return shortId;
  }

  protected String getMainSpaceId() {
    return mainSpaceId;
  }

  protected String getProjectName() {
    return projectName;
  }

  protected String getProjectDescription() {
    return projectDescription;
  }

  protected String getArchetypePluginVersion() {
    return archetypePluginVersion;
  }

  protected String getProjectVersion() {
    return projectVersion;
  }

  protected String getProjectSettingsId() {
    return projectSettingsId;
  }

  protected String getModelsFolder() {
    return modelsFolder;
  }

  protected boolean isCreateExamples() {
    return createExamples;
  }

  protected abstract String getArchetypeArtifactId();

  protected abstract String getArtifactIdPrefix();

  protected abstract String getArchetypeVersion();

  // --- business -------------------------------------------------------------

  protected void check() throws MojoExecutionException {
    if (StringUtils.isNotBlank(commandLine)) {
      final String[] args = StringFunction.splitByChar(commandLine, '|');
      switch (args.length) {
        default:
          // longer lists are not supported
        case 3:
          shortId = args[0];
        case 2:
          projectName = args[1];
        case 1:
          projectDescription = args[2];
        case 0:
          // nothing to do
          createExamples = false;
      }
    }

    isValidIdentifier(getShortId());
  }

  protected ProjectSettings createSettings() throws MojoExecutionException {
    final String baseDir = System.getProperty("user.dir");
    getLog().info("Creating doctype add-on project " + getShortId() + " in "
        + baseDir + " ...");
    final ProjectSettings settings = configure(baseDir);
    return settings;
  }

  protected void isValidIdentifier(final String id)
      throws MojoExecutionException {
    if (StringUtils.isNotBlank(id)) {
      if (Character.isJavaIdentifierStart(id.charAt(0))) {
        final int length = id.length();
        if (length > 1) {
          for (int i = 1; i < length; i++) {
            final char ch = id.charAt(i);
            if (!Character.isJavaIdentifierPart(ch)) {
              throw new MojoExecutionException(
                  createMessageWithUrlToCreateDoctypeAddonProject(
                      "Identifier '" + id + "' is not valid: character '" + ch
                          + "' at position " + i
                          + " is not a valid identifier part."
                          + " The use of letters and digits is recommended."));
            }
          }
        }
        return;
      }

      throw new MojoExecutionException(
          createMessageWithUrlToCreateDoctypeAddonProject("Identifier '" + id
              + "' is not valid. It must start with a valid identifier character."
              + " The use of a lower case letter is recommended."));
    }

    throw new MojoExecutionException(
        createMessageWithUrlToCreateDoctypeAddonProject(
            "Identifier is not valid because it is blank."
                + " Please specify one with -DshortId!"));
  }

  private static String createMessageWithUrlToCreateDoctypeAddonProject(
      final String message) {
    return message
        + "\nSee https://www.smartics.eu/confluence/x/4QMvB for details"
        + " on how to create a doctype model project!";
  }

  protected File createWorkFolder(final File baseDir)
      throws MojoExecutionException {
    final File workFolder = baseDir;
    if (!workFolder.exists()) {
      if (!workFolder.mkdirs()) {
        throw new MojoExecutionException(
            "Cannot create work folder " + workFolder.getAbsolutePath() + ".");
      }
    }

    if (isProjectPresent()) {
      final File target = new File(workFolder, "target");
      if (!target.exists()) {
        if (!target.mkdirs()) {
          throw new MojoExecutionException(
              "Cannot create target folder " + target.getAbsolutePath() + ".");
        }
      }
      return target;
    }

    return workFolder;
  }

  protected boolean isProjectPresent() {
    return getMavenSession().getRequest()
        .isProjectPresent();
  }

  protected void replaceProperties(final String packageId, final File file)
      throws MojoExecutionException {
    try {
      final String content = FileUtils.fileRead(file, "UTF-8");
      final String replaced = content.replace("${package}", packageId);
      FileUtils.fileWrite(file.getAbsolutePath(), "UTF-8", replaced);
    } catch (final IOException e) {
      throw new MojoExecutionException("Cannot replace properties in "
          + file.getAbsolutePath() + ": " + e.getMessage(), e);
    }
  }

  protected File copyModelFile(final File modelsFolderFile,
      final String modelFileName) throws MojoExecutionException {
    try {
      final URL addOnUrl = Thread.currentThread()
          .getContextClassLoader()
          .getResource("examples/" + modelFileName);
      final File destFile = new File(modelsFolderFile, modelFileName);
      FileUtils.copyURLToFile(addOnUrl, destFile);
      return destFile;
    } catch (final IOException e) {
      throw new MojoExecutionException(
          "Cannot copy model file '" + modelFileName + "': " + e.getMessage(),
          e);
    }
  }

  protected String extract(final String key, final String fileBased,
      final String mojoBased) {
    return extract(key, fileBased, mojoBased,
        StringUtils.isNotBlank(mojoBased));
  }

  protected String extract(final String key, final String fileBased,
      final String mojoBased, final boolean mojoOverrides) {
    if (!mojoOverrides) {
      if (StringUtils.isNotBlank(fileBased)) {
        final String systemBased = System.getProperty(key);
        if (StringUtils.isBlank(systemBased)) {
          return fileBased;
        } else {
          return systemBased;
        }
      }
    }
    return mojoBased;
  }

  protected void applyStandardPomContribution(final ProjectSettings settings)
      throws MojoExecutionException {
    final File homeDir = new File(System.getProperty("user.home"));
    final File projectSettingsPomFile =
        new File(homeDir, ".m2/" + getProjectSettingsId() + "-pom.xml");
    if (projectSettingsPomFile.isFile() && projectSettingsPomFile.canRead()) {
      final File pomFile = new File(projectFolder, "pom.xml");
      try {
        final PomHandler pomHandler = new PomHandler(getLog(), standardReplacer,
            projectSettingsPomFile, pomFile);
        pomHandler.execute(settings);
      } catch (final ModelParseException e) {
        throw new MojoExecutionException(
            "Cannot parse POM file: " + e.getMessage(), e);
      } catch (final IOException e) {
        throw new MojoExecutionException(
            "Cannot access POM file: " + e.getMessage(), e);
      }
    }
  }

  @CheckForNull
  protected ProjectSettings configure(final String baseDir)
      throws MojoExecutionException {
    final ProjectSettings projectSettings = readProjectSettings();
    final Layout layout = projectSettings.getLayout();

    final File baseFolder = new File(baseDir);
    this.workFolder = createWorkFolder(baseFolder);

    this.projectFolder =
        new File(workFolder, getProjectFolderName(projectSettings));

    if (isProjectPresent()) {
      if (projectFolder.exists()) {
        return null;
      }
    }

    if (!projectFolder.exists()) {
      if (!projectFolder.mkdirs()) {
        throw new MojoExecutionException("Cannot create project folder "
            + projectFolder.getAbsolutePath() + ".");
      }
    }


    final String resolvedModelsFolder =
        extract("modelsFolder", layout.getModelsFolder(), modelsFolder);
    final File modelsFolderFile = new File(projectFolder, resolvedModelsFolder);
    final File spaceFolder = new File(modelsFolderFile, "spaces");
    spaceFolder.mkdirs();
    final File doctypesFolder = new File(modelsFolderFile, "doctypes");
    doctypesFolder.mkdirs();

    final String modelsFolder = getModelsFolder();
    if (isCreateExamples()) {
      final Keys keys = projectSettings.getKeys();
      final String packagePrefix =
          extract("packagePrefix", keys.getPackagePrefix(), getPackagePrefix());
      final String packageId = packagePrefix + '.' + getShortId();

      copyModelFile(modelsFolderFile, "add-on.xml");
      copyModelFile(spaceFolder, "main-space.xml");
      final File doctypeFile =
          copyModelFile(doctypesFolder, "sample-doctype.xml");
      replaceProperties(packageId, doctypeFile);
    } else {
      final File relocatedModelsFolder = new File(baseFolder, modelsFolder);
      if (relocatedModelsFolder.exists()) {
        final File destModelsFolder = new File(projectFolder, modelsFolder);
        if (!destModelsFolder.exists()) {
          if (!destModelsFolder.mkdirs()) {
            throw new MojoExecutionException("Cannot create models folder "
                + destModelsFolder.getAbsolutePath() + " in destination.");
          }
        }
        try {
          FileUtils.copyDirectoryStructure(relocatedModelsFolder,
              destModelsFolder);
        } catch (final IOException e) {
          throw new MojoExecutionException("Cannot copy models folder "
              + relocatedModelsFolder.getAbsolutePath() + " to "
              + destModelsFolder.getAbsolutePath() + ": " + e.getMessage());
        }
      }
    }

    return projectSettings;
  }

  private ProjectSettings readProjectSettings() throws MojoExecutionException {
    if (readProjectSettings) {
      final File homeDir = new File(System.getProperty("user.home"));
      final File projectSettingsFile =
          new File(homeDir, ".m2/" + getProjectSettingsId() + ".xml");
      final ProjectSettingsLoader loader = new ProjectSettingsLoader();
      final ProjectSettings projectSettings = loader.read(projectSettingsFile);
      return projectSettings;
    } else {
      return new ProjectSettings();
    }
  }

  protected abstract String getProjectFolderName(
      ProjectSettings projectSettings);

  protected abstract List<Reference> getReferences(
      final ProjectSettings settings) throws MojoExecutionException;

  protected abstract String getCopyright(final String signature);

  // --- object basics --------------------------------------------------------

}
