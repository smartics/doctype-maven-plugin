/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import de.smartics.maven.projectdoc.domain.pom.InstructionsHandler;

import org.apache.commons.lang3.StringUtils;

/**
 * Definition of a plugin artifact to add to the Confluence server
 * configuration.
 */
public class Dependency {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private String groupId;

  private String artifactId;

  private String version;

  private String scope;

  private String importInstruction;

  private String dynamicImportInstruction;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getScope() {
    return scope;
  }

  public String getGroupId() {
    return groupId;
  }

  public String getArtifactId() {
    return artifactId;
  }

  public String getVersion() {
    return version;
  }

  public void toXml(final StringBuilder buffer) {
    buffer.append("\n            <dependency>\n");
    buffer.append("              <groupId>").append(groupId)
        .append("</groupId>\n");
    buffer.append("              <artifactId>").append(artifactId)
        .append("</artifactId>\n");
    if (StringUtils.isNotBlank(version)) {
      buffer.append("              <version>").append(version)
          .append("</version>\n");
    }
    if (StringUtils.isNotBlank(scope)) {
      buffer.append("              <scope>").append(scope).append("</scope>\n");
    }
    buffer.append("            </dependency>");
  }


  public void toInstructions(final InstructionsHandler handler) {
    handler.importInstruction(importInstruction);
    handler.dynamicImportInstruction(dynamicImportInstruction);
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
