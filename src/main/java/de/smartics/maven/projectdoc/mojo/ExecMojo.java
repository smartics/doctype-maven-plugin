/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.Executor;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;

/**
 * Runs the Confluence locally in debug mode. This is quite crude.
 *
 * @description Runs the Confluence locally in debug mode.
 * @since 1.0
 */
@Mojo(name = "run", threadSafe = true, requiresProject = true)
public class ExecMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The identifier part of artifact ID and package path of the new project.
   *
   * @since 1.0
   */
  @Parameter(required = true)
  private String shortId;

  /**
   * Artifact ID for the project files in the target folder. <code>null</code>
   * if in archetype mode.
   * <p>
   * Archetype mode generates to the source folder. Otherwise the generation is
   * run in the target folder (not using the POM in the target folder).
   * </p>
   *
   * @since 1.0
   */
  @Parameter(required = true)
  private String artifactIdPrefix;


  @Parameter
  private String command;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    if (isSkip()) {
      final Log log = getLog();
      log.info("Skipping running Confluence server since skip='true'.");
      return;
    }

    configure();

    run();
  }

  private void configure() {}

  private void run() throws MojoExecutionException, MojoFailureException {
    try {
      final File targetFolder = new File(project.getBuild().getDirectory());
      final String artifactId = artifactIdPrefix + shortId;
      final File baseFolder = new File(targetFolder, artifactId);

      final Executor exec = new DefaultExecutor();
      final String commandString;
      if (StringUtils.isBlank(command)) {
        if (SystemUtils.IS_OS_WINDOWS) {
          commandString = "cmd.exe /C START \"Confluence: " + artifactId
              + "\" atlas-debug -Dmaven.test.skip";
        } else {
          commandString = "atlas-debug -Dmaven.test.skip&";
        }
      } else {
        commandString = command;
      }
      final CommandLine commandLine = CommandLine.parse(commandString);
      exec.setWorkingDirectory(baseFolder);
      final int exitValue = exec.execute(commandLine);
      if (exec.isFailure(exitValue)) {
        throw new MojoExecutionException(
            "Failed to run command '" + command + "': " + exitValue);
      }
    } catch (final IOException e) {
      throw new MojoExecutionException(e.getMessage(), e);
    }
  }

  // --- object basics --------------------------------------------------------

}
