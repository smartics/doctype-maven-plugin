/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Definition of a plugin artifact to add to the Confluence server
 * configuration.
 */
@SuppressFBWarnings("UWF_UNWRITTEN_FIELD")
public class PluginArtifact {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private String groupId;

  private String artifactId;

  private String version;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  public String getGroupId() {
    return groupId;
  }

  public String getArtifactId() {
    return artifactId;
  }

  public String getVersion() {
    return version;
  }

  public void toXml(final StringBuilder buffer) {
    buffer.append("\n            <pluginArtifact>\n");
    buffer.append("              <groupId>").append(groupId)
        .append("</groupId>\n");
    buffer.append("              <artifactId>").append(artifactId)
        .append("</artifactId>\n");
    buffer.append("              <version>").append(version)
        .append("</version>\n");
    buffer.append("            </pluginArtifact>");
  }

  // --- business -------------------------------------------------------------

  // --- object basics --------------------------------------------------------

}
