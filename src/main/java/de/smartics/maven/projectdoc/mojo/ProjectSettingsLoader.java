/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import de.smartics.maven.projectdoc.domain.bo.settings.ProjectSettings;

import org.apache.maven.plugin.MojoExecutionException;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

class ProjectSettingsLoader {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  private final SettingsAccessor accessor;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  ProjectSettingsLoader() throws MojoExecutionException {
    try {
      this.accessor = new SettingsAccessor();
    } catch (final JAXBException e) {
      throw new MojoExecutionException(
          "Cannot instantiate XML parsing for the project settings: "
              + e.getMessage(),
          e);
    }
  }

  // ****************************** Inner Classes *****************************

  private final class SettingsAccessor {
    // ******************************** Fields ********************************

    // --- constants ----------------------------------------------------------

    // --- members ------------------------------------------------------------

    private final JAXBContext xmlContext;

    private final Unmarshaller unmarshaller;

    // ***************************** Initializer ******************************

    // ***************************** Constructors *****************************

    private SettingsAccessor() throws JAXBException {
      this.xmlContext = JAXBContext.newInstance(ProjectSettings.class);
      this.unmarshaller = xmlContext.createUnmarshaller();
    }

    // ***************************** Inner Classes ****************************

    // ******************************** Methods *******************************

    // --- init ---------------------------------------------------------------

    // --- get&set ------------------------------------------------------------

    // --- business -----------------------------------------------------------

    private ProjectSettings unmarshal(final File file) throws JAXBException {
      final ProjectSettings entity =
          (ProjectSettings) unmarshaller.unmarshal(file);
      return entity;
    }

    // --- object basics ------------------------------------------------------
  }

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  ProjectSettings read(final File file) throws MojoExecutionException {
    if (file.canRead() && file.isFile()) {
      try {
        final ProjectSettings settings = accessor.unmarshal(file);
        if (settings != null) {
          return settings;
        }
      } catch (final JAXBException e) {
        throw new MojoExecutionException(
            "Cannot parse the project settings: " + e.getMessage(), e);
      }
    }
    return new ProjectSettings();
  }

  // --- object basics --------------------------------------------------------

}
