/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.mojo;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;

/**
 * Creates a basic patch tree in the models folder.
 *
 * @description Creates a basic patch tree in the models folder.
 * @since 1.0
 */
@Mojo(name = "create-patch-tree", threadSafe = true, requiresProject = true,
    defaultPhase = LifecyclePhase.NONE)
public class CreatePatchTreeMojo extends AbstractBaseMojo {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  /**
   * The location of the model descriptors.
   * <p>
   * The models folder contains the <code>add-on.xml</code> and folders for
   * space and doctype descriptors.
   * </p>
   *
   * @since 1.0
   */
  @Parameter(defaultValue = "/src/main/resources/projectdoc-models")
  private String modelsFolder;

  /**
   * The package prefix for the new project. Defaults to
   * '<code>com.myorg.projectdoc.doctypes</code>'.
   *
   * @since 1.0
   */
  @Parameter(required = true)
  private String packagePrefix;

  /**
   * The identifier part of artifact ID and package path of the new project.
   *
   * @since 1.0
   */
  @Parameter(required = true)
  private String shortId;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  public void execute() throws MojoExecutionException, MojoFailureException {
    if (isSkip()) {
      final Log log = getLog();
      log.info("Skipping patch tree creation since skip='true'.");
      return;
    }

    configure();

    run();
  }

  private void configure() {}

  private void run() throws MojoExecutionException, MojoFailureException {
    final File baseDir = project.getBasedir();
    final File modelsFolder = new File(baseDir, this.modelsFolder);
    final String packagePath = packagePrefix.replace('.', '/') + '/' + shortId;
    final File patchTreePath = new File(modelsFolder,
        "/patch/files/src/main/resources/" + packagePath);
    if (!patchTreePath.exists()) {
      if (!patchTreePath.mkdirs()) {
        throw new MojoExecutionException(
            "Cannot create path directory: " + patchTreePath.getAbsolutePath());
      }
    }
  }

  // --- object basics --------------------------------------------------------

}
