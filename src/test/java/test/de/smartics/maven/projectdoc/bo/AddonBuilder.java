/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package test.de.smartics.maven.projectdoc.bo;

import de.smartics.maven.projectdoc.domain.bo.addon.Addon;

/**
 * Builds {@link Addon} instances for tests.
 */
public class AddonBuilder {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  public static final Addon EMPTY = a().build();

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  private AddonBuilder() {}

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static AddonBuilder a() {
    return new AddonBuilder();
  }

  public Addon build() {
    return new Addon();
  }

  // --- object basics --------------------------------------------------------

}
