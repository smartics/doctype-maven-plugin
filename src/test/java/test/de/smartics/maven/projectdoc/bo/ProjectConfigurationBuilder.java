/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package test.de.smartics.maven.projectdoc.bo;

import de.smartics.maven.projectdoc.domain.project.ProjectConfiguration;

import java.io.File;

/**
 * Creates {@link ProjectConfiguration} instances for tests.
 */
public class ProjectConfigurationBuilder {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  public static ProjectConfiguration a(final File workingFolder,
      final File modelsFolder) {
    final ProjectConfiguration config = new ProjectConfiguration.Builder()
        .withShortId("shortId").withProjectName("projectName")
        .withGroupId("groupId").withArtifactId("artifactId")
        .withPackagePrefix("eu.smartics.test")
        .withWorkingFolder(workingFolder).withModelsFolder(modelsFolder)
        .build();
    return config;
  }

  // --- object basics --------------------------------------------------------

}
