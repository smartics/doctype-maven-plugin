/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.projectdoc.domain.importer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle;
import de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.Label;
import de.smartics.maven.projectdoc.domain.bo.doctype.DoctypeResourceBundle.Labels;
import de.smartics.maven.projectdoc.domain.bo.doctype.Property;
import de.smartics.maven.projectdoc.domain.bo.doctype.Section;
import de.smartics.maven.projectdoc.domain.importer.DoctypesImportService;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.util.test.io.FileTestUtils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import test.de.smartics.maven.projectdoc.bo.AddonBuilder;
import test.de.smartics.maven.projectdoc.bo.ProjectConfigurationBuilder;

/**
 * Tests {@link DoctypesImportService}.
 */
public class DoctypesImportServiceTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @Rule
  public TemporaryFolder exportFolder = new TemporaryFolder();

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  private DoctypesImportService createUut(final String id) throws IOException {
    final File workingFolder = exportFolder.newFolder("project");
    final File modelsFolder = FileTestUtils
        .getFileFromRelativeResource(DoctypesImportServiceTest.class, id);
    final DoctypesImportService uut =
        DoctypesImportService.create(new AddonRepository(
            ProjectConfigurationBuilder.a(workingFolder, modelsFolder),
            AddonBuilder.EMPTY));
    return uut;
  }

  // --- tests ----------------------------------------------------------------

  @Test
  public void supportsL10n() throws IOException {
    final DoctypesImportService uut = createUut("resourceBundle");

    final Doctype doctype = uut.iterator().next();

    final DoctypeResourceBundle resourceBundle = doctype.getResourceBundle();

    final Iterator<Labels> i = resourceBundle.iterator();
    final Labels l10nDefault = i.next();
    assertEquals("Default locale Name", "Threat Category",
        doctype.getName(null).toString());
    assertEquals("Default locale Name (pl)", "Treat Categories",
        doctype.getNamePlural(null).toString());
    assertEquals("Default locale Desc",
        "Defines categories to organize potential threats.",
        l10nDefault.getDescription());

    final Labels l10nDe = i.next();
    assertEquals("DE locale Name", "Bedrohungskategorie",
        doctype.getName("de").toString());
    assertEquals("DE locale Name", "Bedrohungskategorien",
        doctype.getNamePlural("de").toString());
    assertEquals("DE locale Desc",
        "Definiert Kategorien für die Organisation von m\u00f6glichen Bedrohungen.",
        l10nDe.getDescription());
  }

  @Test
  public void readsProperties() throws IOException {
    final DoctypesImportService uut = createUut("properties");

    final Doctype doctype = uut.iterator().next();
    assertEquals("Doctype name", "properties", doctype.getId());

    final List<Property> properties = doctype.getProperties();
    assertEquals("Properties", 2, properties.size());

    final Property property1 = properties.get(0);
    assertEquals("Property 1 name",
        "<at:i18n at:key=\"projectdoc.doctype.common.shortDescription\"/>",
        property1.getName());
    assertEquals("Property 1 name",
        "<at:var at:name=\"projectdoc.doctype.common.shortDescription\"/>",
        property1.getValue(null, null));
    assertNull("Property 1 controls", property1.getControls());
  }

  @Test
  public void readsSections() throws IOException {
    final DoctypesImportService uut = createUut("sections");

    final Doctype doctype = uut.iterator().next();
    assertEquals("Doctype name", "sections", doctype.getId());

    final List<Section> sections = doctype.getSections();
    assertEquals("Sections", 1, sections.size());

    final Section section1 = sections.get(0);
    assertEquals("Section 1 xml",
        "<ac:structured-macro ac:name=\"projectdoc-section\">\n"
            + "          <ac:parameter ac:name=\"title\"><at:i18n at:key=\"projectdoc.doctype.minutes.goals\"/></ac:parameter>\n"
            + "          <ac:rich-text-body>\n"
            + "            <ac:placeholder><at:i18n at:key=\"projectdoc.doctype.minutes.goals.placeholder\"/></ac:placeholder>\n"
            + "          </ac:rich-text-body>\n"
            + "        </ac:structured-macro>",
        section1.getXml(null, doctype.getId()));

    final DoctypeResourceBundle resourceBundle = section1.getResourceBundle();
    final Labels labels = resourceBundle.iterator().next();
    assertEquals("Title", labels.getName().getSingular());
    assertEquals("Help Text", labels.getDescription());
  }

  @Test
  public void supportsDefaults() throws IOException {
    final DoctypesImportService uut = createUut("baseTemplate");

    final Doctype doctype = uut.iterator().next();

    final List<Property> properties = doctype.getProperties();
    // Doctype not a property ...
    assertEquals("Properties", 12, properties.size());
    final List<Section> sections = doctype.getSections();
    assertEquals("Sections", 6, sections.size());
  }

  @Test
  public void supportsTypeGeneration() throws IOException {
    final DoctypesImportService uut = createUut("addType");

    final Iterator<Doctype> doctypes = uut.iterator();
    final Doctype doctype = doctypes.next();
    final Doctype doctypeType = doctypes.next();

    final String id = doctype.getId();
    assertEquals("ID", "addType", id);
    final String idType = doctypeType.getId();
    assertEquals("ID Type", "addType-type", idType);
  }

  @Test
  public void supportsPropertMacroValue() throws IOException {
    final DoctypesImportService uut = createUut("macro");

    final Doctype doctype = uut.iterator().next();

    final String name = doctype.getId();
    assertEquals("Name", "macro", name);
    final Property property = doctype.getProperties().get(0);
    final String paramName = property.getName();
    assertEquals("Param Name", "<at:i18n at:key=\"type\"/>", paramName);
    final String paramValue = property.getValue(null, null);
    assertEquals("Param Value",
        "<ac:structured-macro ac:name=\"projectdoc-name-list\">\n"
            + "                  <ac:parameter ac:name=\"doctype\">threat-target-type</ac:parameter>\n"
            + "                  <ac:parameter ac:name=\"property\"><at:i18n at:key=\"projectdoc.doctype.common.type\"/></ac:parameter>\n"
            + "                  <ac:parameter ac:name=\"render-no-hits-as-blank\">true</ac:parameter>\n"
            + "                  </ac:structured-macro>",
        paramValue);
  }

  @Test
  public void supportsPropertyPlaceholderValue() throws IOException {
    final DoctypesImportService uut = createUut("placeholder");

    final Doctype doctype = uut.iterator().next();

    final String name = doctype.getId();
    assertEquals("Name", "placeholder", name);
    final List<Property> properties = doctype.getProperties();
    assertEquals(4, properties.size());

    final Property property1 = properties.get(0);
    final String paramName1 = property1.getName();
    assertEquals("Param Name", "<at:i18n at:key=\"type\"/>", paramName1);
    final String paramValue1 = property1.getValue(null, null);
    assertEquals("Param Value",
        "<ac:placeholder><at:i18n at:key=\"type.placeholder\"/></ac:placeholder>",
        paramValue1);

    final Property property2 = properties.get(1);
    assertEquals("Param Name", "<at:i18n at:key=\"with-description\"/>",
        property2.getName());
    assertEquals("Param Value",
        "<ac:placeholder><at:i18n at:key=\"type.placeholder\"/></ac:placeholder>",
        property2.getValue(null, null));

    final Property property3 = properties.get(2);
    assertEquals("Param Name", "<at:i18n at:key=\"implied-placeholder\"/>",
        property3.getName());
    assertEquals("Param Value",
        "<ac:placeholder><at:i18n at:key=\"implied-placeholder.placeholder\"/></ac:placeholder>",
        property3.getValue(null, null));

    final Property property4 = properties.get(3);
    assertEquals("Param Name", "<at:i18n at:key=\"no-placeholder\"/>",
        property4.getName());
    assertEquals("Param Value", "A value.", property4.getValue(null, null));
  }

  @Test
  public void supportsSectionLabels() throws IOException {
    final DoctypesImportService uut = createUut("sectionLabels");

    final Doctype doctype = uut.iterator().next();

    final List<Section> sections = doctype.getSections();
    assertEquals("One section should be found", 1, sections.size());

    final Section section = sections.get(0);

    final DoctypeResourceBundle bundle = section.getResourceBundle();

    final Iterator<Labels> i = bundle.iterator();
    final Labels l10nDefault = i.next();
    assertEquals("Default locale Name", "Name",
        l10nDefault.getName().toString());
    final Label labelEn = l10nDefault.iterator().next();
    assertEquals("Default label key", "key-prefix.label", labelEn.getKey());
    assertEquals("Default label value", "Label", labelEn.getValue());

    final Labels l10nDe = i.next();
    assertEquals("DE locale Name", "Name de", l10nDe.getName().toString());
    final Label labelDe = l10nDe.iterator().next();
    assertEquals("DE label key", "key-prefix.label", labelDe.getKey());
    assertEquals("DE label value", "Label de", labelDe.getValue());
  }
}
