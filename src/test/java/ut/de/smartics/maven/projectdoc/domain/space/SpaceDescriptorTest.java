/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.projectdoc.domain.space;

import static org.junit.Assert.assertEquals;

import de.smartics.maven.projectdoc.domain.bo.space.ImportDoctypes;
import de.smartics.maven.projectdoc.domain.bo.space.RemoteDoctypeRef;
import de.smartics.maven.projectdoc.domain.bo.space.RemoteDoctypeSet;
import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.importer.SpacesImportService;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.util.test.io.FileTestUtils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.List;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import test.de.smartics.maven.projectdoc.bo.ProjectConfigurationBuilder;

/**
 * Tests {@link Space}
 */
public class SpaceDescriptorTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @Rule
  public TemporaryFolder exportFolder = new TemporaryFolder();


  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  private AddonRepository createUut(final String id) throws IOException {
    final File workingFolder = exportFolder.newFolder("project");
    final File modelsFolder = FileTestUtils
        .getFileFromRelativeResource(SpaceDescriptorTest.class, id);
    final AddonRepository uut = AddonRepository
        .create(ProjectConfigurationBuilder.a(workingFolder, modelsFolder));
    final SpacesImportService spaceImporter = SpacesImportService.create(uut);
    for (final Space space : spaceImporter) {
      uut.addSpace(space);
    }
    return uut;
  }

  // --- tests ----------------------------------------------------------------

  @Test
  @SuppressFBWarnings(value="NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE")
  public void importDoctypes() throws IOException {
    final AddonRepository uut = createUut("importDoctypes");
    final Space space = uut.getSpace("main");
    final ImportDoctypes importDoctypes = space.getImportDoctypes();

    final List<RemoteDoctypeSet> doctypeSets = importDoctypes.getDoctypeSets();
    assertEquals(2, doctypeSets.size());
    final List<RemoteDoctypeRef> doctypeRefs1 =
        doctypeSets.get(0).getDoctypeRefs();
    assertEquals(3, doctypeRefs1.size());
    assertEquals("association", doctypeRefs1.get(0).getId());
    assertEquals("association-type", doctypeRefs1.get(1).getId());
    assertEquals("category", doctypeRefs1.get(2).getId());
    final List<RemoteDoctypeRef> doctypeRefs2 =
        doctypeSets.get(1).getDoctypeRefs();
    assertEquals(1, doctypeRefs2.size());
    assertEquals("user-story", doctypeRefs2.get(0).getId());
  }
}
