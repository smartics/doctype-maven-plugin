/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.projectdoc.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import de.smartics.maven.projectdoc.io.StringFunction;

import org.junit.Test;

/**
 * Tests {@link StringFunction#toLower(String)}.
 */
public class StringFunctionToLowerTest {
  //********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  // --- tests ----------------------------------------------------------------

  @Test
  public void allowsNull() {
    final String input = null;
    final String result = StringFunction.toLower(input);
    assertNull(result);
  }

  @Test
  public void empty() {
    final String input = "";
    final String result = StringFunction.toLower(input);
    assertEquals("", result);
  }

  @Test
  public void onlyLowerCase() {
    final String input = "abc cde";
    final String result = StringFunction.toLower(input);
    assertEquals(input, result);
  }

  @Test
  public void allToLowerCase() {
    final String input = "Abc Cde";
    final String result = StringFunction.toLower(input);
    assertEquals("abc cde", result);
  }

  @Test
  public void preservesSingleUpperCaseLetter() {
    final String input = "Abc I Cde";
    final String result = StringFunction.toLower(input);
    assertEquals("abc I cde", result);
  }

  @Test
  public void preservesConsecutiveUpperCaseLetters() {
    final String input = "Abc IT Cde";
    final String result = StringFunction.toLower(input);
    assertEquals("abc IT cde", result);
  }

  @Test
  public void preservesCamelCaseLettersStart() {
    final String input = "Abc ITgo Cde";
    final String result = StringFunction.toLower(input);
    assertEquals("abc ITgo cde", result);
  }

  @Test
  public void preservesCamelCaseLettersMid() {
    final String input = "Abc xxITgo Cde";
    final String result = StringFunction.toLower(input);
    assertEquals("abc xxITgo cde", result);
  }

  @Test
  public void preservesCamelCaseLettersEnd() {
    final String input = "Abc xxIT Cde";
    final String result = StringFunction.toLower(input);
    assertEquals("abc xxIT cde", result);
  }

  @Test
  public void preservesUpperCaseLetterEnd() {
    final String input = "Abc IT";
    final String result = StringFunction.toLower(input);
    assertEquals("abc IT", result);
  }

  @Test
  public void preservesRepeating() {
    final String input = "Abc xxIT Cde I Xxx-Yyy IT Zzz";
    final String result = StringFunction.toLower(input);
    assertEquals("abc xxIT cde I xxx-yyy IT zzz", result);
  }

  @Test
  public void preservesStart() {
    final String input = "IT Cde";
    final String result = StringFunction.toLower(input);
    assertEquals("IT cde", result);
  }

  @Test
  public void preservesDash() {
    final String input = "IT-Cde";
    final String result = StringFunction.toLower(input);
    assertEquals("IT-cde", result);
  }

  @Test
  public void preservesStartCamel() {
    final String input = "ITCde-Xxx";
    final String result = StringFunction.toLower(input);
    assertEquals("ITCde-xxx", result);
  }
}
