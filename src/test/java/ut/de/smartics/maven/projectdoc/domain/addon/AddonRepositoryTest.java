/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.projectdoc.domain.addon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.smartics.maven.projectdoc.domain.bo.addon.Category;
import de.smartics.maven.projectdoc.domain.bo.addon.ImportDoctypes;
import de.smartics.maven.projectdoc.domain.bo.addon.RemoteDoctypeRef;
import de.smartics.maven.projectdoc.domain.bo.addon.RemoteDoctypeSet;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.util.test.io.FileTestUtils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.List;

import test.de.smartics.maven.projectdoc.bo.ProjectConfigurationBuilder;

/**
 * Tests {@link AddonRepository}
 */
public class AddonRepositoryTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @Rule
  public TemporaryFolder exportFolder = new TemporaryFolder();


  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  private AddonRepository createUut(final String id) throws IOException {
    final File workingFolder = exportFolder.newFolder("project");
    final File modelsFolder = FileTestUtils
        .getFileFromRelativeResource(AddonRepositoryTest.class, id);
    final AddonRepository uut = AddonRepository
        .create(ProjectConfigurationBuilder.a(workingFolder, modelsFolder));
    return uut;
  }

  // --- tests ----------------------------------------------------------------

  @Test
  public void readsLocalesFromFirstCategory() throws IOException {
    final AddonRepository uut = createUut("oneCategory");
    final List<String> result = uut.getSupportedLocales();
    assertEquals(2, result.size());
    assertTrue(result.contains(null));
    assertTrue(result.contains("de"));

    final List<Category> categories = uut.getAddon().getCategories();
    assertEquals(1, categories.size());
    final Category category = categories.get(0);
    final String nameEn = category.getName(null);
    final String descriptionEn = category.getDescription(null);
    assertEquals("My Main Category", nameEn);
    assertEquals("My category description.", descriptionEn);
    final String nameDe = category.getName("de");
    final String descriptionDe = category.getDescription("de");
    assertEquals("Meine Hauptkategorie", nameDe);
    assertEquals("Meine Kategoriebeschreibung.", descriptionDe);
  }

  @Test
  public void defaultsToDefaultAndDeIfNoLocaleIsProvided() throws IOException {
    final AddonRepository uut = createUut("noLocale");
    final List<String> result = uut.getSupportedLocales();
    assertEquals(2, result.size());
    assertTrue(result.contains(null));
    assertTrue(result.contains("de"));
  }

  @Test
  public void importDoctypes() throws IOException {
    final AddonRepository uut = createUut("importDoctypes");
    final ImportDoctypes importDoctypes = uut.getAddon().getImportDoctypes();

    final List<RemoteDoctypeSet> doctypeSets = importDoctypes.getDoctypeSets();
    assertEquals(2, doctypeSets.size());
    final List<RemoteDoctypeRef> doctypeRefs1 =
        doctypeSets.get(0).getDoctypeRefs();
    assertEquals(3, doctypeRefs1.size());
    assertEquals("association", doctypeRefs1.get(0).getId());
    assertEquals("association-type", doctypeRefs1.get(1).getId());
    assertEquals("category", doctypeRefs1.get(2).getId());
    final List<RemoteDoctypeRef> doctypeRefs2 =
        doctypeSets.get(1).getDoctypeRefs();
    assertEquals(2, doctypeRefs2.size());
    assertEquals("user-story", doctypeRefs2.get(0).getId());
  }
}
