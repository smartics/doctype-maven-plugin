/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.projectdoc.domain.exporter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import de.smartics.maven.projectdoc.domain.bo.doctype.Doctype;
import de.smartics.maven.projectdoc.domain.bo.doctype.Property;
import de.smartics.maven.projectdoc.domain.exporter.DoctypeExportService;
import de.smartics.maven.projectdoc.domain.importer.DoctypesImportService;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.util.test.io.FileTestUtils;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.List;

import test.de.smartics.maven.projectdoc.bo.AddonBuilder;
import test.de.smartics.maven.projectdoc.bo.ProjectConfigurationBuilder;
import ut.de.smartics.maven.projectdoc.domain.importer.DoctypesImportServiceTest;

/**
 * Tests {@link DoctypeExportService}.
 */
public class DoctypeExportServiceTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @Rule
  public TemporaryFolder exportFolder = new TemporaryFolder();

  private File workingFolder;

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  @Before
  public void setUp() throws IOException {
    workingFolder = exportFolder.newFolder("project");
  }

  // --- helper ---------------------------------------------------------------

  private DoctypeExportService createUut(final String id,
      final File modelsFolder) throws IOException {
    final DoctypeExportService uut =
        new DoctypeExportService(new AddonRepository(
            ProjectConfigurationBuilder.a(workingFolder, modelsFolder),
            AddonBuilder.EMPTY));
    return uut;
  }

  private DoctypesImportService createImportService(final String id,
      final File modelsFolder) {
    final DoctypesImportService uut =
        DoctypesImportService.create(new AddonRepository(
            ProjectConfigurationBuilder.a(workingFolder, modelsFolder),
            AddonBuilder.EMPTY));
    return uut;
  }

  private Doctype runExport(final String id) throws IOException {
    final File doctypesFolder = FileTestUtils
        .getFileFromRelativeResource(DoctypesImportServiceTest.class, id);
    final DoctypesImportService importer =
        createImportService(id, doctypesFolder);
    final DoctypeExportService uut = createUut(id, doctypesFolder);

    final Doctype doctype = importer.iterator().next();
    if (doctype == null) {
      throw new IllegalStateException("No doctype for id '" + id + "'.");
    }
    uut.export(doctype);
    return doctype;
  }

  private String readGeneratedPropertiesFile() throws IOException {
    final File file = new File(workingFolder,
        "src/main/resources/eu/smartics/test/shortId/text-resources.properties");
    final String content = FileUtils.readFileToString(file, "ISO-8859-1");
    return content;
  }

  private String readGeneratedTemplateFile(final String id) throws IOException {
    final File file = new File(workingFolder,
        "src/main/resources/eu/smartics/test/shortId/content/doctypes/" + id
            + "/" + id + "-template.xml");
    final String content = FileUtils.readFileToString(file, "UTF-8");
    return content;
  }

  private String readGeneratedSoyFile(final String id) throws IOException {
    final File file = new File(workingFolder,
        "src/main/resources/eu/smartics/test/shortId/soy/page-templates/projectdoc-template-"
            + id + ".soy");
    final String content = FileUtils.readFileToString(file, "UTF-8");
    return content;
  }

  // --- tests ----------------------------------------------------------------

  @Test
  public void exportsDoctype() throws IOException {
    final String id = "addType";
    final File modelsFolder = FileTestUtils
        .getFileFromRelativeResource(DoctypesImportServiceTest.class, id);
    final DoctypesImportService importer =
        createImportService(id, modelsFolder);
    final DoctypeExportService uut = createUut(id, modelsFolder);

    final Doctype doctype = importer.iterator().next();
    uut.export(doctype);

    final Doctype doctypeType = importer.iterator().next();
    uut.export(doctypeType);
  }

  @Test
  public void exportsDoctypeType() throws IOException {
    final String id = "baseTemplate";
    /* final Doctype doctype = */ runExport(id);
    // FIXME: Test results
  }

  @Test
  public void exportsPlaceholder() throws IOException {
    final String id = "placeholder";
    final Doctype doctype = runExport(id);

    final List<Property> properties = doctype.getProperties();
    assertEquals(4, properties.size());
    final Property property1 = properties.get(0);
    assertTrue(property1.hasPlaceholderValue());
    final Property property2 = properties.get(1);
    assertTrue(property2.hasPlaceholderValue());
    final Property property3 = properties.get(2);
    assertTrue(property3.hasPlaceholderValue());
    final Property property4 = properties.get(3);
    assertFalse(property4.hasPlaceholderValue());

    final String content = readGeneratedPropertiesFile();
    assertTrue("The placeholder without description should have a TODO value",
        content.contains("type.placeholder=TODO"));
    assertTrue("The placeholder with description should have its value",
        content.contains("with-description.placeholder=Test Description"));
    assertTrue("The implied placeholder should have its value",
        content.contains("implied-placeholder.placeholder=Test 3 Description"));
    assertFalse(
        "The non-placeholder without description should have no TODO value",
        content.contains("no-placeholder.placeholder="));

    final String templateContent = readGeneratedTemplateFile("placeholder");
    assertTrue("Should write section with title",
        templateContent.contains("<at:i18n at:key=\"implied-placeholder\" />"));
    assertTrue("Should write section with placeholder", templateContent
        .contains("<at:i18n at:key=\"implied-placeholder.placeholder\" />"));
  }

  @Test
  public void exportsSectionLabels() throws IOException {
    final String id = "sectionLabels";
    runExport(id);
    final String content = readGeneratedPropertiesFile();

    assertTrue("key-prefix.label", content.contains("key-prefix.label=Label"));
  }

  @Test
  public void supportsDefaultPlaceholderForDescriptionSection()
      throws IOException {
    final String id = "defaultDescription";
    runExport(id);
    final String content = readGeneratedPropertiesFile();

    assertFalse("Should not write Commons-Key",
        content.contains("projectdoc.doctype.common.description.placeholder"));
    assertFalse("Should not write own Name key", content
        .contains("projectdoc.doctype.default-description.description="));
    assertTrue("Should write own key", content.contains(
        "projectdoc.doctype.default-description.description.placeholder=Placeholder"));

    final String templateContent =
        readGeneratedTemplateFile("default-description");
    assertTrue("Should write section with title", templateContent.contains(
        "<at:i18n at:key=\"projectdoc.doctype.common.description\" />"));
    assertTrue("Should write section with placeholder",
        templateContent.contains(
            "<at:i18n at:key=\"projectdoc.doctype.default-description.description.placeholder\" />"));
  }

  @Test
  public void supportsSimplePlaceholderSection() throws IOException {
    final String id = "sectionPlaceholder";
    runExport(id);

    final String propertiesContent = readGeneratedPropertiesFile();
    assertTrue("Should write section title resource",
        propertiesContent.contains("section-id=Name"));
    assertTrue("Should write section placeholder resource",
        propertiesContent.contains("section-id.placeholder=Description"));

    final String templateContent =
        readGeneratedTemplateFile("section-placeholder");
    assertTrue("Should write section with title",
        templateContent.contains("<at:i18n at:key=\"section-id\" />"));
    assertTrue("Should write section with placeholder", templateContent
        .contains("<at:i18n at:key=\"section-id.placeholder\" />"));
  }

  @Test
  public void supportsPropertyOverride() throws IOException {
    final String id = "propertyOverride";
    runExport(id);

    final String propertiesContent = readGeneratedPropertiesFile();
    assertFalse("Should not write section title resource",
        propertiesContent.contains("projectdoc.doctype.common.reference="));
    assertFalse("Should not write section title resource", propertiesContent
        .contains("projectdoc.doctype.common.reference.placeholder="));
    assertTrue("Should write section placeholder resource",
        propertiesContent.contains(
            "projectdoc.doctype.property-override.reference.placeholder=Reference"));

    final String templateContent =
        readGeneratedTemplateFile("property-override");
    assertTrue("Should write section with title", templateContent.contains(
        "<at:i18n at:key=\"projectdoc.doctype.common.reference\" />"));
    assertFalse("Should not write section with doctype-specific title",
        templateContent.contains(
            "<at:i18n at:key=\"projectdoc.doctype.property-override.reference\" />"));
    assertTrue("Should write section with placeholder",
        templateContent.contains(
            "<at:i18n at:key=\"projectdoc.doctype.property-override.reference.placeholder\" />"));
  }

  @Test
  public void supportsPropertyValueOverride() throws IOException {
    final String id = "propertyValueOverride";
    runExport(id);

    final String templateContent =
        readGeneratedTemplateFile("property-value-override");
    assertTrue("Should write name property.", templateContent.contains(
        "<ac:structured-macro ac:name=\"projectdoc-transclusion-parent-property\">"));
    assertFalse(
        "Should have overridden standard name property, but the standard property is still there.",
        templateContent.contains(
            "<td class=\"confluenceTd\"><at:var at:name=\"projectdoc.doctype.common.name\" /></td>"));
  }

  @Test
  public void supportsWizards() throws IOException {
    final String id = "wizardSelection";
    runExport(id);

    final String soyContent = readGeneratedSoyFile("wizard-selection");
    assertTrue(soyContent.startsWith(
        "{namespace ProjectDocShortIdPlugin.Blueprints.Template.Wizardselection}"));
    // The wizard selected does not contain the send-to-homepage element
    assertTrue(!soyContent.contains("target-location-binder"));
  }
}
