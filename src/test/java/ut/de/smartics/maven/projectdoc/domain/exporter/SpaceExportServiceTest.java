/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.projectdoc.domain.exporter;

import static org.junit.Assert.assertFalse;

import de.smartics.maven.projectdoc.domain.bo.space.Space;
import de.smartics.maven.projectdoc.domain.exporter.DoctypeExportService;
import de.smartics.maven.projectdoc.domain.exporter.SpaceExportService;
import de.smartics.maven.projectdoc.domain.importer.SpacesImportService;
import de.smartics.maven.projectdoc.domain.project.AddonRepository;
import de.smartics.util.test.io.FileTestUtils;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import test.de.smartics.maven.projectdoc.bo.AddonBuilder;
import test.de.smartics.maven.projectdoc.bo.ProjectConfigurationBuilder;
import ut.de.smartics.maven.projectdoc.domain.importer.DoctypesImportServiceTest;

/**
 * Tests {@link DoctypeExportService}.
 */
public class SpaceExportServiceTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @Rule
  public TemporaryFolder exportFolder = new TemporaryFolder();

  private File workingFolder;

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  @Before
  public void setUp() throws IOException {
    workingFolder = exportFolder.newFolder("project");
  }

  // --- helper ---------------------------------------------------------------

  private SpaceExportService createUut(final String id, final File modelsFolder)
      throws IOException {
    final SpaceExportService uut = new SpaceExportService(new AddonRepository(
        ProjectConfigurationBuilder.a(workingFolder, modelsFolder),
        AddonBuilder.EMPTY));
    return uut;
  }

  private SpacesImportService createImportService(final String id,
      final File modelsFolder) {
    final SpacesImportService uut =
        SpacesImportService.create(new AddonRepository(
            ProjectConfigurationBuilder.a(workingFolder, modelsFolder),
            AddonBuilder.EMPTY));
    return uut;
  }

  // --- tests ----------------------------------------------------------------

  @Test
  public void exportsSpace() throws IOException {
    final String id = "spaceMain";
    final File spaceFolder = FileTestUtils
        .getFileFromRelativeResource(DoctypesImportServiceTest.class, id);
    final SpacesImportService importer = createImportService(id, spaceFolder);
    final SpaceExportService uut = createUut(id, spaceFolder);

    final Space space = importer.iterator().next();
    uut.export(space);

    final File file = new File(workingFolder,
        "src/main/resources/eu/smartics/test/shortId/text-resources.properties");
    final String resources = FileUtils.readFileToString(file, "ISO-8859-1");
    assertFalse(resources.contains("${"));
  }
}
