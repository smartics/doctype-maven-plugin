/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ut.de.smartics.maven.projectdoc.io;

import static org.junit.Assert.assertTrue;

import de.smartics.maven.projectdoc.io.StringFunction;

import org.junit.Test;

import java.util.Properties;

/**
 * Tests {@link StringFunction#toData(java.util.Properties)}.
 */
public class StringFunctionToDataTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  // --- tests ----------------------------------------------------------------

  @Test
  public void handlesApostrophs() {
    final Properties properties = new Properties();
    properties.setProperty("key", "value's");
    final String result = StringFunction.toData(properties);
    assertTrue(result.contains("value''s"));
  }
}
