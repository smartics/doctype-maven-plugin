/*
 * Copyright 2017-2025 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.maven.projectdoc.domain.exporter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.smartics.maven.projectdoc.io.PropertiesHelper;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Properties;

/**
 * Tests {@link PropertiesHelper}.
 */
public class PropertiesHelperTest {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  @Rule
  public TemporaryFolder propertiesFolder = new TemporaryFolder();

  private final PropertiesHelper uut = new PropertiesHelper();

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- prepare --------------------------------------------------------------

  // --- helper ---------------------------------------------------------------

  private void writeProperties(final File propertiesFile,
      final Properties properties)
      throws UnsupportedEncodingException, IOException {
    final Writer out = new OutputStreamWriter(
        FileUtils.openOutputStream(propertiesFile), "ISO-8859-1");
    try {
      properties.store(out, "Test with one value containing an apostroph");
    } finally {
      IOUtils.closeQuietly(out);
    }
  }

  private Properties readProperties(final File propertiesFile)
      throws IOException {
    final Properties loaded = new Properties();
    final InputStream in = FileUtils.openInputStream(propertiesFile);
    try {
      loaded.load(in);
    } finally {
      IOUtils.closeQuietly(in);
    }
    return loaded;
  }

  // --- tests ----------------------------------------------------------------

  @Test
  public void handlesApostrophs() throws IOException {
    final File propertiesFile = propertiesFolder.newFile("test-properties");
    final Properties properties = new Properties();
    properties.setProperty("old", "One's");
    writeProperties(propertiesFile, properties);

    final Properties newProperties = new Properties();
    newProperties.setProperty("new", "Anotherone's");
    uut.mergeAndWriteProperties(newProperties, propertiesFile);

    final Properties result = readProperties(propertiesFile);
    assertEquals("Failed to read stored property.", "One's",
        result.getProperty("old"));
    assertEquals("Failed to read updated property.", "Anotherone''s",
        result.getProperty("new"));

    final String propertiesFileContent =
        FileUtils.readFileToString(propertiesFile, "ISO-8859-1");
    assertTrue("Failed to encode stored property.",
        propertiesFileContent.contains("One's"));
    assertTrue("Failed to encode updated property.",
        propertiesFileContent.contains("Anotherone''s"));
  }

  /**
   * @see <a href="https://www.smartics.eu/jira/browse/DTSMP-15">[DTSMP-15]
   *      Support Leading Spaces in Property Values</a>
   */
  @Test
  public void handleLeadingSpaces() throws IOException {
    final File propertiesFile = propertiesFolder.newFile("test-properties");
    final Properties properties = new Properties();
    properties.setProperty("old", " One with leading spaces");
    writeProperties(propertiesFile, properties);

    final Properties newProperties = new Properties();
    newProperties.setProperty("new", " One with leading spaces");
    uut.mergeAndWriteProperties(newProperties, propertiesFile);

    final Properties result = readProperties(propertiesFile);
//  Should be, but the fix is applied on writing the properties' file content.
//  assertEquals("Failed to read stored property.",
//        "\\ One with leading spaces", result.getProperty("new"));
    assertEquals("Failed to read stored property.", " One with leading spaces",
        result.getProperty("new"));

    final String propertiesFileContent =
        FileUtils.readFileToString(propertiesFile, "ISO-8859-1");
    assertTrue("Failed to encode space in property.",
        propertiesFileContent.contains("\\ One with leading spaces"));
  }
}
